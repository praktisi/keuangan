﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using MySql.Data.MySqlClient;
using Aplikasi_Main_Akademik;

namespace SMSGatewayHonorDosen
{
    public partial class MainKeuanganForm : Form
    {
        //private bool isWindowOpened;
        //MySqlConnection connection;

        RegClass regClass = new RegClass();

        public MainKeuanganForm()
        {
            InitializeComponent();
        }

        public static bool isFormAnak = false;


        private void MainFormKeuangan_Load(object sender, EventArgs e)
        {
            Constanta.server = regClass.ReadFromRegistry("ip");            
        }

        private void dosenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isFormAnak)
            {
                smsgatewayhonordosen gateDosen = new smsgatewayhonordosen();
                gateDosen.MdiParent = this;
                
                gateDosen.Show();
                isFormAnak = true;
            }
        }

        private void mahasiswaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isFormAnak)
            {
                smsgatewaymahasiswa gateMhs = new smsgatewaymahasiswa();
                gateMhs.MdiParent = this;

                gateMhs.Show();

                isFormAnak = true;
            }
        }

        private void optionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string x = Interaction.InputBox("Developer Team Account : ", "Entering To Configuration", "");
            if (x == "Uci Arya")
            {
                //if (!isFormAnak)
                //{
                //    isFormAnak = !isFormAnak;
                    Option option = new Option();
                    option.MdiParent = this;

                    option.Show();
                //}
            }
            else
            {
                MessageBox.Show("Configuration is Denied");
            }
        }

        private void settingConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string x = Interaction.InputBox("Developer Team Account : ", "Entering To Configuration", "");
            if (x == "Uci Arya")
            {
                //if (!isFormAnak)
                //{
                //    isFormAnak = !isFormAnak;
                    SettingConfiguration sc = new SettingConfiguration();
                    sc.MdiParent = this;

                    sc.Show();
                //}
            }
            else
            {
                MessageBox.Show("Configuration is Denied");
            }
        }

        private void menuToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void pembayaranSPPDPPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(!isFormAnak)
            {
                Form_BayarSPPDPP bds = new Form_BayarSPPDPP();
                bds.MdiParent = this;
                
                bds.Show();

                isFormAnak = true;
            }            
        }

        private void MainKeuanganForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void inputDigitalInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isFormAnak)
            {
                Form_Input_Digi_Info digInfo = new Form_Input_Digi_Info();

                digInfo.MdiParent = this;
                digInfo.Show();

                isFormAnak = true;
            }
        }

        private void distribusiMahasiswaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isFormAnak)
            {
                Form_DistribusiPembayaranMahasiswa dpm = new Form_DistribusiPembayaranMahasiswa();
                dpm.MdiParent = this;

                dpm.Show();

                isFormAnak = true;    
            }
        }

        private void validasiKartuUjianToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (!isFormAnak)
            {
                Form_Cek_Kuisioner fck = new Form_Cek_Kuisioner();
                fck.MdiParent = this;
                fck.Show();

                isFormAnak = true;
            }
        }

    }
}
