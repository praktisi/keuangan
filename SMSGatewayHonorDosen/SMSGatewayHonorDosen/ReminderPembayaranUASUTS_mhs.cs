﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using MySql.Data.MySqlClient;

namespace SMSGatewayHonorDosen
{
    public partial class ReminderPembayaranUASUTS_mhs : Form
    {
        MySqlConnection connection;
        MySqlCommand cmd;
        MySqlDataAdapter adp;

        string query;

        //bool isDipilih;

        List<string> kode = new List<string>();

        RegClass regClass = new RegClass();

        public ReminderPembayaranUASUTS_mhs()
        {
            InitializeComponent();

            Constanta.server = regClass.ReadFromRegistry("ip");
            Constanta.Server();
            connection = new MySqlConnection(Constanta.conString);

        }

        private void ReminderPembayaranUASUTS_mhs_Load(object sender, EventArgs e)
        {
            RadioBtn_opsi2.Checked = false;
            RadioBtn_opsi1.Checked = false;
            VisibleGroupBox(false);
            //isi combobox angkatan
            FillCmbBoxAngkatan();
        }

        void FillCmbBoxAngkatan()
        {
            cmbBox_Angkatan.Items.Clear();
            //isDipilih = false;

            query = "SELECT DISTINCT kode_angkatan FROM tb_mahasiswa";
            cmd = new MySqlCommand(query, connection);

            try
            {
                connection.Open();
                adp = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    cmbBox_Angkatan.Items.Add(r[0].ToString());
                }

                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            //cmbBox_Jurusan.SelectedValue = null;
            //isDipilih = true;

        }

        private void cmbBox_Angkatan_SelectedValueChanged(object sender, EventArgs e)
        {
            //MessageBox.Show("a");
            //MessageBox.Show(cmbBox_Angkatan.SelectedItem.ToString());
            FillCmbBoxJurusan(cmbBox_Angkatan.Text);
        }

        bool isConnected()
        {
            bool b = false;
            try
            {
                connection.Open();
                b = true;
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);               
            }

            return b;
        }

        void FillCmbBoxJurusan(string angkatan)
        {
            cmbBox_Jurusan.Items.Clear();
            //isDipilih = false;

            query = "SELECT DISTINCT tb_jurusan.kode_jurusan , tb_jurusan.nama_jurusan FROM tb_mahasiswa INNER JOIN tb_jurusan ON tb_mahasiswa.kode_jurusan = tb_jurusan.kode_jurusan WHERE kode_angkatan ='"
                + angkatan + "'";
            //query = "SELECT DISTINCT tb_jurusan.nama_jurusan FROM tb_mahasiswa INNER JOIN tb_jurusan ON tb_mahasiswa.kode_jurusan = tb_jurusan.kode_jurusan WHERE kode_angkatan ='"
            //    + angkatan + "'";
            cmd = new MySqlCommand(query, connection);

            try
            {
                connection.Open();
                adp = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    list_kode_ang.Add(r[0].ToString());
                    cmbBox_Jurusan.Items.Add(r[1].ToString());
                    //cmbBox_Jurusan.ValueMember = "nama_jurusan";
                    //cmbBox_Jurusan.DisplayMember = "kode_jurusan";
                    //cmbBox_Jurusan.DataSource = dt;
                }

                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            if (isConnected())
            {
                //cmbBox_Angkatan.Items.Add();
            }

            //cmbBox_Jurusan.Text = null;
            //isDipilih = true;
        }

        void checkbox()
        {
            if (chekBox_Opsi.Checked == true )
            {

            }
        }

        List<string> list_kode_ang = new List<string>();
        string kode_ang;

        private void cmbBox_Jurusan_SelectedIndexChanged(object sender, EventArgs e)
        {
            kode_ang = list_kode_ang[cmbBox_Jurusan.SelectedIndex];
            //MessageBox.Show(kode_ang);
            //MessageBox.Show(cmbBox_Jurusan.SelectedItem.ToString());
            //MessageBox.Show("b");
            //MessageBox.Show(cmbBox_Jurusan.SelectedItem.ToString());
            FillCmbBoxKelas(cmbBox_Angkatan.Text, kode_ang);
        }

        void FillCmbBoxKelas(string angkatan, string jurusan)
        {
            connection = new MySqlConnection(Constanta.conString);
            cmbBox_Kelas.Items.Clear();
            query = "SELECT DISTINCT tb_kelas.nama_kelas FROM tb_mahasiswa INNER JOIN tb_kelas ON tb_mahasiswa.kode_kelas = tb_kelas.kode_kelas WHERE kode_angkatan ='" + angkatan + "' AND kode_jurusan ='" + jurusan + "'";
            cmd = new MySqlCommand(query, connection);

            try
            {
                connection.Open();
                adp = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    cmbBox_Kelas.Items.Add(r[0].ToString());
                }

                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        void getNohp()
        {
            
               
        }

        private void btn_Input_Click(object sender, EventArgs e)
        {
            string s = txtBox_NoHP.Text;

            if (txtBox_NoHP.Text != "")
            {
                listBox_Nohp.Items.Add(s);
            }
        }

        void VisibleGroupBox(bool isVisible)
        {
            groupBox2.Enabled = isVisible;
            grupBox_Opsicmb.Enabled = isVisible;
        }

        void ClearComboBox()
        {
            cmbBox_Angkatan.SelectedIndex = -1;
            cmbBox_Jurusan.SelectedIndex = -1;
            cmbBox_Kelas.SelectedIndex = -1;
        }

        private void RadioBtn_opsi1_Click(object sender, EventArgs e)
        {
           VisibleGroupBox(false);
           groupBox2.Enabled = true;
        }

        private void RadioBtn_opsi2_Click(object sender, EventArgs e)
        {
            VisibleGroupBox(false);
            grupBox_Opsicmb.Enabled = true;
        }

        private void settingConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string x = Interaction.InputBox("Developer Team Account : ", "Entering To Configuration", "");
            if (x == "Uci Arya")
            {
                SettingConfiguration fset = new SettingConfiguration();
                fset.ShowDialog();
            }
            else
            {
                MessageBox.Show("Configuration is Denied");
            }
        }

        private void optionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Ini untuk Buka tombol opsi
            string x = Interaction.InputBox("Developer Team Account : ", "Entering To Configuration", "");
            if (x == "Uci Arya")
            {
                Option option = new Option();
                option.ShowDialog();
            }
            else
            {
                MessageBox.Show("Configuration is Denied");
            }
        }

        private void btn_Kirim_Click(object sender, EventArgs e)
        {
            ClearComboBox();
        }

    }
}
