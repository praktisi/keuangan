﻿namespace SMSGatewayHonorDosen
{
    partial class MainKeuanganForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sMSBlastToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dosenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mahasiswaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orangTuaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mahasiswaDTLToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inputDigitalInfoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inputDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pembayaranSPPDPPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.klasifikasiBayaranToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.distribusiMahasiswaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.olahDataKlasifikasiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.validasiKartuUjianToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem,
            this.inputDataToolStripMenuItem,
            this.optionToolStripMenuItem,
            this.settingConfigurationToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1184, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sMSBlastToolStripMenuItem,
            this.inputDigitalInfoToolStripMenuItem,
            this.validasiKartuUjianToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            this.menuToolStripMenuItem.Click += new System.EventHandler(this.menuToolStripMenuItem_Click);
            // 
            // sMSBlastToolStripMenuItem
            // 
            this.sMSBlastToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dosenToolStripMenuItem,
            this.mahasiswaToolStripMenuItem,
            this.orangTuaToolStripMenuItem,
            this.mahasiswaDTLToolStripMenuItem});
            this.sMSBlastToolStripMenuItem.Name = "sMSBlastToolStripMenuItem";
            this.sMSBlastToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.sMSBlastToolStripMenuItem.Text = "SMS Blast";
            // 
            // dosenToolStripMenuItem
            // 
            this.dosenToolStripMenuItem.Name = "dosenToolStripMenuItem";
            this.dosenToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.dosenToolStripMenuItem.Text = "Dosen";
            this.dosenToolStripMenuItem.Click += new System.EventHandler(this.dosenToolStripMenuItem_Click);
            // 
            // mahasiswaToolStripMenuItem
            // 
            this.mahasiswaToolStripMenuItem.Name = "mahasiswaToolStripMenuItem";
            this.mahasiswaToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.mahasiswaToolStripMenuItem.Text = "Mahasiswa";
            this.mahasiswaToolStripMenuItem.Click += new System.EventHandler(this.mahasiswaToolStripMenuItem_Click);
            // 
            // orangTuaToolStripMenuItem
            // 
            this.orangTuaToolStripMenuItem.Name = "orangTuaToolStripMenuItem";
            this.orangTuaToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.orangTuaToolStripMenuItem.Text = "Orang Tua";
            // 
            // mahasiswaDTLToolStripMenuItem
            // 
            this.mahasiswaDTLToolStripMenuItem.Name = "mahasiswaDTLToolStripMenuItem";
            this.mahasiswaDTLToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.mahasiswaDTLToolStripMenuItem.Text = "Mahasiswa DTL";
            // 
            // inputDigitalInfoToolStripMenuItem
            // 
            this.inputDigitalInfoToolStripMenuItem.Name = "inputDigitalInfoToolStripMenuItem";
            this.inputDigitalInfoToolStripMenuItem.Size = new System.Drawing.Size(163, 22);
            this.inputDigitalInfoToolStripMenuItem.Text = "Input Digital Info";
            this.inputDigitalInfoToolStripMenuItem.Click += new System.EventHandler(this.inputDigitalInfoToolStripMenuItem_Click);
            // 
            // inputDataToolStripMenuItem
            // 
            this.inputDataToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.pembayaranSPPDPPToolStripMenuItem,
            this.klasifikasiBayaranToolStripMenuItem});
            this.inputDataToolStripMenuItem.Name = "inputDataToolStripMenuItem";
            this.inputDataToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.inputDataToolStripMenuItem.Text = "Pembayaran";
            // 
            // pembayaranSPPDPPToolStripMenuItem
            // 
            this.pembayaranSPPDPPToolStripMenuItem.Name = "pembayaranSPPDPPToolStripMenuItem";
            this.pembayaranSPPDPPToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.pembayaranSPPDPPToolStripMenuItem.Text = "SPP dan DPP";
            this.pembayaranSPPDPPToolStripMenuItem.Click += new System.EventHandler(this.pembayaranSPPDPPToolStripMenuItem_Click);
            // 
            // klasifikasiBayaranToolStripMenuItem
            // 
            this.klasifikasiBayaranToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.distribusiMahasiswaToolStripMenuItem,
            this.olahDataKlasifikasiToolStripMenuItem});
            this.klasifikasiBayaranToolStripMenuItem.Name = "klasifikasiBayaranToolStripMenuItem";
            this.klasifikasiBayaranToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.klasifikasiBayaranToolStripMenuItem.Text = "Klasifikasi Pembayaran";
            // 
            // distribusiMahasiswaToolStripMenuItem
            // 
            this.distribusiMahasiswaToolStripMenuItem.Name = "distribusiMahasiswaToolStripMenuItem";
            this.distribusiMahasiswaToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.distribusiMahasiswaToolStripMenuItem.Text = "Distribusi Mahasiswa";
            this.distribusiMahasiswaToolStripMenuItem.Click += new System.EventHandler(this.distribusiMahasiswaToolStripMenuItem_Click);
            // 
            // olahDataKlasifikasiToolStripMenuItem
            // 
            this.olahDataKlasifikasiToolStripMenuItem.Name = "olahDataKlasifikasiToolStripMenuItem";
            this.olahDataKlasifikasiToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.olahDataKlasifikasiToolStripMenuItem.Text = "Olah Data Klasifikasi";
            
            // 
            // optionToolStripMenuItem
            // 
            this.optionToolStripMenuItem.Name = "optionToolStripMenuItem";
            this.optionToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.optionToolStripMenuItem.Text = "Option";
            this.optionToolStripMenuItem.Click += new System.EventHandler(this.optionToolStripMenuItem_Click);
            // 
            // settingConfigurationToolStripMenuItem
            // 
            this.settingConfigurationToolStripMenuItem.Name = "settingConfigurationToolStripMenuItem";
            this.settingConfigurationToolStripMenuItem.Size = new System.Drawing.Size(133, 20);
            this.settingConfigurationToolStripMenuItem.Text = "Setting Configuration";
            this.settingConfigurationToolStripMenuItem.Click += new System.EventHandler(this.settingConfigurationToolStripMenuItem_Click);
            // 
            // validasiKartuUjianToolStripMenuItem
            // 
            this.validasiKartuUjianToolStripMenuItem.Name = "validasiKartuUjianToolStripMenuItem";
            this.validasiKartuUjianToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.validasiKartuUjianToolStripMenuItem.Text = "Validasi Kartu Ujian";
            this.validasiKartuUjianToolStripMenuItem.Click += new System.EventHandler(this.validasiKartuUjianToolStripMenuItem_Click);
            // 
            // MainKeuanganForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 733);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainKeuanganForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainFormKeuangan";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainKeuanganForm_FormClosing);
            this.Load += new System.EventHandler(this.MainFormKeuangan_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sMSBlastToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dosenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mahasiswaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orangTuaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingConfigurationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mahasiswaDTLToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inputDataToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pembayaranSPPDPPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inputDigitalInfoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem klasifikasiBayaranToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem distribusiMahasiswaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem olahDataKlasifikasiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem validasiKartuUjianToolStripMenuItem;
    }
}