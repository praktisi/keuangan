﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Mockup_Pembayaran_Mhs_New;
using MainFormKeuangan;

namespace SMSGatewayHonorDosen
{
    public partial class Form_DistribusiPembayaranMahasiswa : Form
    {
        JoyashariSON js = new JoyashariSON();
        OlahDataKeuangan odk = new OlahDataKeuangan();

        DataTable mainDataTable;

        List<string> angkatanDalamGrid = new List<string>();
        List<KategoriPembayaran> kategoriBayar = new List<KategoriPembayaran>();
        List<DataKeuangan> dkList = new List<DataKeuangan>();

        public Form_DistribusiPembayaranMahasiswa()
        {
            InitializeComponent();
            
            //SiapinDataGrid();
            PerbaharuiDataDiGridDariDB();
            
            // isi variable kategori bayar
            kategoriBayar = GetAllKategoriBayar();

            IsiComboBoxKategori(kategoriBayar);

            SelectedCurrentRow();            
        }

        private void PerbaharuiDataDiGridDariDB()
        {
            dkList = GetAllDataKategoriPembayaranVSMahasiswa();            
            InputDataKeDataGridView(dkList);
            
            // bikin variable penampung untuk main datatable
            mainDataTable = GetDataGridToDataTable(dgv_listdistribusipembayaranmahasiswa);
        }

        private List<string> DistinctAngkatanFromGrid()
        {
            return dgv_listdistribusipembayaranmahasiswa.Rows.Cast<DataGridViewRow>().Select(x => x.Cells[4].Value.ToString()).Distinct().ToList();            
        }

        private void IsiComboBoxKategori(List<KategoriPembayaran> a)
        {
            cbox_kategori.Items.Clear();

            cbox_kategori.Items.Add(" ");
            foreach (var item in a)
            {
                cbox_kategori.Items.Add(item.namaKategori);
            }
        }

        private void InputDataKeDataGridView(List<DataKeuangan> dkl)
        {
            
            dgv_listdistribusipembayaranmahasiswa.DataSource = null;

            SiapinDataGrid();

            dgv_listdistribusipembayaranmahasiswa.Rows.Clear();
            //while(dgv_listdistribusipembayaranmahasiswa.Rows.Count > 0)
            //{
            //    dgv_listdistribusipembayaranmahasiswa.Rows.RemoveAt(0);
            //}

            foreach (DataKeuangan item in dkl)
            {
                dgv_listdistribusipembayaranmahasiswa.Rows.Add(item.NPM, item.NamaMhs, item.Kelas, item.Jurusan,
                    item.Angkatan, item.Kategori, item.FotoMhs);
            }

            angkatanDalamGrid = DistinctAngkatanFromGrid();

            foreach (DataGridViewRow item in this.dgv_listdistribusipembayaranmahasiswa.Rows)
            {
                for(int i = 0; i < item.Cells.Count;i++)
                {
                    if(item.Cells[5].Value == null || item.Cells[5].Value == DBNull.Value ||
                        String.IsNullOrWhiteSpace(item.Cells[5].Value.ToString()))
                    {
                        item.Cells[5].Value = " ";
                    }
                }
            }
        }

        private List<DataKeuangan> GetAllDataKategoriPembayaranVSMahasiswa()
        {
            return js.ParsingJSONStringDataKategoriKeuVSMhs(odk.ListKatPembayaranVSMahasiswa("http://" + Constanta.server + "/SisfoPraktisi/php/keuangan/ambildatakategoripembayaranseluruhmahasiswa.php"));                       
        }

        private List<KategoriPembayaran> GetAllKategoriBayar()
        {
            return js.ParsingJSONStringDataKategoriKeu(odk.ListKategoriPembayaran("http://" + Constanta.server + "/SisfoPraktisi/php/keuangan/ambilkategoribayar.php"));
        }

        private void DistribusiPembayaranMahasiswa_FormClosing(object sender, FormClosingEventArgs e)
        {
            MainKeuanganForm.isFormAnak = false;
        }

        private void SiapinDataGrid()
        {
            if(dgv_listdistribusipembayaranmahasiswa.ColumnCount == 0 )
                dgv_listdistribusipembayaranmahasiswa.ColumnCount = 7;

            dgv_listdistribusipembayaranmahasiswa.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgv_listdistribusipembayaranmahasiswa.AllowUserToAddRows = false;
            dgv_listdistribusipembayaranmahasiswa.AllowUserToDeleteRows = false;
            dgv_listdistribusipembayaranmahasiswa.AllowUserToResizeColumns = false;
            dgv_listdistribusipembayaranmahasiswa.AllowUserToResizeRows = false;
            dgv_listdistribusipembayaranmahasiswa.ReadOnly = true;
            dgv_listdistribusipembayaranmahasiswa.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            
            dgv_listdistribusipembayaranmahasiswa.ColumnHeadersDefaultCellStyle.Font = new System.Drawing.Font("Calibri", 11);
            dgv_listdistribusipembayaranmahasiswa.RowHeadersVisible = false;

            dgv_listdistribusipembayaranmahasiswa.Columns[0].Name = "NPM";
            dgv_listdistribusipembayaranmahasiswa.Columns[0].Width = 20;
            dgv_listdistribusipembayaranmahasiswa.Columns[1].Name = "NAMA";
            dgv_listdistribusipembayaranmahasiswa.Columns[1].Visible = false;
            dgv_listdistribusipembayaranmahasiswa.Columns[2].Name = "KELAS";
            dgv_listdistribusipembayaranmahasiswa.Columns[2].Visible = false;
            dgv_listdistribusipembayaranmahasiswa.Columns[3].Name = "JURUSAN";
            dgv_listdistribusipembayaranmahasiswa.Columns[3].Visible = false;
            dgv_listdistribusipembayaranmahasiswa.Columns[4].Name = "ANGKATAN";
            dgv_listdistribusipembayaranmahasiswa.Columns[4].Visible = false;
            dgv_listdistribusipembayaranmahasiswa.Columns[5].Name = "KATEGORI";
            dgv_listdistribusipembayaranmahasiswa.Columns[6].Name = "FOTO";
            dgv_listdistribusipembayaranmahasiswa.Columns[6].Visible = false;

            foreach (DataGridViewColumn c in dgv_listdistribusipembayaranmahasiswa.Columns)
            {
                c.DefaultCellStyle.ForeColor = Color.Black;
                c.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                c.DefaultCellStyle.Font = new Font("Tahoma", 12f, GraphicsUnit.Pixel);
            }
        }

        // input perubahan kategori ke database
        private void btn_updatemahasiswakategoribayar_Click(object sender, EventArgs e)
        {
            if (cbox_kategori.SelectedIndex > 0)
            {
                if (dgv_listdistribusipembayaranmahasiswa.SelectedRows[0].Cells[5].Value.ToString() == " ")
                {
                    if (MessageBox.Show("Apakah anda yakin akan melakukan input data untuk mahasiswa dengan NPM " +
                        txtbox_npmmhs.Text + " atas nama " + txtbox_nama.Text + " ?", "Validasi Admin",
                         MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        string x = Microsoft.VisualBasic.Interaction.InputBox("Kode Admin: ", "Validasi Kode Admin");
                        string y = Microsoft.VisualBasic.Interaction.InputBox("Password Admin: ", "Validasi Password Admin");

                        if (x == "galih" && y == "ar")
                        {
                            // input ke database dengan kode karyawan
                            //MessageBox.Show("Data kategori pembayaran mahasiswa sudah diinput");
                            string kd = kategoriBayar[cbox_kategori.SelectedIndex - 1].kodeKategori;
                            string nik = Form_Login.nik;
                            string _npm = txtbox_npmmhs.Text;                            

                            //MessageBox.Show("kode kat: " + kd + 
                            //    "; npm: " + txtbox_npmmhs.Text +
                            //    "; nik: " + nik);
                            //MessageBox.Show(js.CreateJSONUpdateDataKategoriKeuanganVSMahasiswa(_npm, kd, nik));

                            if (odk.UploadPerubahanKategoriPembayaranMahasiswa(_npm, kd, nik, "http://" + Constanta.server + "/SisfoPraktisi/php/keuangan/insertkategoripembayaranmahasiswa.php"))
                            {
                                MessageBox.Show("SUCCESS. " + odk.MessageKeu);
                                PerbaharuiDataDiGridDariDB();
                                SelectedCurrentRow();
                            }
                            else
                            {
                                MessageBox.Show("ERROR. " + odk.MessageKeu);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Kode atau Password Admin salah");
                        }
                    }
                }
                else
                {
                    if (MessageBox.Show("Apakah anda yakin akan melakukan perubahan data untuk mahasiswa dengan NPM " +
                        txtbox_npmmhs.Text + " atas nama " + txtbox_nama.Text + " ?", "Validasi Admin",
                         MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        string x = Microsoft.VisualBasic.Interaction.InputBox("Kode Admin: ", "Validasi Kode Admin");
                        string y = Microsoft.VisualBasic.Interaction.InputBox("Password Admin: ", "Validasi Password Admin");

                        if (x == "galih" && y == "ar")
                        {
                            // input ke database dengan kode karyawan
                            //MessageBox.Show("Data kategori pembayaran mahasiswa sudah diubah");
                            string kd = kategoriBayar[cbox_kategori.SelectedIndex - 1].kodeKategori;
                            string nik = Form_Login.nik;
                            string _npm = txtbox_npmmhs.Text;

                            if (odk.UploadPerubahanKategoriPembayaranMahasiswa(_npm, kd, nik, "http://" + Constanta.server + "/SisfoPraktisi/php/keuangan/updatekategoripembayaranmahasiswa.php"))
                            {
                                MessageBox.Show("SUCCESS. " + odk.MessageKeu);
                                PerbaharuiDataDiGridDariDB();
                                SelectedCurrentRow();
                            }
                            else
                            {
                                MessageBox.Show("ERROR. " + odk.MessageKeu);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Kode atau Password Admin salah");
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Isi terlebih dahulu kategori");
            }

            SelectedCurrentRow();
        }

        private void SelectedCurrentRow()
        {
            string npm = dgv_listdistribusipembayaranmahasiswa.SelectedRows[0].Cells[0].Value.ToString();
            string nam = dgv_listdistribusipembayaranmahasiswa.SelectedRows[0].Cells[1].Value.ToString();
            string kel = dgv_listdistribusipembayaranmahasiswa.SelectedRows[0].Cells[2].Value.ToString();
            string jur = dgv_listdistribusipembayaranmahasiswa.SelectedRows[0].Cells[3].Value.ToString();
            string ang = dgv_listdistribusipembayaranmahasiswa.SelectedRows[0].Cells[4].Value.ToString();
            string k = dgv_listdistribusipembayaranmahasiswa.SelectedRows[0].Cells[5].Value.ToString();
            string foto = dgv_listdistribusipembayaranmahasiswa.SelectedRows[0].Cells[6].Value.ToString();

            IsiDataMahasiswa(npm, nam, kel, jur, ang, k, foto);
        }

        private void dgv_listdistribusipembayaranmahasiswa_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectedCurrentRow();
        }

        private void IsiDataMahasiswa(string npm, string nama, string kelas, string jur, string ang, string kat, string foto)
        {
            txtbox_npmmhs.Text = npm;
            txtbox_nama.Text = nama;
            txtbox_kelas.Text = kelas;
            txtbox_jurusan.Text = jur;
            txtbox_angkatan.Text = ang;
            cbox_kategori.SelectedItem = kat;
            pcbox_foto.ImageLocation = "http://" + Constanta.server + foto.Substring(9);
        }

        private void btn_search_Click(object sender, EventArgs e)
        {
            CariNPM(txtbox_npmsearch.Text, 
                mainDataTable, dgv_listdistribusipembayaranmahasiswa);
            
            // clear textbox search NPM
            txtbox_npmsearch.Clear();
        }

        private void CariNPM(string _npm, DataTable dt, DataGridView dgv)
        {
            try
            {
                string npm = _npm;

                string namaKolomNPM = dgv.Columns[0].Name;

                DataView dv = new DataView(dt);

                dv.RowFilter = string.Format(namaKolomNPM + " LIKE '%{0}%'", npm);
                //dv.RowFilter = string.Format(namaKolomANG + " LIKE '%{0}%'", ang);

                dgv.Columns.Clear();
                dgv.DataSource = dv;

                SiapinDataGrid();
                SelectedCurrentRow();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            
        }

        private DataTable GetDataGridToDataTable(DataGridView dg)
        {
            DataTable dt = new DataTable();

            // insert column
            foreach (DataGridViewColumn col in dg.Columns)
            {
                dt.Columns.Add(col.HeaderText);
            }

            // adding row
            foreach (DataGridViewRow row in dg.Rows)
            {
                DataRow dRows = dt.NewRow();

                foreach (DataGridViewCell cell in row.Cells)
                {
                    dRows[cell.ColumnIndex] = cell.Value;
                }

                dt.Rows.Add(dRows);
            }

            return dt;
        }

    }
}
