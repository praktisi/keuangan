﻿namespace Aplikasi_Main_Akademik
{
    partial class Form_Input_Digi_Info
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Btn_Hapus = new System.Windows.Forms.Button();
            this.Btn_Publish = new System.Windows.Forms.Button();
            this.DGV_DaftarPengumuman = new System.Windows.Forms.DataGridView();
            this.Lbl_Bagian = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Btn_Tambah = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.RtBox_Pengumuman = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_DaftarPengumuman)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Btn_Hapus);
            this.groupBox1.Controls.Add(this.Btn_Publish);
            this.groupBox1.Controls.Add(this.DGV_DaftarPengumuman);
            this.groupBox1.Location = new System.Drawing.Point(373, 14);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(537, 468);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Daftar Pengumuman";
            // 
            // Btn_Hapus
            // 
            this.Btn_Hapus.Location = new System.Drawing.Point(348, 421);
            this.Btn_Hapus.Name = "Btn_Hapus";
            this.Btn_Hapus.Size = new System.Drawing.Size(88, 39);
            this.Btn_Hapus.TabIndex = 4;
            this.Btn_Hapus.Text = "Hapus";
            this.Btn_Hapus.UseVisualStyleBackColor = true;
            this.Btn_Hapus.Click += new System.EventHandler(this.Btn_Hapus_Click);
            // 
            // Btn_Publish
            // 
            this.Btn_Publish.Location = new System.Drawing.Point(442, 421);
            this.Btn_Publish.Name = "Btn_Publish";
            this.Btn_Publish.Size = new System.Drawing.Size(88, 39);
            this.Btn_Publish.TabIndex = 5;
            this.Btn_Publish.Text = "Publish";
            this.Btn_Publish.UseVisualStyleBackColor = true;
            this.Btn_Publish.Click += new System.EventHandler(this.Btn_Publish_Click);
            // 
            // DGV_DaftarPengumuman
            // 
            this.DGV_DaftarPengumuman.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_DaftarPengumuman.Location = new System.Drawing.Point(7, 27);
            this.DGV_DaftarPengumuman.Name = "DGV_DaftarPengumuman";
            this.DGV_DaftarPengumuman.Size = new System.Drawing.Size(523, 388);
            this.DGV_DaftarPengumuman.TabIndex = 0;
            this.DGV_DaftarPengumuman.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_DaftarPengumuman_CellClick);
            // 
            // Lbl_Bagian
            // 
            this.Lbl_Bagian.AutoSize = true;
            this.Lbl_Bagian.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_Bagian.Location = new System.Drawing.Point(12, 9);
            this.Lbl_Bagian.Name = "Lbl_Bagian";
            this.Lbl_Bagian.Size = new System.Drawing.Size(144, 25);
            this.Lbl_Bagian.TabIndex = 1;
            this.Lbl_Bagian.Text = "Pengumuman";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Btn_Tambah);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.RtBox_Pengumuman);
            this.groupBox2.Location = new System.Drawing.Point(13, 39);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(352, 443);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tambah Pengumuman";
            // 
            // Btn_Tambah
            // 
            this.Btn_Tambah.Location = new System.Drawing.Point(7, 396);
            this.Btn_Tambah.Name = "Btn_Tambah";
            this.Btn_Tambah.Size = new System.Drawing.Size(88, 39);
            this.Btn_Tambah.TabIndex = 3;
            this.Btn_Tambah.Text = "Tambah";
            this.Btn_Tambah.UseVisualStyleBackColor = true;
            this.Btn_Tambah.Click += new System.EventHandler(this.Btn_Tambah_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Pengumuman";
            // 
            // RtBox_Pengumuman
            // 
            this.RtBox_Pengumuman.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RtBox_Pengumuman.Location = new System.Drawing.Point(7, 47);
            this.RtBox_Pengumuman.Name = "RtBox_Pengumuman";
            this.RtBox_Pengumuman.Size = new System.Drawing.Size(338, 343);
            this.RtBox_Pengumuman.TabIndex = 0;
            this.RtBox_Pengumuman.Text = "";
            // 
            // Form_Utama
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(923, 524);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.Lbl_Bagian);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form_Utama";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Utama_FormClosed);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_DaftarPengumuman)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView DGV_DaftarPengumuman;
        private System.Windows.Forms.Button Btn_Hapus;
        private System.Windows.Forms.Button Btn_Publish;
        private System.Windows.Forms.Label Lbl_Bagian;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button Btn_Tambah;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox RtBox_Pengumuman;
    }
}

