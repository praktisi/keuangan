﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aplikasi_Main_Akademik;

namespace Mockup_Pembayaran_Mhs_New
{
    class Mahasiswa
    {
        #region Att And Prop        
        private string npm;
        public string NPM { get { return npm; } set { npm = value; } }

        private Pegawai pegawai = new Pegawai();
        public Pegawai Pegawai { get { return pegawai; } set { pegawai = value; } }

        private KategoriPembayaran katBayar = new KategoriPembayaran();
        public KategoriPembayaran KatBayar { get { return katBayar; } set { katBayar = value; } }

        private string nama;
        public string NamaMhs { get { return nama; } set { nama = value; } }

        private string semester;
        public string Semester { get { return semester; } set { semester = value; } }

        private string kelas;
        public string Kelas { get { return kelas; } set { kelas = value; } }

        private string jurusan;
        public string Jurusan { get { return jurusan; } set { jurusan = value; } }

        private string angkatan;
        public string Angkatan { get { return angkatan; } set { angkatan = value; } }

        private string cicilanke;
        public string CicilanKe { get { return cicilanke; } set { cicilanke = value; } }

        private string fotomhs;
        public string FotoMhs { get { return fotomhs; } set { fotomhs = value; } }

        private string piutangTiapSemester; // sisa yang harus dibayar
        public string PiutangTiapSemester { get { return piutangTiapSemester; } set { piutangTiapSemester = value; } }

        private List<DataKeuangan> datakeu = new List<DataKeuangan>();
        public List<DataKeuangan> DataKeu { get { return datakeu; } set { datakeu = value; } }
        #endregion

        #region Constructor
        public Mahasiswa()
        {

        }

        public Mahasiswa(string _npm, string _nama, string _kelas, 
            string _jurusan, string _angkatan, string _cicilanke, string _foto)
        {
            npm = _npm;
            nama = _nama;
            kelas = _kelas;
            jurusan = _jurusan;
            angkatan = _angkatan;
            cicilanke = _cicilanke;
            fotomhs = _foto;
        }

        #endregion
    }
}
