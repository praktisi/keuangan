﻿namespace SMSGatewayHonorDosen
{
    partial class smsgatewayortu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(smsgatewayortu));
            this.lsbox_tidakadaHP = new System.Windows.Forms.ListBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lstBox_noHp = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_Kirim = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.rchBox_TextSms = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbBox_Kelas = new System.Windows.Forms.ComboBox();
            this.cmbBox_Jurusan = new System.Windows.Forms.ComboBox();
            this.cmbBox_Angkatan = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.checkbox_opsi2 = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_Hapus = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_InputNoHP = new System.Windows.Forms.Button();
            this.txtBox_NoHp = new System.Windows.Forms.TextBox();
            this.listbox_NoHp = new System.Windows.Forms.ListBox();
            this.rbtn_opsi2 = new System.Windows.Forms.RadioButton();
            this.rbtn_opsi1 = new System.Windows.Forms.RadioButton();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.lbl_StatusStrip = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // lsbox_tidakadaHP
            // 
            this.lsbox_tidakadaHP.FormattingEnabled = true;
            this.lsbox_tidakadaHP.Location = new System.Drawing.Point(653, 364);
            this.lsbox_tidakadaHP.Name = "lsbox_tidakadaHP";
            this.lsbox_tidakadaHP.Size = new System.Drawing.Size(229, 251);
            this.lsbox_tidakadaHP.TabIndex = 38;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(653, 348);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(141, 13);
            this.label8.TabIndex = 37;
            this.label8.Text = "NPM Tidak Ada Nomor HP :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(653, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 36;
            this.label7.Text = "Nomor HP :";
            // 
            // lstBox_noHp
            // 
            this.lstBox_noHp.FormattingEnabled = true;
            this.lstBox_noHp.Location = new System.Drawing.Point(653, 57);
            this.lstBox_noHp.Name = "lstBox_noHp";
            this.lstBox_noHp.Size = new System.Drawing.Size(229, 277);
            this.lstBox_noHp.TabIndex = 35;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn_Kirim);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.rchBox_TextSms);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(246, 304);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(389, 315);
            this.groupBox3.TabIndex = 34;
            this.groupBox3.TabStop = false;
            // 
            // btn_Kirim
            // 
            this.btn_Kirim.Location = new System.Drawing.Point(269, 276);
            this.btn_Kirim.Name = "btn_Kirim";
            this.btn_Kirim.Size = new System.Drawing.Size(102, 33);
            this.btn_Kirim.TabIndex = 6;
            this.btn_Kirim.Text = "Kirim";
            this.btn_Kirim.UseVisualStyleBackColor = true;
            this.btn_Kirim.Click += new System.EventHandler(this.btn_Kirim_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Text SMS :";
            // 
            // rchBox_TextSms
            // 
            this.rchBox_TextSms.Location = new System.Drawing.Point(12, 48);
            this.rchBox_TextSms.MaxLength = 150;
            this.rchBox_TextSms.Name = "rchBox_TextSms";
            this.rchBox_TextSms.Size = new System.Drawing.Size(360, 222);
            this.rchBox_TextSms.TabIndex = 0;
            this.rchBox_TextSms.Text = "";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbBox_Kelas);
            this.groupBox2.Controls.Add(this.cmbBox_Jurusan);
            this.groupBox2.Controls.Add(this.cmbBox_Angkatan);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.checkbox_opsi2);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(246, 134);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(389, 174);
            this.groupBox2.TabIndex = 39;
            this.groupBox2.TabStop = false;
            // 
            // cmbBox_Kelas
            // 
            this.cmbBox_Kelas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBox_Kelas.FormattingEnabled = true;
            this.cmbBox_Kelas.Location = new System.Drawing.Point(100, 125);
            this.cmbBox_Kelas.Name = "cmbBox_Kelas";
            this.cmbBox_Kelas.Size = new System.Drawing.Size(248, 28);
            this.cmbBox_Kelas.TabIndex = 0;
            this.cmbBox_Kelas.SelectedIndexChanged += new System.EventHandler(this.cmbBox_Kelas_SelectedIndexChanged);
            // 
            // cmbBox_Jurusan
            // 
            this.cmbBox_Jurusan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBox_Jurusan.FormattingEnabled = true;
            this.cmbBox_Jurusan.Location = new System.Drawing.Point(100, 91);
            this.cmbBox_Jurusan.Name = "cmbBox_Jurusan";
            this.cmbBox_Jurusan.Size = new System.Drawing.Size(248, 28);
            this.cmbBox_Jurusan.TabIndex = 1;
            this.cmbBox_Jurusan.SelectedIndexChanged += new System.EventHandler(this.cmbBox_Jurusan_SelectedIndexChanged);
            // 
            // cmbBox_Angkatan
            // 
            this.cmbBox_Angkatan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBox_Angkatan.FormattingEnabled = true;
            this.cmbBox_Angkatan.Location = new System.Drawing.Point(100, 56);
            this.cmbBox_Angkatan.Name = "cmbBox_Angkatan";
            this.cmbBox_Angkatan.Size = new System.Drawing.Size(248, 28);
            this.cmbBox_Angkatan.TabIndex = 8;
            this.cmbBox_Angkatan.SelectedIndexChanged += new System.EventHandler(this.cmbBox_Angkatan_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 20);
            this.label6.TabIndex = 7;
            this.label6.Text = "Kelas :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 20);
            this.label5.TabIndex = 6;
            this.label5.Text = "Jurusan :";
            // 
            // checkbox_opsi2
            // 
            this.checkbox_opsi2.AutoSize = true;
            this.checkbox_opsi2.Enabled = false;
            this.checkbox_opsi2.Location = new System.Drawing.Point(17, 22);
            this.checkbox_opsi2.Name = "checkbox_opsi2";
            this.checkbox_opsi2.Size = new System.Drawing.Size(289, 24);
            this.checkbox_opsi2.TabIndex = 5;
            this.checkbox_opsi2.Text = "Centang, Jika akan memilih satu opsi";
            this.checkbox_opsi2.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(86, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Angkatan :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_Hapus);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btn_InputNoHP);
            this.groupBox1.Controls.Add(this.txtBox_NoHp);
            this.groupBox1.Controls.Add(this.listbox_NoHp);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(10, 134);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(208, 485);
            this.groupBox1.TabIndex = 40;
            this.groupBox1.TabStop = false;
            // 
            // btn_Hapus
            // 
            this.btn_Hapus.Location = new System.Drawing.Point(15, 446);
            this.btn_Hapus.Name = "btn_Hapus";
            this.btn_Hapus.Size = new System.Drawing.Size(180, 33);
            this.btn_Hapus.TabIndex = 0;
            this.btn_Hapus.Text = "Hapus";
            this.btn_Hapus.UseVisualStyleBackColor = true;
            this.btn_Hapus.Click += new System.EventHandler(this.btn_Hapus_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Daftar No HP :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "No HP :";
            // 
            // btn_InputNoHP
            // 
            this.btn_InputNoHP.Location = new System.Drawing.Point(15, 83);
            this.btn_InputNoHP.Name = "btn_InputNoHP";
            this.btn_InputNoHP.Size = new System.Drawing.Size(108, 28);
            this.btn_InputNoHP.TabIndex = 1;
            this.btn_InputNoHP.Text = "Input";
            this.btn_InputNoHP.UseVisualStyleBackColor = true;
            this.btn_InputNoHP.Click += new System.EventHandler(this.btn_InputNoHP_Click);
            // 
            // txtBox_NoHp
            // 
            this.txtBox_NoHp.Location = new System.Drawing.Point(15, 48);
            this.txtBox_NoHp.Name = "txtBox_NoHp";
            this.txtBox_NoHp.Size = new System.Drawing.Size(180, 26);
            this.txtBox_NoHp.TabIndex = 0;
            // 
            // listbox_NoHp
            // 
            this.listbox_NoHp.FormattingEnabled = true;
            this.listbox_NoHp.ItemHeight = 20;
            this.listbox_NoHp.Location = new System.Drawing.Point(15, 156);
            this.listbox_NoHp.Name = "listbox_NoHp";
            this.listbox_NoHp.Size = new System.Drawing.Size(180, 284);
            this.listbox_NoHp.TabIndex = 3;
            // 
            // rbtn_opsi2
            // 
            this.rbtn_opsi2.AutoSize = true;
            this.rbtn_opsi2.Checked = true;
            this.rbtn_opsi2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_opsi2.Location = new System.Drawing.Point(263, 113);
            this.rbtn_opsi2.Name = "rbtn_opsi2";
            this.rbtn_opsi2.Size = new System.Drawing.Size(72, 24);
            this.rbtn_opsi2.TabIndex = 41;
            this.rbtn_opsi2.TabStop = true;
            this.rbtn_opsi2.Text = "Opsi 2";
            this.rbtn_opsi2.UseVisualStyleBackColor = true;
            this.rbtn_opsi2.CheckedChanged += new System.EventHandler(this.rbtn_opsi2_CheckedChanged);
            // 
            // rbtn_opsi1
            // 
            this.rbtn_opsi1.AutoSize = true;
            this.rbtn_opsi1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtn_opsi1.Location = new System.Drawing.Point(25, 113);
            this.rbtn_opsi1.Name = "rbtn_opsi1";
            this.rbtn_opsi1.Size = new System.Drawing.Size(72, 24);
            this.rbtn_opsi1.TabIndex = 42;
            this.rbtn_opsi1.TabStop = true;
            this.rbtn_opsi1.Text = "Opsi 1";
            this.rbtn_opsi1.UseVisualStyleBackColor = true;
            this.rbtn_opsi1.CheckedChanged += new System.EventHandler(this.rbtn_opsi1_CheckedChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(95, 57);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(464, 25);
            this.label15.TabIndex = 44;
            this.label15.Text = "PEMBAYARAN UAS/UTS UNTUK ORANG TUA";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(95, 18);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(477, 29);
            this.label14.TabIndex = 33;
            this.label14.Text = "FORM SMS GATEWAY PEMBERITAHUAN";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(11, 15);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(78, 78);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 43;
            this.pictureBox1.TabStop = false;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lbl_StatusStrip});
            this.statusStrip.Location = new System.Drawing.Point(0, 634);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(894, 22);
            this.statusStrip.TabIndex = 45;
            this.statusStrip.Text = "statusStrip1";
            // 
            // lbl_StatusStrip
            // 
            this.lbl_StatusStrip.Name = "lbl_StatusStrip";
            this.lbl_StatusStrip.Size = new System.Drawing.Size(39, 17);
            this.lbl_StatusStrip.Text = "Ready";
            // 
            // smsgatewayortu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(894, 656);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.lsbox_tidakadaHP);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lstBox_noHp);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.rbtn_opsi2);
            this.Controls.Add(this.rbtn_opsi1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Name = "smsgatewayortu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SMS GATEWAY ORANG TUA";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.smsgatewayortu_FormClosed);
            this.Load += new System.EventHandler(this.smsgatewayortu_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lsbox_tidakadaHP;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox lstBox_noHp;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_Kirim;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox rchBox_TextSms;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cmbBox_Kelas;
        private System.Windows.Forms.ComboBox cmbBox_Jurusan;
        private System.Windows.Forms.ComboBox cmbBox_Angkatan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkbox_opsi2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_Hapus;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_InputNoHP;
        private System.Windows.Forms.TextBox txtBox_NoHp;
        private System.Windows.Forms.ListBox listbox_NoHp;
        private System.Windows.Forms.RadioButton rbtn_opsi2;
        private System.Windows.Forms.RadioButton rbtn_opsi1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel lbl_StatusStrip;
    }
}