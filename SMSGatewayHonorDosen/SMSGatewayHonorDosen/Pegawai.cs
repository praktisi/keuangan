﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using MySql.Data.MySqlClient;

namespace Aplikasi_Main_Akademik
{
    class Pegawai
    {
        #region Attribut

        string nik;
        string nama;
        string bagian;
        string password;
        #endregion

        #region Properties

        public string NIK
        {
            get
            {
                return nik;
            }
            set
            {
                nik = value;
            }
        }

        public string Nama
        {
            get
            {
                return nama;
            }
            set
            {
                nama = value;
            }
        }

        public string Bagian
        {
            get
            {
                return bagian;
            }
            set
            {
                bagian = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }

        #endregion

        #region Method

        public bool InisialisasiPasswordNIK(string nik_, string pwd)
        {
            bool isPwdNikTerisi = false;
            if(nik_ != "" && pwd !="")
            {
                NIK = nik_;
                Password = pwd;
                isPwdNikTerisi = true;
            }
            return isPwdNikTerisi;
        }

        public bool Login(MySqlConnection cnn, MySqlCommand com, MySqlDataReader reader)
        {
            bool isPwdBenar = false;

            string query = "SELECT * FROM tb_pegawai WHERE nik_pegawai='" + NIK + "' AND password='" + Password + "'";
            com = new MySqlCommand(query, cnn);

            try
            {
                cnn.Open();

                DataTable dt = new DataTable();
                reader = com.ExecuteReader();
                
                if (reader.Read())
                {
                    Nama = reader[1].ToString();
                    NIK = reader[0].ToString();
                    isPwdBenar = true;
                }

                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return isPwdBenar;
        }

        public string GetBagian(MySqlConnection cnn, MySqlCommand com, MySqlDataReader reader)
        {
            string nmBag = "";
            string query = "SELECT tb_distpegawai.kode_bagian, tb_bagian.nama_bagian FROM tb_distpegawai"
                + " INNER JOIN tb_bagian ON tb_distpegawai.kode_bagian=tb_bagian.kode_bagian"
                + " WHERE nik_pegawai='" + NIK + "'";
            com = new MySqlCommand(query, cnn);

            try
            {
                cnn.Open();

                reader = com.ExecuteReader();
                if (reader.Read())
                {
                    Bagian = reader["kode_bagian"].ToString();
                    nmBag = reader["nama_bagian"].ToString();
                }

                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return nmBag;
        }

        public bool InsertPengumuman(MySqlConnection cnn, MySqlCommand com,
            string pengumuman, string statusTampil, DateTime tglTampil)
        {
            bool isInserted = false;

            string query = "INSERT INTO tb_digitalinfo(pengumuman, kode_bagian, status_tampil, tanggal_tampil, nik_pegawai)"
                + " VALUES(@pengumuman, @kdBag, @statTampil, @tglTampil, @nik)";
            com = new MySqlCommand(query, cnn);

            com.Parameters.AddWithValue("@pengumuman", pengumuman);
            com.Parameters.AddWithValue("@kdBag", Bagian);
            com.Parameters.AddWithValue("@statTampil", statusTampil);
            com.Parameters.AddWithValue("@tglTampil", tglTampil);
            com.Parameters.AddWithValue("@nik", NIK);

            try
            {
                cnn.Open();

                if (com.ExecuteNonQuery() > 0)
                {
                    isInserted = true;
                }

                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return isInserted;
        }

        public void GetPengumuman(MySqlConnection cnn, MySqlCommand com, MySqlDataAdapter adt,
            DataGridView dgv)
        {
            dgv.Rows.Clear();
            List<string> status = new List<string>();

            string query = "SELECT pengumuman, tanggal_tampil, status_tampil, id_info FROM tb_digitalinfo"
                + " WHERE kode_bagian='" + Bagian + "' ORDER BY id_info ASC";
            com = new MySqlCommand(query, cnn);

            try
            {
                cnn.Open();

                DataTable dt = new DataTable();
                adt = new MySqlDataAdapter(com);
                adt.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    dgv.Rows.Add("", r[0].ToString(), r[1].ToString(), r[3].ToString());
                    status.Add(r[2].ToString());
                }

                for (int i = 0; i < dgv.RowCount; i++)
                {
                    dgv.Rows[i].Cells[0].Value = i + 1;
                    dgv.Rows[i].Cells[4].Value = status[i];
                }

                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public bool UpdateStatusTampil(MySqlConnection cnn, MySqlCommand com, MySqlDataAdapter adt,
            string stsTampil, string idInfo)
        {
            bool isUpdated = false;

            string query = "UPDATE tb_digitalinfo SET status_tampil='" + stsTampil + "' WHERE id_info = '" + idInfo + "'";
            com = new MySqlCommand(query, cnn);

            try
            {
                cnn.Open();

                adt = new MySqlDataAdapter(com);
                adt.UpdateCommand = cnn.CreateCommand();
                adt.UpdateCommand.CommandText = query;

                if (com.ExecuteNonQuery() > 0)
                {
                    isUpdated = true;
                }

                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return isUpdated;
        }

        public bool DeletePengumuman(MySqlConnection cnn, MySqlCommand com, MySqlDataAdapter adt,
            string idInfo)
        {
            bool isDeleted = false;

            string query = "DELETE FROM tb_digitalinfo WHERE id_info = '" + idInfo + "'";
            com = new MySqlCommand(query, cnn);

            try
            {
                cnn.Open();

                adt = new MySqlDataAdapter(com);
                adt.UpdateCommand = cnn.CreateCommand();
                adt.UpdateCommand.CommandText = query;

                if (com.ExecuteNonQuery() > 0)
                {
                    isDeleted = true;
                }

                cnn.Close();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

            return isDeleted;
        }

        #endregion

        #region Constructor
        public Pegawai()
        {

        }
        public Pegawai(string _nik)
        {
            nik = _nik;
        }
        public Pegawai(string _nik, string _nama)
        {
            nik = _nik;
            nama = _nama;
        }
        #endregion
    }
}
