﻿namespace Aplikasi_Main_Akademik
{
    partial class Form_Cek_Kuisioner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Lbl_StatusPengisian = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Btn_Cek = new System.Windows.Forms.Button();
            this.TxtBox_NPM = new System.Windows.Forms.TextBox();
            this.btn_printLangsung = new System.Windows.Forms.Button();
            this.btn_Preview = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Lbl_StatusPengisian);
            this.groupBox1.Location = new System.Drawing.Point(13, 57);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(289, 92);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Status Pengisian";
            // 
            // Lbl_StatusPengisian
            // 
            this.Lbl_StatusPengisian.AutoSize = true;
            this.Lbl_StatusPengisian.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Lbl_StatusPengisian.Location = new System.Drawing.Point(7, 43);
            this.Lbl_StatusPengisian.Name = "Lbl_StatusPengisian";
            this.Lbl_StatusPengisian.Size = new System.Drawing.Size(16, 24);
            this.Lbl_StatusPengisian.TabIndex = 0;
            this.Lbl_StatusPengisian.Text = "-";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Input NPM";
            // 
            // Btn_Cek
            // 
            this.Btn_Cek.Location = new System.Drawing.Point(208, 12);
            this.Btn_Cek.Name = "Btn_Cek";
            this.Btn_Cek.Size = new System.Drawing.Size(90, 37);
            this.Btn_Cek.TabIndex = 2;
            this.Btn_Cek.Text = "Cek";
            this.Btn_Cek.UseVisualStyleBackColor = true;
            this.Btn_Cek.Click += new System.EventHandler(this.Btn_Cek_Click);
            // 
            // TxtBox_NPM
            // 
            this.TxtBox_NPM.Location = new System.Drawing.Point(102, 17);
            this.TxtBox_NPM.MaxLength = 7;
            this.TxtBox_NPM.Name = "TxtBox_NPM";
            this.TxtBox_NPM.Size = new System.Drawing.Size(100, 26);
            this.TxtBox_NPM.TabIndex = 1;
            this.TxtBox_NPM.TextChanged += new System.EventHandler(this.TxtBox_NPM_TextChanged);
            // 
            // btn_printLangsung
            // 
            this.btn_printLangsung.Enabled = false;
            this.btn_printLangsung.Location = new System.Drawing.Point(12, 157);
            this.btn_printLangsung.Name = "btn_printLangsung";
            this.btn_printLangsung.Size = new System.Drawing.Size(286, 43);
            this.btn_printLangsung.TabIndex = 4;
            this.btn_printLangsung.Text = "Cetak Kartu Ujian";
            this.btn_printLangsung.UseVisualStyleBackColor = true;
            this.btn_printLangsung.Click += new System.EventHandler(this.btn_printLangsung_Click);
            // 
            // btn_Preview
            // 
            this.btn_Preview.Enabled = false;
            this.btn_Preview.Location = new System.Drawing.Point(12, 206);
            this.btn_Preview.Name = "btn_Preview";
            this.btn_Preview.Size = new System.Drawing.Size(286, 43);
            this.btn_Preview.TabIndex = 5;
            this.btn_Preview.Text = "Preview";
            this.btn_Preview.UseVisualStyleBackColor = true;
            this.btn_Preview.Click += new System.EventHandler(this.btn_Preview_Click);
            // 
            // Form_Cek_Kuisioner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 259);
            this.Controls.Add(this.btn_Preview);
            this.Controls.Add(this.btn_printLangsung);
            this.Controls.Add(this.TxtBox_NPM);
            this.Controls.Add(this.Btn_Cek);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form_Cek_Kuisioner";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form Cek Kuisioner";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form_Cek_Kuisioner_FormClosed);
            this.Load += new System.EventHandler(this.Form_Cek_Kuisioner_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label Lbl_StatusPengisian;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Btn_Cek;
        private System.Windows.Forms.TextBox TxtBox_NPM;
        private System.Windows.Forms.Button btn_printLangsung;
        private System.Windows.Forms.Button btn_Preview;
    }
}