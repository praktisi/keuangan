﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SMSGatewayHonorDosen
{
    class OperasiString
    {
        string inputText;        
        public string InputText { get { return inputText; } set { inputText = value; } }

        // method
        public string InputTextUang(string source)
        {
            string penambah = ",";
            List<int> urutan = new List<int>();
            string finalText = source;
            
            int perulangan = source.Length / 3;

            urutan = CariIndexKoma(perulangan, source.Length);
            
            for (int i = 0; i < urutan.Count; i++)
            {
                string ujung = finalText.Substring(urutan[i]);
                string depan = finalText.Substring(0, urutan[i]);

                finalText = depan + penambah + ujung;
            }
            
            return finalText + ".00";
        }
        private List<int> CariIndexKoma(int ulang, int panjangString)
        {
            List<int> l = new List<int>();

            for (int i = 0; i < ulang; i++)
            {
                panjangString -= 3;
                
                if (panjangString > 0)
                    l.Add(panjangString);
            }

            return l;
        }
    }
}
