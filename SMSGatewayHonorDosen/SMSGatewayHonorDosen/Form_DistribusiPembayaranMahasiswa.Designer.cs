﻿namespace SMSGatewayHonorDosen
{
    partial class Form_DistribusiPembayaranMahasiswa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.btn_search = new System.Windows.Forms.Button();
            this.txtbox_npmsearch = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.dgv_listdistribusipembayaranmahasiswa = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbox_kategori = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btn_updatemahasiswakategoribayar = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pcbox_foto = new System.Windows.Forms.PictureBox();
            this.txtbox_npmmhs = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtbox_angkatan = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtbox_jurusan = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtbox_kelas = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtbox_nama = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listdistribusipembayaranmahasiswa)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbox_foto)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label1.Location = new System.Drawing.Point(40, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(704, 36);
            this.label1.TabIndex = 1;
            this.label1.Text = "DISTRIBUSI KATEGORI PEMBAYARAN MAHASISWA   V 0.0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.label2.Location = new System.Drawing.Point(221, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(320, 33);
            this.label2.TabIndex = 2;
            this.label2.Text = "Politeknik Praktisi Bandung";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.SteelBlue;
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.btn_search);
            this.groupBox2.Controls.Add(this.txtbox_npmsearch);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.dgv_listdistribusipembayaranmahasiswa);
            this.groupBox2.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Aqua;
            this.groupBox2.Location = new System.Drawing.Point(17, 120);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(332, 281);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "List Distribusi Pembayaran Mahasiswa ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(123, 43);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(13, 19);
            this.label15.TabIndex = 6;
            this.label15.Text = ":";
            // 
            // btn_search
            // 
            this.btn_search.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.btn_search.BackgroundImage = global::SMSGatewayHonorDosen.Properties.Resources.search_btn;
            this.btn_search.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_search.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_search.Location = new System.Drawing.Point(272, 26);
            this.btn_search.Name = "btn_search";
            this.btn_search.Size = new System.Drawing.Size(50, 52);
            this.btn_search.TabIndex = 3;
            this.btn_search.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_search.UseVisualStyleBackColor = false;
            this.btn_search.Click += new System.EventHandler(this.btn_search_Click);
            // 
            // txtbox_npmsearch
            // 
            this.txtbox_npmsearch.Location = new System.Drawing.Point(142, 40);
            this.txtbox_npmsearch.Name = "txtbox_npmsearch";
            this.txtbox_npmsearch.Size = new System.Drawing.Size(124, 27);
            this.txtbox_npmsearch.TabIndex = 2;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(6, 43);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(106, 19);
            this.label14.TabIndex = 3;
            this.label14.Text = "Search by NPM";
            // 
            // dgv_listdistribusipembayaranmahasiswa
            // 
            this.dgv_listdistribusipembayaranmahasiswa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_listdistribusipembayaranmahasiswa.Location = new System.Drawing.Point(6, 84);
            this.dgv_listdistribusipembayaranmahasiswa.Name = "dgv_listdistribusipembayaranmahasiswa";
            this.dgv_listdistribusipembayaranmahasiswa.Size = new System.Drawing.Size(316, 191);
            this.dgv_listdistribusipembayaranmahasiswa.TabIndex = 0;
            this.dgv_listdistribusipembayaranmahasiswa.TabStop = false;
            this.dgv_listdistribusipembayaranmahasiswa.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_listdistribusipembayaranmahasiswa_CellClick);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.SteelBlue;
            this.groupBox1.Controls.Add(this.cbox_kategori);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.btn_updatemahasiswakategoribayar);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.txtbox_npmmhs);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.txtbox_angkatan);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtbox_jurusan);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtbox_kelas);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtbox_nama);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Aqua;
            this.groupBox1.Location = new System.Drawing.Point(355, 120);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(432, 281);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informasi Mahasiswa";
            // 
            // cbox_kategori
            // 
            this.cbox_kategori.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbox_kategori.FormattingEnabled = true;
            this.cbox_kategori.Location = new System.Drawing.Point(110, 188);
            this.cbox_kategori.Name = "cbox_kategori";
            this.cbox_kategori.Size = new System.Drawing.Size(161, 27);
            this.cbox_kategori.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(89, 191);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(15, 21);
            this.label7.TabIndex = 21;
            this.label7.Text = ":";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(6, 190);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 21);
            this.label8.TabIndex = 20;
            this.label8.Text = "KATEGORI";
            // 
            // btn_updatemahasiswakategoribayar
            // 
            this.btn_updatemahasiswakategoribayar.ForeColor = System.Drawing.Color.Teal;
            this.btn_updatemahasiswakategoribayar.Location = new System.Drawing.Point(10, 237);
            this.btn_updatemahasiswakategoribayar.Name = "btn_updatemahasiswakategoribayar";
            this.btn_updatemahasiswakategoribayar.Size = new System.Drawing.Size(410, 27);
            this.btn_updatemahasiswakategoribayar.TabIndex = 5;
            this.btn_updatemahasiswakategoribayar.Text = "Update Ke Database";
            this.btn_updatemahasiswakategoribayar.UseVisualStyleBackColor = true;
            this.btn_updatemahasiswakategoribayar.Click += new System.EventHandler(this.btn_updatemahasiswakategoribayar_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.pcbox_foto);
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(280, 22);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(140, 193);
            this.groupBox4.TabIndex = 18;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Foto";
            // 
            // pcbox_foto
            // 
            this.pcbox_foto.Location = new System.Drawing.Point(7, 18);
            this.pcbox_foto.Name = "pcbox_foto";
            this.pcbox_foto.Size = new System.Drawing.Size(127, 169);
            this.pcbox_foto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbox_foto.TabIndex = 8;
            this.pcbox_foto.TabStop = false;
            // 
            // txtbox_npmmhs
            // 
            this.txtbox_npmmhs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtbox_npmmhs.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_npmmhs.Location = new System.Drawing.Point(110, 32);
            this.txtbox_npmmhs.Name = "txtbox_npmmhs";
            this.txtbox_npmmhs.ReadOnly = true;
            this.txtbox_npmmhs.Size = new System.Drawing.Size(161, 26);
            this.txtbox_npmmhs.TabIndex = 17;
            this.txtbox_npmmhs.TabStop = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(89, 155);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(15, 21);
            this.label23.TabIndex = 16;
            this.label23.Text = ":";
            // 
            // txtbox_angkatan
            // 
            this.txtbox_angkatan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtbox_angkatan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_angkatan.Location = new System.Drawing.Point(110, 155);
            this.txtbox_angkatan.Name = "txtbox_angkatan";
            this.txtbox_angkatan.ReadOnly = true;
            this.txtbox_angkatan.Size = new System.Drawing.Size(161, 26);
            this.txtbox_angkatan.TabIndex = 15;
            this.txtbox_angkatan.TabStop = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(6, 156);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(88, 21);
            this.label24.TabIndex = 14;
            this.label24.Text = "ANGKATAN";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(89, 123);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(15, 21);
            this.label13.TabIndex = 13;
            this.label13.Text = ":";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(89, 93);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 21);
            this.label12.TabIndex = 12;
            this.label12.Text = ":";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(89, 63);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(15, 21);
            this.label11.TabIndex = 11;
            this.label11.Text = ":";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(89, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 21);
            this.label10.TabIndex = 10;
            this.label10.Text = ":";
            // 
            // txtbox_jurusan
            // 
            this.txtbox_jurusan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtbox_jurusan.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_jurusan.Location = new System.Drawing.Point(110, 124);
            this.txtbox_jurusan.Name = "txtbox_jurusan";
            this.txtbox_jurusan.ReadOnly = true;
            this.txtbox_jurusan.Size = new System.Drawing.Size(161, 26);
            this.txtbox_jurusan.TabIndex = 7;
            this.txtbox_jurusan.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(6, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "JURUSAN ";
            // 
            // txtbox_kelas
            // 
            this.txtbox_kelas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtbox_kelas.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_kelas.Location = new System.Drawing.Point(110, 93);
            this.txtbox_kelas.Name = "txtbox_kelas";
            this.txtbox_kelas.ReadOnly = true;
            this.txtbox_kelas.Size = new System.Drawing.Size(161, 26);
            this.txtbox_kelas.TabIndex = 5;
            this.txtbox_kelas.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(6, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 21);
            this.label3.TabIndex = 4;
            this.label3.Text = "KELAS ";
            // 
            // txtbox_nama
            // 
            this.txtbox_nama.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtbox_nama.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_nama.Location = new System.Drawing.Point(110, 65);
            this.txtbox_nama.Name = "txtbox_nama";
            this.txtbox_nama.ReadOnly = true;
            this.txtbox_nama.Size = new System.Drawing.Size(161, 22);
            this.txtbox_nama.TabIndex = 3;
            this.txtbox_nama.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(6, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 21);
            this.label5.TabIndex = 2;
            this.label5.Text = "NAMA ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(6, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 21);
            this.label6.TabIndex = 0;
            this.label6.Text = "NPM";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.textBox1.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.ForeColor = System.Drawing.SystemColors.Info;
            this.textBox1.Location = new System.Drawing.Point(0, 422);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(800, 21);
            this.textBox1.TabIndex = 4;
            this.textBox1.Text = "D  E  V  E  L  O  P  E  D    B  Y    S  I  S  F  O    2  0  1  7 ";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // Form_DistribusiPembayaranMahasiswa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.RoyalBlue;
            this.ClientSize = new System.Drawing.Size(800, 451);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form_DistribusiPembayaranMahasiswa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DistribusiPembayaranMahasiswa_FormClosing);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_listdistribusipembayaranmahasiswa)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcbox_foto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_updatemahasiswakategoribayar;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.PictureBox pcbox_foto;
        private System.Windows.Forms.TextBox txtbox_npmmhs;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtbox_angkatan;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtbox_jurusan;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtbox_kelas;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtbox_nama;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cbox_kategori;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dgv_listdistribusipembayaranmahasiswa;
        private System.Windows.Forms.Button btn_search;
        private System.Windows.Forms.TextBox txtbox_npmsearch;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox1;
    }
}