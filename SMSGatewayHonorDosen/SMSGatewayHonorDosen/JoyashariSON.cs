﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Aplikasi_Main_Akademik;

namespace Mockup_Pembayaran_Mhs_New
{
    
    class JoyashariSON
    {
        Mahasiswa mhs = new Mahasiswa();
        DataKeuangan keu = new DataKeuangan();
        KategoriPembayaran katBay = new KategoriPembayaran();
        Pegawai peg = new Pegawai();
        public JoyashariSON()
        {

        }

        #region Method
        public string CreateJSONStringNPM(string npmSource)
        {
            mhs.NPM = npmSource;

            //string JSONOutput = ser.Serialize(mhs);
            string JSONOutput = JsonConvert.SerializeObject(mhs);
            
            return JSONOutput;
        }

        public string CreateJSONInputKeu(string npm, string tgl, string tglBank, int sem, int cic, int bay, string kat)
        {
            keu.NPM = npm;
            keu.TanggalBayar = DateTime.Parse(tgl);
            keu.TanggalBank = DateTime.ParseExact(tglBank, "yyyy-MM-dd" ,System.Globalization.CultureInfo.InvariantCulture);
            keu.SemesterPembayaran = sem.ToString();
            keu.CicilanKe = cic.ToString();            
            keu.Pembayaran = bay.ToString();
            keu.Kategori = kat;

            //string json = ser.Serialize(keu);
            string json = JsonConvert.SerializeObject(keu);

            return json;
        }

        public string CreateJSONUpdateDataKategoriKeuanganVSMahasiswa(string npm, string kat, string nik)
        {
            peg.NIK = nik;
            katBay.kodeKategori = kat;

            mhs.Pegawai.NIK = peg.NIK;
            mhs.KatBayar.kodeKategori = katBay.kodeKategori;
            mhs.NPM = npm;

            string json = JsonConvert.SerializeObject(mhs);
            //System.Windows.Forms.MessageBox.Show("json: " + json);
            return json;
        }

        public List<Mahasiswa> ParsingJSONString(string source)
        {
            List<Mahasiswa> m = new List<Mahasiswa>();
            m = JsonConvert.DeserializeObject<List<Mahasiswa>>(source);
            return m;
        }

        public List<DataKeuangan> ParsingJSONStringInfoDataKeuUmumBasedOnClass(string source)
        {
            //List<DataKeuangan> dk = ser.Deserialize<List<DataKeuangan>>(source);
            List<DataKeuangan> dk = JsonConvert.DeserializeObject<List<DataKeuangan>>(source);
            return dk;
        }

        public List<DataKeuangan> ParsingJSONStringDataKategoriKeuVSMhs(string source)
        {
            List<DataKeuangan> dk = JsonConvert.DeserializeObject<List<DataKeuangan>>(source);
            return dk;
        }

        public List<KategoriPembayaran> ParsingJSONStringDataKategoriKeu(string source)
        {
            List<KategoriPembayaran> kat = JsonConvert.DeserializeObject<List<KategoriPembayaran>>(source);
            return kat;
        }

        #endregion
    }
}
