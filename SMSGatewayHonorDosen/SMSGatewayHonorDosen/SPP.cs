﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Collections.Specialized;

namespace Mockup_Pembayaran_Mhs_New
{
    class SPP
    {
        #region Attribut
        private string message;
        private string messageKeu;
        #endregion

        #region Properties
        public string Message { get { return message; } private set { message = value; } }
        public string MessageKeu { get { return messageKeu; } set { messageKeu = value; } }
        #endregion

        #region Method
        public bool NPMChecking(string npm, string url)
        {
            bool check = false;

            WebClient wc = new WebClient();
            NameValueCollection nvc = new NameValueCollection();
            
            // create json
            JoyashariSON j = new JoyashariSON();
            string strForJSON = j.CreateJSONStringNPM(npm);

            nvc["NPM"] = strForJSON;            

            byte[] responBytes = wc.UploadValues(url, "POST", nvc);
            string responFromServer = Encoding.UTF8.GetString(responBytes);

            if( responFromServer == "ga ada")
            {
                // 
            }
            else if(responFromServer.Contains("error in connection"))
            {
                //
            }
            else
            {                
                check = true;
            }

            Message = responFromServer;
           
            return check;
        }

        public string ListPembayaran(string npm, string url)
        {
            WebClient wc = new WebClient();
            NameValueCollection nvc = new NameValueCollection();

            // create json
            JoyashariSON j = new JoyashariSON();
            string strForJSON = j.CreateJSONStringNPM(npm);

            nvc["NPM"] = strForJSON;
            byte[] bytes = wc.UploadValues(url, "POST", nvc);
            string respServer = Encoding.UTF8.GetString(bytes);

            //System.Windows.Forms.MessageBox.Show("resp: " + respServer);

            if (respServer == "")
                messageKeu = "Tidak Ada Riwayat Pembayaran";

            return respServer;
        }

        public bool UploadPembayaran(string npm, string d, string dBank, int sem, int cic, int bayar, 
            string katBayar, string url)
        {
            bool b = false;

            WebClient w = new WebClient();
            NameValueCollection nvc = new NameValueCollection();
            JoyashariSON js = new JoyashariSON();

            string j = js.CreateJSONInputKeu(npm, d, dBank, sem, cic, bayar, katBayar);
            System.Windows.Forms.MessageBox.Show("j: " + j);
            nvc["datakeu"] = j;
            
            byte[] upSer = w.UploadValues(url, "POST", nvc);
            string respServer = Encoding.UTF8.GetString(upSer);

            if (respServer.Contains("data pembayaran telah berhasil"))
            {
                b = true;
            }

            MessageKeu = respServer;

            return b;
        }
        #endregion
    }
}