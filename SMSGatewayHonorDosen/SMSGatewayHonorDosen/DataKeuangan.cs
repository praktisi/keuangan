﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Aplikasi_Main_Akademik;

namespace Mockup_Pembayaran_Mhs_New
{
    class KategoriPembayaran
    {
        public string kodeKategori;
        public string namaKategori;
        public string persentaseBayar;
    }

    class DataKeuangan : Mahasiswa
    {
        private string resi;                // nomor resi
        private string cicilanke;           // cicilan ke berapa
        private string semesterPembayaran;  // semester ke berapa pembayaran
        private string pembayaran;          // besar pembayaran
        private string dpp;
        private string kategori;            // dpp atau spp
        private DateTime tanggalBayar;      // tanggal melakukan pembayaran ke bagian keuangan
        private DateTime tanggalBank;       // tanggal melakukan transfer ke bank        

        public string DPP { get { return dpp; } set { dpp = value; } }
        public string Kategori { get { return kategori; } set { kategori = value; } }
        public string SemesterPembayaran { get { return semesterPembayaran; } set { semesterPembayaran = value; } }
        public string Resi { get { return resi; } set { resi = value; } }
        public DateTime TanggalBayar { get { return tanggalBayar; } set { tanggalBayar = value; } }
        public DateTime TanggalBank { get { return tanggalBank; } set { tanggalBank = value; } }
        public string CicilanKe { get { return cicilanke; } set { cicilanke = value; } }
        public string Pembayaran { get { return pembayaran; } set { pembayaran = value; } }        

        #region Constructor
        public DataKeuangan()
        {

        }
        public DataKeuangan(string _resi, string _cicilanke, string _semester, 
            string _pembayaran, DateTime _tgl, DateTime _tglBank)
        {            
            resi = _resi;
            cicilanke = _cicilanke;
            semesterPembayaran = _semester;
            pembayaran = _pembayaran;            
            tanggalBayar = _tgl;
            tanggalBayar = _tglBank;
        }        
        #endregion

        #region Method
        public int GetDataCurrentCicilan(int cicilanCurr, int rowsCount)
        {
            int cicke = 1;

            if( rowsCount > 0)
            {
                cicke = cicilanCurr + 1;
            }
            return cicke;
        }
        
        #endregion
    }
}
