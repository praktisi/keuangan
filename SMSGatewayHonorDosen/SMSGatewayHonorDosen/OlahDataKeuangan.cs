﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Collections.Specialized;

namespace Mockup_Pembayaran_Mhs_New
{
    class OlahDataKeuangan
    {
        #region Attribut
        private string message;
        private string messageKeu;
        #endregion

        #region Properties
        public string Message { get { return message; } private set { message = value; } }
        public string MessageKeu { get { return messageKeu; } set { messageKeu = value; } }
        #endregion

        #region Method
        public bool NPMChecking(string npm, string url)
        {
            bool check = false;

            WebClient wc = new WebClient();
            NameValueCollection nvc = new NameValueCollection();
            
            // create json
            JoyashariSON j = new JoyashariSON();
            string strForJSON = j.CreateJSONStringNPM(npm);
            //System.Windows.Forms.MessageBox.Show("" + strForJSON);
            nvc["NPM"] = strForJSON;

            try
            {
                byte[] responBytes = wc.UploadValues(url, "POST", nvc);
                string responFromServer = Encoding.UTF8.GetString(responBytes);

                if (responFromServer.Contains("data tidak ditemukan"))
                {
                    // 
                }
                else if (responFromServer.Contains("error in connection"))
                {
                    //
                }
                else
                {
                    check = true;
                }

                Message = responFromServer;
            
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
            //System.Windows.Forms.MessageBox.Show("cek: " + check);
            return check;
        }

        public string ListKatPembayaranVSMahasiswa(string url)
        {
            WebClient wc = new WebClient();
            
            string respServer = wc.DownloadString(url);

            if (respServer == "")
                messageKeu = "Tidak Ada Data";

            return respServer;
        }

        public string ListKategoriPembayaran(string url)
        {
            WebClient wc = new WebClient();

            string respServer = wc.DownloadString(url);

            if(respServer == "")
                messageKeu = "Tidak Ada Data";

            return respServer;
        }

        public string ListPembayaran(string npm, string url)
        {
            WebClient wc = new WebClient();
            NameValueCollection nvc = new NameValueCollection();

            // create json
            JoyashariSON j = new JoyashariSON();
            string strForJSON = j.CreateJSONStringNPM(npm);

            nvc["NPM"] = strForJSON;
            byte[] bytes = wc.UploadValues(url, "POST", nvc);
            string respServer = Encoding.UTF8.GetString(bytes);

            if (respServer == "")
                messageKeu = "Tidak Ada Riwayat Pembayaran";

            return respServer;
        }

        public bool UploadPembayaran(string npm, string d, string dBank, int sem, int cic, int bayar, 
            string katBayar, string url)
        {
            bool b = false;

            WebClient w = new WebClient();
            NameValueCollection nvc = new NameValueCollection();
            JoyashariSON js = new JoyashariSON();

            string j = js.CreateJSONInputKeu(npm, d, dBank, sem, cic, bayar, katBayar);
            
            nvc["datakeu"] = j;

            try
            {
                byte[] upSer = w.UploadValues(url, "POST", nvc);
                string respServer = Encoding.UTF8.GetString(upSer);

                if (respServer.Contains("data pembayaran telah berhasil"))
                {
                    b = true;
                }

                MessageKeu = respServer;

            }
            catch (Exception ex)
            {
                MessageKeu = ex.Message;
            }
            
            return b;
        }

        public bool UploadPerubahanKategoriPembayaranMahasiswa(string _npm, string _kategori, string _nik, string url)
        {
            bool b = false;

            WebClient wc = new WebClient();
            NameValueCollection nvc = new NameValueCollection();
            // json object
            JoyashariSON json = new JoyashariSON();

            string j = json.CreateJSONUpdateDataKategoriKeuanganVSMahasiswa(_npm, _kategori, _nik);

            nvc["katmahbayar"] = j;

            try
            {
                byte[] upSer = wc.UploadValues(url, "POST", nvc);
                string responServer = Encoding.UTF8.GetString(upSer);

                if (responServer.Contains("BERHASIL"))
                {
                    b = true;
                }

                MessageKeu = responServer;
            }
            catch (Exception ex)
            {
                MessageKeu = ex.Message;
            }
            

            return b;
        }
        #endregion
    }
}