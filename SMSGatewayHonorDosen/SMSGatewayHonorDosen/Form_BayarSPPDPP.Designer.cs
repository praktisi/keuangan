﻿namespace SMSGatewayHonorDosen
{
    partial class Form_BayarSPPDPP
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pcbox_foto = new System.Windows.Forms.PictureBox();
            this.txtbox_npmmhs = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtbox_angkatan = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtbox_jurusan = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtbox_kelas = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtbox_nama = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dgv_historyPembayaran = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.gbox_pembayaran = new System.Windows.Forms.GroupBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtbox_sisaBayar = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.txtbox_status = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.cbox_semester = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.rbtn_dpp = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.rbtn_spp = new System.Windows.Forms.RadioButton();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtbox_besarbayaran = new System.Windows.Forms.TextBox();
            this.lbl_bayar = new System.Windows.Forms.Label();
            this.txtbox_cicilanke = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_inputbayar = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.txtbox_cekNPM = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.btn_cariNPM = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbox_foto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_historyPembayaran)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.gbox_pembayaran.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.txtbox_npmmhs);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.txtbox_angkatan);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.txtbox_jurusan);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txtbox_kelas);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtbox_nama);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Coral;
            this.groupBox1.Location = new System.Drawing.Point(16, 75);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(448, 201);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informasi Mahasiswa";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.pcbox_foto);
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(309, 13);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(133, 182);
            this.groupBox4.TabIndex = 18;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Foto";
            // 
            // pcbox_foto
            // 
            this.pcbox_foto.Location = new System.Drawing.Point(7, 18);
            this.pcbox_foto.Name = "pcbox_foto";
            this.pcbox_foto.Size = new System.Drawing.Size(120, 158);
            this.pcbox_foto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbox_foto.TabIndex = 8;
            this.pcbox_foto.TabStop = false;
            // 
            // txtbox_npmmhs
            // 
            this.txtbox_npmmhs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtbox_npmmhs.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_npmmhs.Location = new System.Drawing.Point(120, 32);
            this.txtbox_npmmhs.Name = "txtbox_npmmhs";
            this.txtbox_npmmhs.ReadOnly = true;
            this.txtbox_npmmhs.Size = new System.Drawing.Size(176, 22);
            this.txtbox_npmmhs.TabIndex = 17;
            this.txtbox_npmmhs.TabStop = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(93, 156);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(15, 21);
            this.label23.TabIndex = 16;
            this.label23.Text = ":";
            // 
            // txtbox_angkatan
            // 
            this.txtbox_angkatan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtbox_angkatan.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_angkatan.Location = new System.Drawing.Point(120, 155);
            this.txtbox_angkatan.Name = "txtbox_angkatan";
            this.txtbox_angkatan.ReadOnly = true;
            this.txtbox_angkatan.Size = new System.Drawing.Size(176, 22);
            this.txtbox_angkatan.TabIndex = 15;
            this.txtbox_angkatan.TabStop = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.White;
            this.label24.Location = new System.Drawing.Point(6, 156);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(88, 21);
            this.label24.TabIndex = 14;
            this.label24.Text = "ANGKATAN";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.White;
            this.label13.Location = new System.Drawing.Point(93, 122);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(15, 21);
            this.label13.TabIndex = 13;
            this.label13.Text = ":";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.White;
            this.label12.Location = new System.Drawing.Point(93, 92);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 21);
            this.label12.TabIndex = 12;
            this.label12.Text = ":";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(93, 61);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(15, 21);
            this.label11.TabIndex = 11;
            this.label11.Text = ":";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(93, 33);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(15, 21);
            this.label10.TabIndex = 10;
            this.label10.Text = ":";
            // 
            // txtbox_jurusan
            // 
            this.txtbox_jurusan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtbox_jurusan.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_jurusan.Location = new System.Drawing.Point(120, 124);
            this.txtbox_jurusan.Name = "txtbox_jurusan";
            this.txtbox_jurusan.ReadOnly = true;
            this.txtbox_jurusan.Size = new System.Drawing.Size(176, 22);
            this.txtbox_jurusan.TabIndex = 7;
            this.txtbox_jurusan.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(6, 125);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "JURUSAN ";
            // 
            // txtbox_kelas
            // 
            this.txtbox_kelas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtbox_kelas.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_kelas.Location = new System.Drawing.Point(120, 93);
            this.txtbox_kelas.Name = "txtbox_kelas";
            this.txtbox_kelas.ReadOnly = true;
            this.txtbox_kelas.Size = new System.Drawing.Size(176, 22);
            this.txtbox_kelas.TabIndex = 5;
            this.txtbox_kelas.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(6, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 21);
            this.label3.TabIndex = 4;
            this.label3.Text = "KELAS ";
            // 
            // txtbox_nama
            // 
            this.txtbox_nama.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtbox_nama.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_nama.Location = new System.Drawing.Point(120, 63);
            this.txtbox_nama.Name = "txtbox_nama";
            this.txtbox_nama.ReadOnly = true;
            this.txtbox_nama.Size = new System.Drawing.Size(176, 22);
            this.txtbox_nama.TabIndex = 3;
            this.txtbox_nama.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(6, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "NAMA ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(6, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "NPM";
            // 
            // dgv_historyPembayaran
            // 
            this.dgv_historyPembayaran.AllowUserToResizeColumns = false;
            this.dgv_historyPembayaran.AllowUserToResizeRows = false;
            this.dgv_historyPembayaran.BackgroundColor = System.Drawing.Color.White;
            this.dgv_historyPembayaran.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dgv_historyPembayaran.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv_historyPembayaran.EnableHeadersVisualStyles = false;
            this.dgv_historyPembayaran.Location = new System.Drawing.Point(6, 27);
            this.dgv_historyPembayaran.MultiSelect = false;
            this.dgv_historyPembayaran.Name = "dgv_historyPembayaran";
            this.dgv_historyPembayaran.ReadOnly = true;
            this.dgv_historyPembayaran.Size = new System.Drawing.Size(626, 398);
            this.dgv_historyPembayaran.TabIndex = 1;
            this.dgv_historyPembayaran.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgv_historyPembayaran);
            this.groupBox2.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.groupBox2.Location = new System.Drawing.Point(479, 105);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(638, 444);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data History Pembayaran";
            // 
            // gbox_pembayaran
            // 
            this.gbox_pembayaran.Controls.Add(this.dateTimePicker1);
            this.gbox_pembayaran.Controls.Add(this.label31);
            this.gbox_pembayaran.Controls.Add(this.label30);
            this.gbox_pembayaran.Controls.Add(this.label29);
            this.gbox_pembayaran.Controls.Add(this.txtbox_sisaBayar);
            this.gbox_pembayaran.Controls.Add(this.label27);
            this.gbox_pembayaran.Controls.Add(this.label28);
            this.gbox_pembayaran.Controls.Add(this.txtbox_status);
            this.gbox_pembayaran.Controls.Add(this.label15);
            this.gbox_pembayaran.Controls.Add(this.label6);
            this.gbox_pembayaran.Controls.Add(this.cbox_semester);
            this.gbox_pembayaran.Controls.Add(this.label20);
            this.gbox_pembayaran.Controls.Add(this.rbtn_dpp);
            this.gbox_pembayaran.Controls.Add(this.label8);
            this.gbox_pembayaran.Controls.Add(this.rbtn_spp);
            this.gbox_pembayaran.Controls.Add(this.label18);
            this.gbox_pembayaran.Controls.Add(this.label17);
            this.gbox_pembayaran.Controls.Add(this.label16);
            this.gbox_pembayaran.Controls.Add(this.label14);
            this.gbox_pembayaran.Controls.Add(this.label9);
            this.gbox_pembayaran.Controls.Add(this.txtbox_besarbayaran);
            this.gbox_pembayaran.Controls.Add(this.lbl_bayar);
            this.gbox_pembayaran.Controls.Add(this.txtbox_cicilanke);
            this.gbox_pembayaran.Controls.Add(this.label5);
            this.gbox_pembayaran.Controls.Add(this.btn_inputbayar);
            this.gbox_pembayaran.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbox_pembayaran.ForeColor = System.Drawing.Color.SpringGreen;
            this.gbox_pembayaran.Location = new System.Drawing.Point(16, 278);
            this.gbox_pembayaran.Name = "gbox_pembayaran";
            this.gbox_pembayaran.Size = new System.Drawing.Size(448, 271);
            this.gbox_pembayaran.TabIndex = 4;
            this.gbox_pembayaran.TabStop = false;
            this.gbox_pembayaran.Text = "Pembayaran";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Enabled = false;
            this.dateTimePicker1.Location = new System.Drawing.Point(132, 153);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(231, 27);
            this.dateTimePicker1.TabIndex = 31;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.White;
            this.label31.Location = new System.Drawing.Point(112, 155);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(15, 21);
            this.label31.TabIndex = 30;
            this.label31.Text = ":";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.White;
            this.label30.Location = new System.Drawing.Point(11, 155);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(94, 21);
            this.label30.TabIndex = 29;
            this.label30.Text = "Tgl. Tfr Bank";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.White;
            this.label29.Location = new System.Drawing.Point(128, 123);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(30, 19);
            this.label29.TabIndex = 28;
            this.label29.Text = "Rp.";
            // 
            // txtbox_sisaBayar
            // 
            this.txtbox_sisaBayar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtbox_sisaBayar.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_sisaBayar.Location = new System.Drawing.Point(159, 123);
            this.txtbox_sisaBayar.Name = "txtbox_sisaBayar";
            this.txtbox_sisaBayar.ReadOnly = true;
            this.txtbox_sisaBayar.Size = new System.Drawing.Size(116, 27);
            this.txtbox_sisaBayar.TabIndex = 27;
            this.txtbox_sisaBayar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.ForeColor = System.Drawing.Color.White;
            this.label27.Location = new System.Drawing.Point(112, 122);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(15, 21);
            this.label27.TabIndex = 26;
            this.label27.Text = ":";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.White;
            this.label28.Location = new System.Drawing.Point(12, 122);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(80, 21);
            this.label28.TabIndex = 25;
            this.label28.Text = "Sisa Bayar";
            // 
            // txtbox_status
            // 
            this.txtbox_status.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtbox_status.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_status.Location = new System.Drawing.Point(132, 90);
            this.txtbox_status.Name = "txtbox_status";
            this.txtbox_status.ReadOnly = true;
            this.txtbox_status.Size = new System.Drawing.Size(97, 27);
            this.txtbox_status.TabIndex = 24;
            this.txtbox_status.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(112, 91);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(15, 21);
            this.label15.TabIndex = 23;
            this.label15.Text = ":";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(11, 91);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 21);
            this.label6.TabIndex = 22;
            this.label6.Text = "Status";
            // 
            // cbox_semester
            // 
            this.cbox_semester.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.cbox_semester.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbox_semester.Enabled = false;
            this.cbox_semester.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbox_semester.FormattingEnabled = true;
            this.cbox_semester.Items.AddRange(new object[] {
            "",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6"});
            this.cbox_semester.Location = new System.Drawing.Point(133, 54);
            this.cbox_semester.Name = "cbox_semester";
            this.cbox_semester.Size = new System.Drawing.Size(96, 23);
            this.cbox_semester.TabIndex = 5;
            this.cbox_semester.SelectedIndexChanged += new System.EventHandler(this.cbox_semester_SelectedIndexChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.White;
            this.label20.Location = new System.Drawing.Point(112, 56);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(15, 21);
            this.label20.TabIndex = 20;
            this.label20.Text = ":";
            // 
            // rbtn_dpp
            // 
            this.rbtn_dpp.AutoSize = true;
            this.rbtn_dpp.Enabled = false;
            this.rbtn_dpp.Location = new System.Drawing.Point(191, 22);
            this.rbtn_dpp.Name = "rbtn_dpp";
            this.rbtn_dpp.Size = new System.Drawing.Size(53, 23);
            this.rbtn_dpp.TabIndex = 4;
            this.rbtn_dpp.TabStop = true;
            this.rbtn_dpp.Text = "DPP";
            this.rbtn_dpp.UseVisualStyleBackColor = true;
            this.rbtn_dpp.CheckedChanged += new System.EventHandler(this.rbtn_dpp_CheckedChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(10, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 21);
            this.label8.TabIndex = 19;
            this.label8.Text = "Semester";
            // 
            // rbtn_spp
            // 
            this.rbtn_spp.AutoSize = true;
            this.rbtn_spp.Enabled = false;
            this.rbtn_spp.Location = new System.Drawing.Point(132, 22);
            this.rbtn_spp.Name = "rbtn_spp";
            this.rbtn_spp.Size = new System.Drawing.Size(50, 23);
            this.rbtn_spp.TabIndex = 3;
            this.rbtn_spp.TabStop = true;
            this.rbtn_spp.Text = "SPP";
            this.rbtn_spp.UseVisualStyleBackColor = true;
            this.rbtn_spp.CheckedChanged += new System.EventHandler(this.rbtn_spp_CheckedChanged);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.White;
            this.label18.Location = new System.Drawing.Point(112, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(15, 21);
            this.label18.TabIndex = 18;
            this.label18.Text = ":";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.White;
            this.label17.Location = new System.Drawing.Point(11, 24);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 21);
            this.label17.TabIndex = 17;
            this.label17.Text = "Kategori";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.White;
            this.label16.Location = new System.Drawing.Point(112, 186);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(15, 21);
            this.label16.TabIndex = 16;
            this.label16.Text = ":";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.White;
            this.label14.Location = new System.Drawing.Point(346, 56);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(15, 21);
            this.label14.TabIndex = 14;
            this.label14.Text = ":";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(128, 187);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 19);
            this.label9.TabIndex = 9;
            this.label9.Text = "Rp.";
            // 
            // txtbox_besarbayaran
            // 
            this.txtbox_besarbayaran.BackColor = System.Drawing.SystemColors.Window;
            this.txtbox_besarbayaran.Enabled = false;
            this.txtbox_besarbayaran.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_besarbayaran.Location = new System.Drawing.Point(159, 187);
            this.txtbox_besarbayaran.Name = "txtbox_besarbayaran";
            this.txtbox_besarbayaran.Size = new System.Drawing.Size(116, 23);
            this.txtbox_besarbayaran.TabIndex = 6;
            this.txtbox_besarbayaran.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lbl_bayar
            // 
            this.lbl_bayar.AutoSize = true;
            this.lbl_bayar.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_bayar.ForeColor = System.Drawing.Color.White;
            this.lbl_bayar.Location = new System.Drawing.Point(10, 186);
            this.lbl_bayar.Name = "lbl_bayar";
            this.lbl_bayar.Size = new System.Drawing.Size(49, 21);
            this.lbl_bayar.TabIndex = 7;
            this.lbl_bayar.Text = "Bayar";
            // 
            // txtbox_cicilanke
            // 
            this.txtbox_cicilanke.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.txtbox_cicilanke.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_cicilanke.Location = new System.Drawing.Point(366, 54);
            this.txtbox_cicilanke.Name = "txtbox_cicilanke";
            this.txtbox_cicilanke.ReadOnly = true;
            this.txtbox_cicilanke.Size = new System.Drawing.Size(48, 23);
            this.txtbox_cicilanke.TabIndex = 5;
            this.txtbox_cicilanke.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(245, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 21);
            this.label5.TabIndex = 1;
            this.label5.Text = "Cicilan Ke";
            // 
            // btn_inputbayar
            // 
            this.btn_inputbayar.BackColor = System.Drawing.Color.Yellow;
            this.btn_inputbayar.Enabled = false;
            this.btn_inputbayar.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_inputbayar.ForeColor = System.Drawing.Color.Black;
            this.btn_inputbayar.Location = new System.Drawing.Point(10, 224);
            this.btn_inputbayar.Name = "btn_inputbayar";
            this.btn_inputbayar.Size = new System.Drawing.Size(428, 34);
            this.btn_inputbayar.TabIndex = 7;
            this.btn_inputbayar.Text = "Input Pembayaran";
            this.btn_inputbayar.UseVisualStyleBackColor = false;
            this.btn_inputbayar.Click += new System.EventHandler(this.btn_inputbayar_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(12, 37);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(70, 19);
            this.label19.TabIndex = 17;
            this.label19.Text = "Cari NPM";
            // 
            // txtbox_cekNPM
            // 
            this.txtbox_cekNPM.BackColor = System.Drawing.SystemColors.Window;
            this.txtbox_cekNPM.Font = new System.Drawing.Font("Rockwell", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_cekNPM.Location = new System.Drawing.Point(133, 35);
            this.txtbox_cekNPM.Name = "txtbox_cekNPM";
            this.txtbox_cekNPM.Size = new System.Drawing.Size(176, 25);
            this.txtbox_cekNPM.TabIndex = 1;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.White;
            this.label22.Location = new System.Drawing.Point(916, 561);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(201, 21);
            this.label22.TabIndex = 20;
            this.label22.Text = "@developed by SISFO 2016";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.White;
            this.label25.Location = new System.Drawing.Point(112, 36);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(15, 21);
            this.label25.TabIndex = 19;
            this.label25.Text = ":";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.RoyalBlue;
            this.label21.Location = new System.Drawing.Point(683, 23);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(428, 33);
            this.label21.TabIndex = 21;
            this.label21.Text = "APLIKASI PEMBAYARAN MAHASISWA";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.label26.Location = new System.Drawing.Point(740, 57);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(377, 33);
            this.label26.TabIndex = 22;
            this.label26.Text = "POLITEKNIK PRAKTISI BANDUNG";
            // 
            // btn_cariNPM
            // 
            this.btn_cariNPM.BackgroundImage = global::SMSGatewayHonorDosen.Properties.Resources.search_btn;
            this.btn_cariNPM.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cariNPM.Location = new System.Drawing.Point(322, 30);
            this.btn_cariNPM.Name = "btn_cariNPM";
            this.btn_cariNPM.Size = new System.Drawing.Size(36, 34);
            this.btn_cariNPM.TabIndex = 2;
            this.btn_cariNPM.UseVisualStyleBackColor = true;
            this.btn_cariNPM.Click += new System.EventHandler(this.btn_cariNPM_Click);
            // 
            // Form_BayarSPPDPP
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1129, 591);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.btn_cariNPM);
            this.Controls.Add(this.txtbox_cekNPM);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.gbox_pembayaran);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.HelpButton = true;
            this.MaximizeBox = false;
            this.Name = "Form_BayarSPPDPP";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " Pembayaran SPP dan DPP Mahasiswa";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OlahDataSPP_FormClosing);
            this.Load += new System.EventHandler(this.OlahDataSPP_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pcbox_foto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_historyPembayaran)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.gbox_pembayaran.ResumeLayout(false);
            this.gbox_pembayaran.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtbox_jurusan;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtbox_kelas;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtbox_nama;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgv_historyPembayaran;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pcbox_foto;
        private System.Windows.Forms.GroupBox gbox_pembayaran;
        private System.Windows.Forms.Button btn_inputbayar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtbox_besarbayaran;
        private System.Windows.Forms.Label lbl_bayar;
        private System.Windows.Forms.TextBox txtbox_cicilanke;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtbox_angkatan;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtbox_cekNPM;
        private System.Windows.Forms.Button btn_cariNPM;
        private System.Windows.Forms.TextBox txtbox_npmmhs;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton rbtn_dpp;
        private System.Windows.Forms.RadioButton rbtn_spp;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbox_semester;
        private System.Windows.Forms.TextBox txtbox_status;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtbox_sisaBayar;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label31;
    }
}