﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using MySql.Data.MySqlClient;
using System.Data.OleDb;

namespace SMSGatewayHonorDosen
{
    public partial class SMSGatewayDosen : Form
    {
        MySqlConnection connection;
        MySqlCommand cmd;
        MySqlDataAdapter adp;

        RegClass regClass = new RegClass();

        string query;
        string pathConnection;
        string sheetExcel;

        string tanggalTransfer;
        string periode;

        string periodeAwal;
        string periodeAkhir;

        string totalTgl;

        string kategoriGaji;
        string besarTransfer;
        string banyakPertemuan;
        string rateGaji;
        
        string jumlahBerkas;
        string hargaPerBerkas;
        string banyakNgawas;
        string hargaPerNgawas;


        List<string> kodeDosen = new List<string>();
        List<string> namaDosen = new List<string>();

        public SMSGatewayDosen()
        {
            InitializeComponent();

            Constanta.server = regClass.ReadFromRegistry("ip");                             

            //FillComboBoxPeriode();
            FillComboBoxKateg();
            //txtBox_PeriodeGaji.Enabled = false;
            dTP_PeriodeAwal.Enabled = false;
            dTP_PeriodeAkhir.Enabled = false;
            cmbBox_KategoriGaji.Enabled = false;
            btn_ImportFile.Enabled = false;
            
        }

        private void optionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Ini untuk Buka tombol opsi
            string x = Interaction.InputBox("Developer Team Account : ", "Entering To Configuration", "");
            if (x == "Uci Arya")
            {
                Option option = new Option();
                option.ShowDialog();
            }
            else
            {
                MessageBox.Show("Configuration is Denied");
            }
        }

        //private void FillComboBoxPeriode()
        //{
        //    Constanta.ServerKeuangan();
        //    connection = new MySqlConnection(Constanta.conStringKeuangan);

        //    cmbox_periodegaji.Items.Clear();

        //    query = "SELECT DISTINCT periode FROM tb_penerimaangaji";

        //    cmd = new MySqlCommand(query, connection);

        //    try
        //    {
        //        connection.Open();
        //        adp = new MySqlDataAdapter(cmd);
        //        DataTable dt = new DataTable();
        //        adp.Fill(dt);
        //        foreach (DataRow r in dt.Rows)
        //        {
        //            cmbox_periodegaji.Items.Add(r[0].ToString());
        //        }
        //        connection.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
            
        //}

        private void FillComboBoxKateg()
        {
            //Ini untuk FillCombobox Kategori Gaji
            Constanta.Server();
            connection = new MySqlConnection(Constanta.conString);

            MessageBox.Show(Constanta.conString.ToString());

            cmbBox_KategoriGaji.Items.Clear();

            query = "SELECT nama_kategori FROM tb_kategoripenggajian";

            cmd = new MySqlCommand(query, connection);

            try
            {
                connection.Open();
                adp = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cmbBox_KategoriGaji.Items.Add(r[0].ToString());
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        // Ini pakai sheet Kategori Gaji
        private void cmbBox_KategoriGaji_SelectedIndexChanged(object sender, EventArgs e)
        {
            sheetExcel = cmbBox_KategoriGaji.Text;
            gbx_InfoDosen.Visible = false;

            importExcel(sheetExcel, pathFile);

            labelKosong();
            TextBoxAktif();
            labeling(cmbBox_KategoriGaji.Text);
            TextBox(cmbBox_KategoriGaji.Text);
        }

        private void DesainGridView()
        {
            dgv_tabelPenerimaan.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgv_tabelPenerimaan.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv_tabelPenerimaan.MultiSelect = false;
            dgv_tabelPenerimaan.AllowUserToAddRows = false;
            dgv_tabelPenerimaan.AllowUserToResizeColumns = false;
            dgv_tabelPenerimaan.AllowUserToDeleteRows = false;
        }

        string pathFile;

        // Untuk Manipulasi
        private string NamaDosen(string kodeDosen)
        {
            string s = "";
            Constanta.Server();
            connection = new MySqlConnection(Constanta.conString);
            query = "SELECT nama_dosen FROM tb_dosen WHERE kode_dosen ='" + kodeDosen + "'";
            cmd = new MySqlCommand(query, connection);

            try
            {
                connection.Open();
                adp = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    s = r[0].ToString();
                }

                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return s;
        }
        
        // Untuk ngimpor excel ke datagrid

        private void importExcel(string cbox, string path)
        {
            try
            {
                // Untuk Microsoft Office 2010
                pathConnection = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source=" + path + ";Extended Properties=\"Excel 12.0; HDR=Yes;\";";
                OleDbConnection conn = new OleDbConnection(pathConnection);

                OleDbDataAdapter dataAdapter = new OleDbDataAdapter("Select * FROM [" + cbox + "$]", pathConnection);

                DataTable dt = new DataTable();
                dataAdapter.Fill(dt);
                dgv_tabelPenerimaan.DataSource = dt;

                // manipulasi kode dos--> nama dos
                //MessageBox.Show(dgv_tabelPenerimaan.Rows.Count.ToString());
                //for (int i = 0; i < dgv_tabelPenerimaan.Rows.Count - 1; i++)
                //{
                //    //MessageBox.Show(dgv_tabelPenerimaan.Rows[i].Cells[1].Value.ToString());
                //    kodeDosen.Add(dgv_tabelPenerimaan.Rows[i].Cells[1].Value.ToString());
                //}
                ////Untuk manipulasi kode dosen ke nama dosen
                //for (int i = 0; i < kodeDosen.Count; i++)
                //{
                //    namaDosen.Add(NamaDosen(kodeDosen[i]));
                //    //MessageBox.Show(namaDosen[i]);
                //}

                //for (int i = 0; i < namaDosen.Count; i++)
                //{
                //    //MessageBox.Show(dgv_tabelPenerimaan.Rows[i].Cells[1].Value.ToString());
                //    dgv_tabelPenerimaan.Rows[i].Cells[1].Value = namaDosen[i];
                //}

                DesainGridView();
                btn_SendSMS.Enabled = true;
                TeksBoxClear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            #region excel dengan tombol udah di coment
            //OpenFileDialog open = new OpenFileDialog();

            //if (open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{
            //    //path = open.FileName;
            //    // Untuk Microsoft Office 2010
            //    pathConnection = "Provider = Microsoft.ACE.OLEDB.12.0; Data Source=" + path + ";Extended Properties=\"Excel 12.0; HDR=Yes;\";";
            //    OleDbConnection conn = new OleDbConnection(pathConnection);

            //    OleDbDataAdapter dataAdapter = new OleDbDataAdapter("Select * FROM [" + cbox + "$]", pathConnection);

            //    DataTable dt = new DataTable();
            //    dataAdapter.Fill(dt);
            //    dgv_tabelPenerimaan.DataSource = dt;

            //    DesainGridView();
            //    btn_SendSMS.Enabled = true;
            //    TeksBoxClear();
            //} 
            #endregion
        }

        private void btn_ImportFile_Click(object sender, EventArgs e)
        {
           //MessageBox.Show(tanggalTransfer);

            try
            {
                OpenFileDialog open = new OpenFileDialog();

                if (open.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    pathFile = open.FileName;
                }
                cmbBox_KategoriGaji.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            

        }

        //Penentuan tanggal transfer melalui datetimepicker
        private void dTP_TanggalTransfer_ValueChanged(object sender, EventArgs e)
        {
            tanggalTransfer = dTP_TanggalTransfer.Value.ToShortDateString();
            //cmbox_periodegaji.Enabled = true;
            //txtBox_PeriodeGaji.Enabled = true;
            dTP_PeriodeAwal.Enabled = true;
        }
        
        //penentuan tanggal melalui button
        private void btn_Today_Click(object sender, EventArgs e)
        {
            tanggalTransfer = DateTime.Now.ToShortDateString();
            //txtBox_PeriodeGaji.Enabled = true;
            dTP_PeriodeAwal.Enabled = true;
        }

        //Pengaturan Label
        private void labeling(string kategori)
        {
            gbx_InfoDosen.Visible = true;
            //labeling untuk mengajar
            if (kategori == "Mengajar")
            {
                lbl1.Text = "Kode   : ";
                lbl2.Text = "Nama   : ";
                lbl3.Text = "No Handphone : ";
                lbl4.Text = "Status Transfer : ";
                lbl5.Text = "Pertemuan : ";
                lbl6.Text = "Rate : ";
                lbl7.Text = "Total : ";
                lbl8.Text = "Rp ";
                lbl9.Text = "Rp ";
                lbl10.Text = "X";
            }
            else if (kategori == "Koordinator Mata Kuliah" || kategori == "Dosen Tetap")
            {
                lbl1.Text = "Kode   : ";
                lbl2.Text = "Nama : ";
                lbl3.Text = "No Handphone : ";
                lbl4.Text = "Status Transfer : ";
                lbl5.Text = "Renumerasi : Rp";
                
                txtBox_Pertemuan.Size = new System.Drawing.Size(135, 24);
            }
            else if (kategori == "Dosen Wali")
            {
                lbl1.Text = "Kode   : ";
                lbl2.Text = "Nama : ";
                lbl3.Text = "No Handphone : ";
                lbl4.Text = "Status Transfer : ";
                lbl5.Text = "Jumlah Mhs : ";
                lbl6.Text = "Honor/Mhs : ";
                lbl7.Text = "Renumerasi : ";
                lbl9.Text = "Rp ";

                //txtBox_Pertemuan.Size = new System.Drawing.Size(135, 24);


            }
            else if (kategori == "Pemeriksaan Berkas")
            {
                lbl1.Text = "Kode   : ";
                lbl2.Text = "Nama : ";
                lbl3.Text = "No Handphone : ";
                lbl4.Text = "Status Transfer : ";
                lbl5.Text = "Jumlah Bekas: ";
                lbl6.Text = "Honor/Berkas : ";
                lbl7.Text = "Renumerasi : ";
                lbl9.Text = "Rp ";

                //txtBox_Pertemuan.Size = new System.Drawing.Size(135, 24);
            }
            else if (kategori == "Mengawas Ujian")
            {
                lbl1.Text = "Kode   : ";
                lbl2.Text = "Nama : ";
                lbl3.Text = "No Handphone : ";
                lbl4.Text = "Status Transfer : ";
                lbl5.Text = "Jumlah Ngawas: ";
                lbl6.Text = "Honor/Ngawas : ";
                lbl7.Text = "Renumerasi : ";
                lbl9.Text = "Rp ";

                //txtBox_Pertemuan.Size = new System.Drawing.Size(135, 24);
            }
        }
        
        //Bikin Kosong label
        private void labelKosong()
        {
            lbl1.Text = " ";
            lbl2.Text = " ";
            lbl3.Text = " ";
            lbl4.Text = " ";
            lbl5.Text = " ";
            lbl6.Text = " ";
            lbl7.Text = " ";
            lbl8.Text = " ";
            lbl9.Text = " ";
            lbl10.Text = " ";

            //Default Ukuran textbox
            txtBox_Status.Size = new System.Drawing.Size(100, 24);
            txtBox_Pertemuan.Size = new System.Drawing.Size(84, 24);
        }
        
        //Bikin Textbox sesuai dengan label yang nyala
        private void TextBox(string kategori)
        {
            if (kategori == "Koordinator Mata Kuliah" || kategori == "Dosen Tetap")
            {
                txtBox_Rate.Visible = false;
                txtBox_Total.Visible = false;
            }
        }

        //Teksbox true semua;
        private void TextBoxAktif()
        {
            txtBox_Kode.Visible = true;
            txtBox_Nama.Visible = true;
            txtBox_NoHP.Visible = true;
            txtBox_Pertemuan.Visible = true;
            txtBox_Rate.Visible = true;
            txtBox_Status.Visible = true;
            txtBox_Total.Visible = true; 
        }

        //Pengisian variabel dari datagrid
        private void dgv_tabelPenerimaan_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv_tabelPenerimaan.Rows.Count > 0)
            {
                txtBox_KodeSlip.Clear();
                btn_SendSMS.Enabled = false;

                if (gbx_InfoDosen.Enabled == false)
                {
                    gbx_InfoDosen.Enabled = true;
                }
                
                // Pengisian variabel
                PengisianVariabel(cmbBox_KategoriGaji.Text);

                if(btn_SendSMS.Enabled == false)
                {
                    btn_SendSMS.Enabled = true;
                }
            }

            isiParameterUntukSlip(cmbBox_KategoriGaji.Text);
            isiParameterUntukStatus(cmbBox_KategoriGaji.Text);
        }

        private void PengisianVariabel(string kategori)
        {
            string kode = "";
            string nama = "";
            string nohp = "";
            string pertemuan = "";
            string rate = "";
            string total = "";
            //string status = "";

            txtBox_Status.Text = "FALSE";

            if (kategori == "Mengajar")
            {
                kode = dgv_tabelPenerimaan.CurrentRow.Cells[0].Value.ToString();
                nama = dgv_tabelPenerimaan.CurrentRow.Cells[1].Value.ToString();
                nohp = dgv_tabelPenerimaan.CurrentRow.Cells[2].Value.ToString();
                pertemuan = dgv_tabelPenerimaan.CurrentRow.Cells[3].Value.ToString();
                rate = dgv_tabelPenerimaan.CurrentRow.Cells[4].Value.ToString();
                total = dgv_tabelPenerimaan.CurrentRow.Cells[5].Value.ToString();
                //status = dgv_tabelPenerimaan.CurrentRow.Cells[6].Value.ToString();
                
                txtBox_Kode.Text = kode;
                txtBox_Nama.Text = nama;
                txtBox_NoHP.Text = nohp;
                txtBox_Pertemuan.Text = pertemuan;
                txtBox_Rate.Text = rate;
                txtBox_Total.Text = total;
                //txtBox_Status.Text = status;
            }
            else if (kategori == "Koordinator Mata Kuliah" || kategori == "Dosen Tetap")
            {
                kode = dgv_tabelPenerimaan.CurrentRow.Cells[0].Value.ToString();
                nama = dgv_tabelPenerimaan.CurrentRow.Cells[1].Value.ToString();
                nohp = dgv_tabelPenerimaan.CurrentRow.Cells[2].Value.ToString();
                pertemuan = dgv_tabelPenerimaan.CurrentRow.Cells[3].Value.ToString();

                txtBox_Kode.Text = kode;
                txtBox_Nama.Text = nama;
                txtBox_NoHP.Text = nohp;
                //Renumerasi
                txtBox_Pertemuan.Text = pertemuan;
            }
            else if (kategori == "Dosen Wali")
            {
                kode = dgv_tabelPenerimaan.CurrentRow.Cells[0].Value.ToString();
                nama = dgv_tabelPenerimaan.CurrentRow.Cells[1].Value.ToString();
                nohp = dgv_tabelPenerimaan.CurrentRow.Cells[2].Value.ToString();
                //Get jumlah Mahasiswa
                pertemuan = dgv_tabelPenerimaan.CurrentRow.Cells[3].Value.ToString();
                //Gate honor / mahasiswa
                rate = dgv_tabelPenerimaan.CurrentRow.Cells[4].Value.ToString();
                //Total
                total = dgv_tabelPenerimaan.CurrentRow.Cells[5].Value.ToString();

                txtBox_Kode.Text = kode;
                txtBox_Nama.Text = nama;
                txtBox_NoHP.Text = nohp;
                //Jumlah Berkas
                txtBox_Pertemuan.Text = pertemuan;
                //Honor/ Mahasiswa
                txtBox_Rate.Text = rate;
                //Total
                txtBox_Total.Text = total;
            }
            else if (kategori == "Pemeriksaan Berkas")
            {
                kode = dgv_tabelPenerimaan.CurrentRow.Cells[0].Value.ToString();
                nama = dgv_tabelPenerimaan.CurrentRow.Cells[1].Value.ToString();
                nohp = dgv_tabelPenerimaan.CurrentRow.Cells[2].Value.ToString();
                //Jumlah Berkas
                pertemuan = dgv_tabelPenerimaan.CurrentRow.Cells[3].Value.ToString();
                //Honor/berkas
                rate = dgv_tabelPenerimaan.CurrentRow.Cells[4].Value.ToString();
                //total
                total = dgv_tabelPenerimaan.CurrentRow.Cells[5].Value.ToString();

                txtBox_Kode.Text = kode;
                txtBox_Nama.Text = nama;
                txtBox_NoHP.Text = nohp;
                //Jumlah Berkas
                txtBox_Pertemuan.Text = pertemuan;
                //Honor/Berkas
                txtBox_Rate.Text = rate;
                //Total
                txtBox_Total.Text = total;
            }
            else if (kategori == "Mengawas Ujian")
            {
                kode = dgv_tabelPenerimaan.CurrentRow.Cells[0].Value.ToString();
                nama = dgv_tabelPenerimaan.CurrentRow.Cells[1].Value.ToString();
                nohp = dgv_tabelPenerimaan.CurrentRow.Cells[2].Value.ToString();
                //Jumlah Ngawas
                pertemuan = dgv_tabelPenerimaan.CurrentRow.Cells[3].Value.ToString();
                //Honor/Ngawas
                rate = dgv_tabelPenerimaan.CurrentRow.Cells[4].Value.ToString();
                //Total
                total = dgv_tabelPenerimaan.CurrentRow.Cells[5].Value.ToString();

                txtBox_Kode.Text = kode;
                txtBox_Nama.Text = nama;
                txtBox_NoHP.Text = nohp;
                //Jumlah Ngawas
                txtBox_Pertemuan.Text = pertemuan;
                //Honor/Ngawas
                txtBox_Rate.Text = rate;
                //Total
                txtBox_Total.Text = total;
            }
        }
        
        //Textbox clear
        private void TeksBoxClear()
        {
            txtBox_Kode.Clear();
            txtBox_KodeSlip.Clear();
            txtBox_Nama.Clear();
            txtBox_NoHP.Clear();
            txtBox_Status.Clear();
            txtBox_Rate.Clear();
            txtBox_Total.Clear();
            txtBox_Pertemuan.Clear();
        }

        //Pengisian isi text SMS
        private List<string> IsiTextSms(string kategori)
        {
            List<string> text = new List<string>();

            kategoriGaji = kategori; 
            periode = ManipulasiPeriode(dTP_PeriodeAwal.Value.ToString("dd/MM/yyyy"), dTP_PeriodeAkhir.Value.ToString("dd/MM/yyyy"));
            
            //string s = "";
            if (kategori == "Mengajar")
            {
                besarTransfer = txtBox_Total.Text;
                banyakPertemuan = txtBox_Pertemuan.Text;
                rateGaji = txtBox_Rate.Text;

                text.Add("Assalamu'alaikum.wr.wb bersama ini kami informasikan honor " + kategoriGaji + " periode " + periode
                    + " telah ditransfer pada " + tanggalTransfer);

                text.Add("sebesar Rp. " + besarTransfer + " dengan rincian" + banyakPertemuan + "X pertemuan dengan rate Rp." + rateGaji + "(Keu.Poltek Praktisi)");

            }
            else if (kategori == "Koordinator Mata Kuliah")
            {
                besarTransfer = txtBox_Pertemuan.Text;

                text.Add("Assalamu'alaikum.wr.wb bersama ini kami informasikan honor " + kategoriGaji + " periode " + periode
                    + " telah ditransfer pada " + tanggalTransfer);
                
                text.Add("sebesar Rp. " + besarTransfer + "(Keu.Poltek Praktisi)");
            }
            else if (kategoriGaji == "Dosen Tetap")
            {
                besarTransfer = txtBox_Pertemuan.Text;

                text.Add("Assalamu'alaikum.wr.wb bersama ini kami informasikan honor " + kategoriGaji + " periode " + periode
                    + " telah ditransfer pada " + tanggalTransfer);

                text.Add("sebesar Rp. " + besarTransfer + "(Keu.Poltek Praktisi)");
            }
            else if (kategoriGaji == "Dosen Wali")
            {
                besarTransfer = txtBox_Total.Text;

                text.Add("Assalamu'alaikum.wr.wb bersama ini kami informasikan honor " + kategoriGaji + " periode " + periode
                    + " telah ditransfer pada " + tanggalTransfer);

                text.Add("sebesar Rp. " + besarTransfer + "(Keu.Poltek Praktisi)");
            }
            else if (kategori == "Pemeriksaan Berkas")
            {
                besarTransfer = txtBox_Total.Text;
                jumlahBerkas = txtBox_Pertemuan.Text;
                hargaPerBerkas = txtBox_Rate.Text;

                text.Add("Assalamu'alaikum.wr.wb bersama ini kami informasikan honor " + kategoriGaji + " periode " + periode
                    + " telah ditransfer pada " + tanggalTransfer);

                text.Add("sebesar Rp. " + besarTransfer + " dengan rincian " + jumlahBerkas + " berkas x @ " + hargaPerBerkas + "(Keu.Poltek Praktisi)");
                
            }
            else if ( kategori == "Mengawas Ujian")
            {
                besarTransfer = txtBox_Total.Text;
                banyakNgawas = txtBox_Pertemuan.Text;
                hargaPerNgawas = txtBox_Rate.Text;

                text.Add("Assalamu'alaikum.wr.wb bersama ini kami informasikan honor " + kategoriGaji + " periode " + periode
                    + " telah ditransfer pada " + tanggalTransfer);

                text.Add("sebesar Rp. " + besarTransfer + " dengan rincian " + banyakNgawas + " x @ " + hargaPerNgawas + "(Keu.Poltek Praktisi)");
            }

            return text;
        }

        // Kirim smsnya
        private void kirimSMS(List<string> isiText)
        {
            Constanta.Server();
            connection = new MySqlConnection(Constanta.conString);

            for (int i = 0; i < isiText.Count; i++)
            {
                query = "INSERT INTO outbox (DestinationNumber, TextDecoded, DeliveryReport) VALUES (@dn, @td, 'no')";
                cmd = new MySqlCommand(query, connection);
                cmd.Parameters.AddWithValue("@dn", txtBox_NoHP.Text);
                cmd.Parameters.AddWithValue("@td", isiText[i]);
                try
                {
                    connection.Open();
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        MessageBox.Show("SMS has been inputed");
                    }
                    connection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            
        }

        //Ngisi Database
        //private void IsiDbHonorMengajar()
        //{
        //    Constanta.ServerKeuangan();
        //    connection = new MySqlConnection(Constanta.conStringKeuangan);

        //    query = "INSERT INTO tb_honormengajar (kode_dosen, kode_slip_gaji , banyak_pertemuan, rate_gaji, total, periode, tanggal_transfer, status_transfer) VALUES (@dosen, @kd, @pertemuan, @rate, @total, @periode , @tgl, 'TRUE') ";
        //    cmd = new MySqlCommand(query, connection);
        //    cmd.Parameters.AddWithValue("@dosen", txtBox_Kode.Text);
        //    cmd.Parameters.AddWithValue("@kd", txtBox_KodeSlip.Text);
        //    cmd.Parameters.AddWithValue("@pertemuan", int.Parse(txtBox_Pertemuan.Text));
        //    cmd.Parameters.AddWithValue("@rate", int.Parse(txtBox_Rate.Text));
        //    cmd.Parameters.AddWithValue("@total", int.Parse(txtBox_Total.Text));
        //    cmd.Parameters.AddWithValue("@periode", ManipulasiPeriode(dTP_PeriodeAwal.Value.ToString("dd/MM/yyyy"), dTP_PeriodeAkhir.Value.ToString("dd/MM/yyyy")));
        //    cmd.Parameters.AddWithValue("@tgl", dTP_TanggalTransfer.Value.ToString("yyyy-MM-dd"));
        //    //cmd.Parameters.AddWithValue("@stat", stat);
            
        //    try
        //    {
        //        connection.Open();
        //        if (cmd.ExecuteNonQuery() > 0)
        //        {
        //            MessageBox.Show("Data Honor Mengajar has been inputed");
        //        }
        //        connection.Close();
        //    }
        //    catch (Exception ex)
        //    { 
        //        MessageBox.Show(ex.Message);
        //    }
        //}

        //IsisDB

        private string q(string kategori)
        {
            query = "";
            if (kategori == "Mengajar")
            {
                query = "INSERT INTO tb_honormengajar (kode_dosen, kode_slip_gaji , banyak_pertemuan, rate_gaji, total, periode, tanggal_transfer, status_transfer) VALUES (@dosen, @kd, @pertemuan, @rate, @total, @periode , @tgl, 'TRUE') ";
            }
            else if (kategori == "Koordinator Mata Kuliah")
            {
                query = "INSERT INTO tb_hnrdosenkoord (kode_dosen, kode_slip_gaji , renumerasi, periode, tanggal_transfer, status_transfer) VALUES (@dosen, @kd , @renumerasi, @periode , @tgl, 'TRUE') ";
            }
            else if (kategori == "Dosen Tetap")
            {
                query = "INSERT INTO tb_hnrdosentetap (kode_dosen, kode_slip_gaji , renumerasi, periode, tanggal_transfer, status_transfer) VALUES (@dosen, @kd , @renumerasi, @periode , @tgl, 'TRUE') ";

            }
            else if (kategori == "Dosen Wali")
            {
                query = "INSERT INTO tb_hnrdosenwali (kode_dosen, kode_slip_gaji , jumlah_mahasiswa, honorpermahasiswa, total, periode, tanggal_transfer, status_transfer) VALUES (@dosen, @kd, @jmlmhs, @hnrpermhs, @total, @periode , @tgl, 'TRUE') ";

            }
            else if (kategori == "Pemeriksaan Berkas")
            {
                query = "INSERT INTO tb_hnrpemeriksaanberkas (kode_dosen, kode_slip_gaji , banyak_berkas, honorperberkas, total, periode, tanggal_transfer, status_transfer) VALUES (@dosen, @kd, @bnykberkas, @hnrperbrkas, @total, @periode , @tgl, 'TRUE') ";
            }
            else if (kategori == "Mengawas Ujian")
            {
                query = "INSERT INTO tb_hnrmengawas (kode_dosen, kode_slip_gaji , banyak_ngawas, honorperngawas, total, periode, tanggal_transfer, status_transfer) VALUES (@dosen, @kd, @bnykngawas, @hnrperngawas, @total, @periode , @tgl, 'TRUE') ";
            }

            return query;
        }

        void InputMengajar(MySqlCommand c, string kodedsn, string kdslip, int pertemuan, int rate,
            int totalHnrMengajar, string periodehnrmengajar, string tgl)
        {
            c.Parameters.AddWithValue("@dosen", kodedsn);
            c.Parameters.AddWithValue("@kd", kdslip);
            c.Parameters.AddWithValue("@pertemuan", pertemuan);
            c.Parameters.AddWithValue("@rate", rate);
            c.Parameters.AddWithValue("@total", totalHnrMengajar);
            c.Parameters.AddWithValue("@periode", periodehnrmengajar);
            c.Parameters.AddWithValue("@tgl", tgl);
        }

        void InputKoorMatkul(MySqlCommand c, string kodedsn, string kdslip, int renumerasiMatkul, 
            string periodehnrMatkul, string tgl)
        {
            c.Parameters.AddWithValue("@dosen", kodedsn);
            c.Parameters.AddWithValue("@kd", kdslip);
            c.Parameters.AddWithValue("@renumerasi", renumerasiMatkul);
            c.Parameters.AddWithValue("@periode", periodehnrMatkul);
            c.Parameters.AddWithValue("@tgl", tgl);
        }

        void InputDosenTetap(MySqlCommand c, string kodedsn, string kdslip, 
            int renumerasiDosenttp, string periodehnrttp, string tgl)
        {
            c.Parameters.AddWithValue("@dosen", kodedsn);
            c.Parameters.AddWithValue("@kd", kdslip);
            c.Parameters.AddWithValue("@renumerasi", renumerasiDosenttp);
            c.Parameters.AddWithValue("@periode", periodehnrttp);
            c.Parameters.AddWithValue("@tgl", tgl);
        }

        void InputDosenWali(MySqlCommand c, string kodedsn, string kdslip,
            int jmlmhs, int hnrmhs, int totalDsnWali, string periodehnrmengajar, string tgl)
        {
           c.Parameters.AddWithValue("@dosen", kodedsn);
           c.Parameters.AddWithValue("@kd", kdslip);
           c.Parameters.AddWithValue("@jmlmhs", jmlmhs);
           c.Parameters.AddWithValue("@hnrpermhs", hnrmhs);
           c.Parameters.AddWithValue("@total", totalDsnWali);
           c.Parameters.AddWithValue("@periode", periodehnrmengajar);
           c.Parameters.AddWithValue("@tgl", tgl);           
        }

        void InputDosenPemeriksaanBerkas(MySqlCommand c, string kodedsn, string kdslip,
            int bnykberkas, int hnrperbrks, int totPemeriksaanBerkas, string periodehnrBerkas, string tgl)
        {
            c.Parameters.AddWithValue("@dosen", kodedsn);
            c.Parameters.AddWithValue("@kd", kdslip);
            c.Parameters.AddWithValue("@bnykberkas", bnykberkas);
            c.Parameters.AddWithValue("@hnrperbrkas", hnrperbrks);
            c.Parameters.AddWithValue("@total", totPemeriksaanBerkas);
            c.Parameters.AddWithValue("@periode", periodehnrBerkas);
            c.Parameters.AddWithValue("@tgl", tgl);
        }

        void InputDosenMengawasujian(MySqlCommand c, string kodedsn, string kdslip,
            int bnykngawas, int hnrperNgawas, int totHnrNgawas, string periodehnrBerkas, string tgl)
        {
            cmd.Parameters.AddWithValue("@dosen", kodedsn);
            cmd.Parameters.AddWithValue("@kd", kdslip);
            cmd.Parameters.AddWithValue("@bnykngawas", bnykngawas);
            cmd.Parameters.AddWithValue("@hnrperngawas", hnrperNgawas);
            cmd.Parameters.AddWithValue("@total", totHnrNgawas);
            cmd.Parameters.AddWithValue("@periode", periodehnrBerkas);
            cmd.Parameters.AddWithValue("tgl", tgl);
        } 

        private void isiParameter(string kategori)
        {
            Constanta.Server();
            connection = new MySqlConnection(Constanta.conString);

            query = q(cmbBox_KategoriGaji.Text);
            cmd = new MySqlCommand(query, connection);

            if (kategori == "Mengajar")
            {
                InputMengajar(cmd, txtBox_Kode.Text, txtBox_KodeSlip.Text, int.Parse(txtBox_Pertemuan.Text),
                    int.Parse(txtBox_Rate.Text), int.Parse(txtBox_Total.Text),
                    ManipulasiPeriode(dTP_PeriodeAwal.Value.ToString("dd/MM/yyyy"), 
                    dTP_PeriodeAkhir.Value.ToString("dd/MM/yyyy")), 
                    dTP_TanggalTransfer.Value.ToString("yyyy-MM-dd")); 
            }
            else if (kategori == "Koordinator Mata Kuliah")
            {
                InputKoorMatkul(cmd, txtBox_Kode.Text, txtBox_KodeSlip.Text, int.Parse(txtBox_Pertemuan.Text),
                     ManipulasiPeriode(dTP_PeriodeAwal.Value.ToString("dd/MM/yyyy"),
                    dTP_PeriodeAkhir.Value.ToString("dd/MM/yyyy")), 
                    dTP_TanggalTransfer.Value.ToString("yyyy-MM-dd"));
            }
            else if (kategori == "Dosen Tetap")
            {
                InputDosenTetap(cmd, txtBox_Kode.Text, txtBox_KodeSlip.Text, int.Parse(txtBox_Pertemuan.Text),
                     ManipulasiPeriode(dTP_PeriodeAwal.Value.ToString("dd/MM/yyyy"),
                    dTP_PeriodeAkhir.Value.ToString("dd/MM/yyyy")),
                    dTP_TanggalTransfer.Value.ToString("yyyy-MM-dd"));
            }
            else if (kategori == "Dosen Wali")
            {
                InputDosenWali(cmd, txtBox_Kode.Text, txtBox_KodeSlip.Text, int.Parse(txtBox_Pertemuan.Text),
                    int.Parse(txtBox_Rate.Text), int.Parse(txtBox_Total.Text), 
                    ManipulasiPeriode(dTP_PeriodeAwal.Value.ToString("dd/MM/yyyy"),
                    dTP_PeriodeAkhir.Value.ToString("dd/MM/yyyy")),
                    dTP_TanggalTransfer.Value.ToString("yyyy-MM-dd"));
            }
            else if (kategori == "Pemeriksaan Berkas")
            {
                InputDosenPemeriksaanBerkas(cmd, txtBox_Kode.Text, txtBox_KodeSlip.Text, int.Parse(txtBox_Pertemuan.Text),
                    int.Parse(txtBox_Rate.Text), int.Parse(txtBox_Total.Text),
                    ManipulasiPeriode(dTP_PeriodeAwal.Value.ToString("dd/MM/yyyy"),
                    dTP_PeriodeAkhir.Value.ToString("dd/MM/yyyy")),
                    dTP_TanggalTransfer.Value.ToString("yyyy-MM-dd"));
            }
            else if (kategori == "Mengawas Ujian")
            {
                InputDosenMengawasujian(cmd, txtBox_Kode.Text, txtBox_KodeSlip.Text, int.Parse(txtBox_Pertemuan.Text),
                    int.Parse(txtBox_Rate.Text), int.Parse(txtBox_Total.Text),
                    ManipulasiPeriode(dTP_PeriodeAwal.Value.ToString("dd/MM/yyyy"),
                    dTP_PeriodeAkhir.Value.ToString("dd/MM/yyyy")),
                    dTP_TanggalTransfer.Value.ToString("yyyy-MM-dd"));
            }

            try
            {
                connection.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {
                    MessageBox.Show("Data Honor " + kategori + " has been inputed");
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btn_SendSMS_Click(object sender, EventArgs e)
        {
            kirimSMS(IsiTextSms(cmbBox_KategoriGaji.Text));
            isiParameter(cmbBox_KategoriGaji.Text);
            TeksBoxClear();
            btn_SendSMS.Enabled = false;
        }

        //private void SelectStatusTrasfer(string kode, string periode)
        //{
        //    Constanta.ServerKeuangan();
        //    connection = new MySqlConnection(Constanta.conStringKeuangan);

        //    query = "SELECT status_transfer FROM tb_honormengajar WHERE kode_dosen ='" + kode + "' AND periode ='" + periode + "'";
        //    cmd = new MySqlCommand(query, connection);

        //    try
        //    {
        //        connection.Open();
        //        adp = new MySqlDataAdapter(cmd);
        //        DataTable dt = new DataTable();
        //        adp.Fill(dt);

        //        foreach (DataRow r in dt.Rows)
        //        {
        //            txtBox_Status.Text = r[0].ToString();
        //            btn_SendSMS.Enabled = false;
        //        }
        //        connection.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //}

        //private void AmbilKodeSlip(string kode)
        //{
        //    Constanta.ServerKeuangan();
        //    connection = new MySqlConnection(Constanta.conStringKeuangan);

        //    query = "SELECT kode_slip_gaji FROM tb_honormengajar WHERE kode_dosen ='" + kode + "'";
        //    cmd = new MySqlCommand(query, connection);

        //    try
        //    {
        //        connection.Open();
        //        adp = new MySqlDataAdapter(cmd);
        //        DataTable dt = new DataTable();
        //        adp.Fill(dt);

        //        foreach (DataRow r in dt.Rows)
        //        {
        //            txtBox_KodeSlip.Text = r[0].ToString();
        //        }
        //        connection.Close();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //}

        //Select Status dari tb

        private string queryToSelectStatus(string kategori, string kode, string periode)
        {
            string q = "";

            if (kategori == "Mengajar")
            {
                q = "SELECT status_transfer FROM tb_honormengajar WHERE kode_dosen ='" + kode + "' AND periode ='" + periode + "'";

            }
            else if (kategori == "Koordinator Mata Kuliah")
            {
                q = "SELECT status_transfer FROM tb_hnrdosenkoord WHERE kode_dosen ='" + kode + "' AND periode ='" + periode + "'";
            }
            else if (kategori == "Dosen Tetap")
            {
                q = "SELECT status_transfer FROM tb_hnrdosentetap WHERE kode_dosen ='" + kode + "' AND periode ='" + periode + "'";

            }
            else if (kategori == "Dosen Wali")
            {
                q = "SELECT status_transfer FROM tb_hnrdosenwali WHERE kode_dosen ='" + kode + "' AND periode ='" + periode + "'";

            }
            else if (kategori == "Pemeriksaan Berkas")
            {
                q = "SELECT status_transfer FROM tb_hnrpemeriksaanberkas WHERE kode_dosen ='" + kode + "' AND periode ='" + periode + "'";
            }
            else if (kategori == "Mengawas Ujian")
            {
                q = "SELECT status_transfer FROM tb_hnrmengawas WHERE kode_dosen ='" + kode + "' AND periode ='" + periode + "'";
            }

            return q;
        }

        private void isiParameterUntukStatus(string kategori)
        {
            Constanta.Server();
            connection = new MySqlConnection(Constanta.conString);

            query = queryToSelectStatus(cmbBox_KategoriGaji.Text, dgv_tabelPenerimaan.CurrentRow.Cells[0].Value.ToString(), ManipulasiPeriode(dTP_PeriodeAwal.Value.ToString("dd/MM/yyyy"), dTP_PeriodeAkhir.Value.ToString("dd/MM/yyyy")));
            cmd = new MySqlCommand(query, connection);

            try
            {
                connection.Open();
                adp = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    txtBox_Status.Text = r[0].ToString();
                    btn_SendSMS.Enabled = false;
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
       
        //select kode_slip

        private string queryToAmbilKodeSlip(string kategori, string kode, string periode)
        {
            string q = "";

            if (kategori == "Mengajar")
            {
                q = "SELECT kode_slip_gaji FROM tb_honormengajar WHERE kode_dosen ='" + kode + "' AND periode ='" + periode + "'";

            }
            else if (kategori == "Koordinator Mata Kuliah")
            {
                q = "SELECT kode_slip_gaji FROM tb_hnrdosenkoord WHERE kode_dosen ='" + kode + "' AND periode ='" + periode + "'";
            }
            else if (kategori == "Dosen Tetap")
            {
                q = "SELECT kode_slip_gaji FROM tb_hnrdosentetap WHERE kode_dosen ='" + kode + "' AND periode ='" + periode + "'";

            }
            else if (kategori == "Dosen Wali")
            {
                q = "SELECT kode_slip_gaji FROM tb_hnrdosenwali WHERE kode_dosen ='" + kode + "' AND periode ='" + periode + "'";

            }
            else if (kategori == "Pemeriksaan Berkas")
            {
                q = "SELECT kode_slip_gaji FROM tb_hnrpemeriksaanberkas WHERE kode_dosen ='" + kode + "' AND periode ='" + periode + "'";
            }
            else if (kategori == "Mengawas Ujian")
            {
                q = "SELECT kode_slip_gaji FROM tb_hnrmengawas WHERE kode_dosen ='" + kode + "' AND periode ='" + periode + "'";
            }

            return q;
 
        }

        private void isiParameterUntukSlip(string kategori)
        {
            Constanta.Server();
            connection = new MySqlConnection(Constanta.conString);

            query = queryToAmbilKodeSlip(cmbBox_KategoriGaji.Text, dgv_tabelPenerimaan.CurrentRow.Cells[0].Value.ToString(), ManipulasiPeriode(dTP_PeriodeAwal.Value.ToString("dd/MM/yyyy"), dTP_PeriodeAkhir.Value.ToString("dd/MM/yyyy")));
            cmd = new MySqlCommand(query, connection);

            try
            {
                connection.Open();
                adp = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    txtBox_Status.Text = r[0].ToString();
                    btn_SendSMS.Enabled = false;
                }
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        //SettingConfiguration
        private void settingConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string x = Interaction.InputBox("Developer Team Account : ", "Entering To Configuration", "");
            if (x == "Uci Arya")
            {
                SettingConfiguration fset = new SettingConfiguration();
                fset.ShowDialog();
            }
            else
            {
                MessageBox.Show("Configuration is Denied");
            }
        }

        private void txtBox_PeriodeGaji_TextChanged(object sender, EventArgs e)
        {
            //btn_ImportFile.Enabled = true;
        }

        private string ManipulasiPeriode(string tgl1, string tgl2)
        {
            string dtp1 = "";
            string dtp2 = "";

            string th1, th2;
            th1 = tgl1.Substring(6, 4);
            th2 = tgl2.Substring(6, 4);

            // 
            if (th1 != th2)
            {
                dtp1 = tgl1;
                dtp2 = tgl2;
            }
            else
            {
                dtp1 = tgl1.Substring(0, 5);
                dtp2 = tgl2;
            }

            return dtp1 + "-" + dtp2;
        }

        private void dTP_PeriodeAkhir_ValueChanged(object sender, EventArgs e)
        {
            btn_ImportFile.Enabled = true;
        }

        private void dTP_PeriodeAwal_ValueChanged(object sender, EventArgs e)
        {
            dTP_PeriodeAkhir.Enabled = true;
        }

      
    }
}
