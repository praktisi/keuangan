﻿namespace SMSGatewayHonorDosen
{
    partial class Option
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_browse = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtbox_Path = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cmbBox_Modem = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbBox_Connection = new System.Windows.Forms.ComboBox();
            this.txtbox_Driver = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbBox_Service = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBox_Database = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtbox_PC = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbBox_Type = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtbox_Password = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_installservice = new System.Windows.Forms.Button();
            this.btn_Copy = new System.Windows.Forms.Button();
            this.btn_saveFile = new System.Windows.Forms.Button();
            this.btn_identify = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtbox_User = new System.Windows.Forms.TextBox();
            this.btn_start = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_browse
            // 
            this.btn_browse.Location = new System.Drawing.Point(202, 62);
            this.btn_browse.Name = "btn_browse";
            this.btn_browse.Size = new System.Drawing.Size(75, 23);
            this.btn_browse.TabIndex = 3;
            this.btn_browse.Text = "Browse";
            this.btn_browse.UseVisualStyleBackColor = true;
            this.btn_browse.Click += new System.EventHandler(this.btn_browse_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtbox_Path);
            this.groupBox1.Controls.Add(this.btn_browse);
            this.groupBox1.Location = new System.Drawing.Point(21, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(296, 100);
            this.groupBox1.TabIndex = 40;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Choose Address";
            // 
            // txtbox_Path
            // 
            this.txtbox_Path.Location = new System.Drawing.Point(19, 36);
            this.txtbox_Path.Name = "txtbox_Path";
            this.txtbox_Path.Size = new System.Drawing.Size(258, 20);
            this.txtbox_Path.TabIndex = 4;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 13);
            this.label9.TabIndex = 29;
            this.label9.Text = "Modem/Hp";
            // 
            // cmbBox_Modem
            // 
            this.cmbBox_Modem.FormattingEnabled = true;
            this.cmbBox_Modem.Location = new System.Drawing.Point(18, 40);
            this.cmbBox_Modem.Name = "cmbBox_Modem";
            this.cmbBox_Modem.Size = new System.Drawing.Size(258, 21);
            this.cmbBox_Modem.TabIndex = 28;
            this.cmbBox_Modem.SelectedIndexChanged += new System.EventHandler(this.cmbBox_Modem_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 109);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Connection";
            // 
            // cmbBox_Connection
            // 
            this.cmbBox_Connection.FormattingEnabled = true;
            this.cmbBox_Connection.Location = new System.Drawing.Point(18, 125);
            this.cmbBox_Connection.Name = "cmbBox_Connection";
            this.cmbBox_Connection.Size = new System.Drawing.Size(258, 21);
            this.cmbBox_Connection.TabIndex = 26;
            this.cmbBox_Connection.SelectedIndexChanged += new System.EventHandler(this.cmbBox_Connection_SelectedIndexChanged);
            // 
            // txtbox_Driver
            // 
            this.txtbox_Driver.Location = new System.Drawing.Point(114, 178);
            this.txtbox_Driver.Name = "txtbox_Driver";
            this.txtbox_Driver.Size = new System.Drawing.Size(175, 20);
            this.txtbox_Driver.TabIndex = 33;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(28, 181);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "Driver :";
            // 
            // cmbBox_Service
            // 
            this.cmbBox_Service.FormattingEnabled = true;
            this.cmbBox_Service.Items.AddRange(new object[] {
            "MYSQL",
            "PGSQL",
            "DBI"});
            this.cmbBox_Service.Location = new System.Drawing.Point(114, 47);
            this.cmbBox_Service.Name = "cmbBox_Service";
            this.cmbBox_Service.Size = new System.Drawing.Size(175, 21);
            this.cmbBox_Service.TabIndex = 31;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "Service :";
            // 
            // txtBox_Database
            // 
            this.txtBox_Database.Location = new System.Drawing.Point(114, 152);
            this.txtBox_Database.Name = "txtBox_Database";
            this.txtBox_Database.Size = new System.Drawing.Size(175, 20);
            this.txtBox_Database.TabIndex = 29;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 28;
            this.label4.Text = "Database :";
            // 
            // txtbox_PC
            // 
            this.txtbox_PC.Location = new System.Drawing.Point(114, 126);
            this.txtbox_PC.Name = "txtbox_PC";
            this.txtbox_PC.Size = new System.Drawing.Size(175, 20);
            this.txtbox_PC.TabIndex = 27;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 66);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 31;
            this.label10.Text = "Type";
            // 
            // cmbBox_Type
            // 
            this.cmbBox_Type.FormattingEnabled = true;
            this.cmbBox_Type.Location = new System.Drawing.Point(18, 82);
            this.cmbBox_Type.Name = "cmbBox_Type";
            this.cmbBox_Type.Size = new System.Drawing.Size(258, 21);
            this.cmbBox_Type.TabIndex = 30;
            this.cmbBox_Type.SelectedIndexChanged += new System.EventHandler(this.cmbBox_Type_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.cmbBox_Type);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.cmbBox_Modem);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.cmbBox_Connection);
            this.groupBox2.Location = new System.Drawing.Point(22, 129);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(295, 163);
            this.groupBox2.TabIndex = 41;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Isian Gammurc";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(28, 133);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 13);
            this.label5.TabIndex = 26;
            this.label5.Text = "PC :";
            // 
            // txtbox_Password
            // 
            this.txtbox_Password.Location = new System.Drawing.Point(114, 100);
            this.txtbox_Password.Name = "txtbox_Password";
            this.txtbox_Password.Size = new System.Drawing.Size(175, 20);
            this.txtbox_Password.TabIndex = 25;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Password :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "User : ";
            // 
            // btn_installservice
            // 
            this.btn_installservice.Location = new System.Drawing.Point(341, 318);
            this.btn_installservice.Name = "btn_installservice";
            this.btn_installservice.Size = new System.Drawing.Size(117, 43);
            this.btn_installservice.TabIndex = 46;
            this.btn_installservice.Text = "Install Service";
            this.btn_installservice.UseVisualStyleBackColor = true;
            this.btn_installservice.Click += new System.EventHandler(this.btn_installservice_Click);
            // 
            // btn_Copy
            // 
            this.btn_Copy.Location = new System.Drawing.Point(242, 328);
            this.btn_Copy.Name = "btn_Copy";
            this.btn_Copy.Size = new System.Drawing.Size(75, 23);
            this.btn_Copy.TabIndex = 45;
            this.btn_Copy.Text = "Copy";
            this.btn_Copy.UseVisualStyleBackColor = true;
            this.btn_Copy.Click += new System.EventHandler(this.btn_Copy_Click);
            // 
            // btn_saveFile
            // 
            this.btn_saveFile.Location = new System.Drawing.Point(141, 328);
            this.btn_saveFile.Name = "btn_saveFile";
            this.btn_saveFile.Size = new System.Drawing.Size(75, 23);
            this.btn_saveFile.TabIndex = 44;
            this.btn_saveFile.Text = "Save";
            this.btn_saveFile.UseVisualStyleBackColor = true;
            this.btn_saveFile.Click += new System.EventHandler(this.btn_saveFile_Click);
            // 
            // btn_identify
            // 
            this.btn_identify.Location = new System.Drawing.Point(36, 328);
            this.btn_identify.Name = "btn_identify";
            this.btn_identify.Size = new System.Drawing.Size(75, 23);
            this.btn_identify.TabIndex = 43;
            this.btn_identify.Text = "Identify";
            this.btn_identify.UseVisualStyleBackColor = true;
            this.btn_identify.Click += new System.EventHandler(this.btn_identify_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtbox_Driver);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.cmbBox_Service);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txtBox_Database);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.txtbox_PC);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtbox_Password);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtbox_User);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(323, 34);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(323, 258);
            this.groupBox3.TabIndex = 42;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Isian SMSDRC";
            // 
            // txtbox_User
            // 
            this.txtbox_User.Location = new System.Drawing.Point(114, 74);
            this.txtbox_User.Name = "txtbox_User";
            this.txtbox_User.Size = new System.Drawing.Size(175, 20);
            this.txtbox_User.TabIndex = 23;
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(482, 318);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(117, 43);
            this.btn_start.TabIndex = 47;
            this.btn_start.Text = "Start Service";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // Option
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(670, 395);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btn_installservice);
            this.Controls.Add(this.btn_Copy);
            this.Controls.Add(this.btn_saveFile);
            this.Controls.Add(this.btn_identify);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btn_start);
            this.Name = "Option";
            this.Text = "Option";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_browse;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtbox_Path;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cmbBox_Modem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbBox_Connection;
        private System.Windows.Forms.TextBox txtbox_Driver;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbBox_Service;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtBox_Database;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtbox_PC;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbBox_Type;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtbox_Password;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_installservice;
        private System.Windows.Forms.Button btn_Copy;
        private System.Windows.Forms.Button btn_saveFile;
        private System.Windows.Forms.Button btn_identify;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtbox_User;
        private System.Windows.Forms.Button btn_start;
    }
}