﻿namespace kartuUjian
{
    partial class CetakKartu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cR_CetakKartu = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.cmbBox_StatusUjian = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbBox_Ta = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbBox_Semester = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_carinpm = new System.Windows.Forms.Button();
            this.txtBox_npm = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // cR_CetakKartu
            // 
            this.cR_CetakKartu.ActiveViewIndex = -1;
            this.cR_CetakKartu.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cR_CetakKartu.Cursor = System.Windows.Forms.Cursors.Default;
            this.cR_CetakKartu.DisplayStatusBar = false;
            this.cR_CetakKartu.DisplayToolbar = false;
            this.cR_CetakKartu.Location = new System.Drawing.Point(16, 231);
            this.cR_CetakKartu.Name = "cR_CetakKartu";
            this.cR_CetakKartu.Size = new System.Drawing.Size(949, 460);
            this.cR_CetakKartu.TabIndex = 3;
            // 
            // cmbBox_StatusUjian
            // 
            this.cmbBox_StatusUjian.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBox_StatusUjian.FormattingEnabled = true;
            this.cmbBox_StatusUjian.Items.AddRange(new object[] {
            "Tengah Semester",
            "Akhir Semester"});
            this.cmbBox_StatusUjian.Location = new System.Drawing.Point(163, 25);
            this.cmbBox_StatusUjian.Name = "cmbBox_StatusUjian";
            this.cmbBox_StatusUjian.Size = new System.Drawing.Size(154, 28);
            this.cmbBox_StatusUjian.TabIndex = 4;
            this.cmbBox_StatusUjian.TabStop = false;
            this.cmbBox_StatusUjian.SelectedIndexChanged += new System.EventHandler(this.cmbBox_StatusUjian_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.cmbBox_Ta);
            this.groupBox2.Controls.Add(this.cmbBox_StatusUjian);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.cmbBox_Semester);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Enabled = false;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(16, 95);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(332, 134);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pilih :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(123, 98);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 20);
            this.label9.TabIndex = 13;
            this.label9.Text = ":";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(123, 66);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 20);
            this.label8.TabIndex = 12;
            this.label8.Text = ":";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(123, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 20);
            this.label7.TabIndex = 11;
            this.label7.Text = ":";
            // 
            // cmbBox_Ta
            // 
            this.cmbBox_Ta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBox_Ta.FormattingEnabled = true;
            this.cmbBox_Ta.Location = new System.Drawing.Point(163, 95);
            this.cmbBox_Ta.Name = "cmbBox_Ta";
            this.cmbBox_Ta.Size = new System.Drawing.Size(154, 28);
            this.cmbBox_Ta.TabIndex = 9;
            this.cmbBox_Ta.TabStop = false;
            this.cmbBox_Ta.SelectedIndexChanged += new System.EventHandler(this.cmbBox_Ta_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 98);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 20);
            this.label6.TabIndex = 8;
            this.label6.Text = "Tahun Ajaran ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 28);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "Status Ujian ";
            // 
            // cmbBox_Semester
            // 
            this.cmbBox_Semester.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBox_Semester.FormattingEnabled = true;
            this.cmbBox_Semester.Location = new System.Drawing.Point(163, 60);
            this.cmbBox_Semester.Name = "cmbBox_Semester";
            this.cmbBox_Semester.Size = new System.Drawing.Size(154, 28);
            this.cmbBox_Semester.TabIndex = 7;
            this.cmbBox_Semester.TabStop = false;
            this.cmbBox_Semester.SelectedIndexChanged += new System.EventHandler(this.cmbBox_Semester_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 20);
            this.label5.TabIndex = 6;
            this.label5.Text = "Semester ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(122, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(441, 29);
            this.label1.TabIndex = 6;
            this.label1.Text = "FORM PENCETAKAN KARTU UJIAN";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(122, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(371, 25);
            this.label2.TabIndex = 8;
            this.label2.Text = "POLITEKNIK PRAKTISI BANDUNG";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(831, 694);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Developed by SISFO 2016";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn_carinpm);
            this.groupBox3.Controls.Add(this.txtBox_npm);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Cursor = System.Windows.Forms.Cursors.Default;
            this.groupBox3.Enabled = false;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(354, 95);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(301, 130);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Cari Mahasiswa :";
            // 
            // btn_carinpm
            // 
            this.btn_carinpm.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_carinpm.Location = new System.Drawing.Point(154, 52);
            this.btn_carinpm.Name = "btn_carinpm";
            this.btn_carinpm.Size = new System.Drawing.Size(138, 31);
            this.btn_carinpm.TabIndex = 2;
            this.btn_carinpm.Text = "Cari";
            this.btn_carinpm.UseVisualStyleBackColor = true;
            this.btn_carinpm.Click += new System.EventHandler(this.btn_carinpm_Click);
            // 
            // txtBox_npm
            // 
            this.txtBox_npm.Location = new System.Drawing.Point(10, 57);
            this.txtBox_npm.Name = "txtBox_npm";
            this.txtBox_npm.Size = new System.Drawing.Size(138, 26);
            this.txtBox_npm.TabIndex = 1;
            this.txtBox_npm.TextChanged += new System.EventHandler(this.txtBox_npm_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(59, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(13, 20);
            this.label11.TabIndex = 14;
            this.label11.Text = ":";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 20);
            this.label10.TabIndex = 0;
            this.label10.Text = "NPM ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SMSGatewayHonorDosen.Properties.Resources._6f01f191_595f_4d36_aa53_29c89ac8fb3d;
            this.pictureBox1.Location = new System.Drawing.Point(15, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 87);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // CetakKartu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(975, 711);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cR_CetakKartu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "CetakKartu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " ";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.CetakKartu_FormClosing);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer cR_CetakKartu;
        private System.Windows.Forms.ComboBox cmbBox_StatusUjian;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbBox_Semester;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbBox_Ta;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_carinpm;
        private System.Windows.Forms.TextBox txtBox_npm;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
    }
}

