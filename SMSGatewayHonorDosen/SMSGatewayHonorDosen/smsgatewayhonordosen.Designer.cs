﻿namespace SMSGatewayHonorDosen
{
    partial class smsgatewayhonordosen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.dgv_tabelPenerimaan = new System.Windows.Forms.DataGridView();
            this.btn_SendSMS = new System.Windows.Forms.Button();
            this.lbl9 = new System.Windows.Forms.Label();
            this.lbl8 = new System.Windows.Forms.Label();
            this.lbl10 = new System.Windows.Forms.Label();
            this.lbl4 = new System.Windows.Forms.Label();
            this.lbl7 = new System.Windows.Forms.Label();
            this.lbl6 = new System.Windows.Forms.Label();
            this.lbl5 = new System.Windows.Forms.Label();
            this.lbl3 = new System.Windows.Forms.Label();
            this.lbl2 = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.txtBox_Total = new System.Windows.Forms.TextBox();
            this.txtBox_Rate = new System.Windows.Forms.TextBox();
            this.txtBox_Pertemuan = new System.Windows.Forms.TextBox();
            this.txtBox_NoHP = new System.Windows.Forms.TextBox();
            this.txtBox_Nama = new System.Windows.Forms.TextBox();
            this.txtBox_Kode = new System.Windows.Forms.TextBox();
            this.gbx_InfoDosen = new System.Windows.Forms.GroupBox();
            this.txtBox_KodeSlip = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtBox_Status = new System.Windows.Forms.TextBox();
            this.cmbBox_KategoriGaji = new System.Windows.Forms.ComboBox();
            this.btn_Today = new System.Windows.Forms.Button();
            this.dTP_TanggalTransfer = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dTP_PeriodeAkhir = new System.Windows.Forms.DateTimePicker();
            this.dTP_PeriodeAwal = new System.Windows.Forms.DateTimePicker();
            this.btn_ImportFile = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_tabelPenerimaan)).BeginInit();
            this.gbx_InfoDosen.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(18, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 18);
            this.label4.TabIndex = 8;
            this.label4.Text = "3. Kategori Gaji :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(97, 59);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(441, 25);
            this.label15.TabIndex = 10;
            this.label15.Text = "BAGIAN KEUANGAN POLITEKNIK PRAKTISI";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(96, 26);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(554, 29);
            this.label14.TabIndex = 8;
            this.label14.Text = "FORM PENGGAJIAN DOSEN BY SMS GATEWAY";
            // 
            // dgv_tabelPenerimaan
            // 
            this.dgv_tabelPenerimaan.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_tabelPenerimaan.Location = new System.Drawing.Point(0, 248);
            this.dgv_tabelPenerimaan.Name = "dgv_tabelPenerimaan";
            this.dgv_tabelPenerimaan.ReadOnly = true;
            this.dgv_tabelPenerimaan.Size = new System.Drawing.Size(793, 197);
            this.dgv_tabelPenerimaan.TabIndex = 12;
            this.dgv_tabelPenerimaan.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_tabelPenerimaan_CellClick);
            //this.dgv_tabelPenerimaan.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_tabelPenerimaan_CellContentClick);
            // 
            // btn_SendSMS
            // 
            this.btn_SendSMS.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SendSMS.Location = new System.Drawing.Point(610, 153);
            this.btn_SendSMS.Name = "btn_SendSMS";
            this.btn_SendSMS.Size = new System.Drawing.Size(177, 34);
            this.btn_SendSMS.TabIndex = 9;
            this.btn_SendSMS.Text = "Send SMS";
            this.btn_SendSMS.UseVisualStyleBackColor = true;
            this.btn_SendSMS.Click += new System.EventHandler(this.btn_SendSMS_Click);
            // 
            // lbl9
            // 
            this.lbl9.AutoSize = true;
            this.lbl9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl9.Location = new System.Drawing.Point(577, 81);
            this.lbl9.Name = "lbl9";
            this.lbl9.Size = new System.Drawing.Size(0, 18);
            this.lbl9.TabIndex = 16;
            // 
            // lbl8
            // 
            this.lbl8.AutoSize = true;
            this.lbl8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl8.Location = new System.Drawing.Point(577, 53);
            this.lbl8.Name = "lbl8";
            this.lbl8.Size = new System.Drawing.Size(0, 18);
            this.lbl8.TabIndex = 15;
            // 
            // lbl10
            // 
            this.lbl10.AutoSize = true;
            this.lbl10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl10.Location = new System.Drawing.Point(700, 25);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(0, 18);
            this.lbl10.TabIndex = 14;
            // 
            // lbl4
            // 
            this.lbl4.AutoSize = true;
            this.lbl4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl4.Location = new System.Drawing.Point(19, 141);
            this.lbl4.Name = "lbl4";
            this.lbl4.Size = new System.Drawing.Size(0, 18);
            this.lbl4.TabIndex = 13;
            // 
            // lbl7
            // 
            this.lbl7.AutoSize = true;
            this.lbl7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl7.Location = new System.Drawing.Point(487, 81);
            this.lbl7.Name = "lbl7";
            this.lbl7.Size = new System.Drawing.Size(0, 18);
            this.lbl7.TabIndex = 12;
            // 
            // lbl6
            // 
            this.lbl6.AutoSize = true;
            this.lbl6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl6.Location = new System.Drawing.Point(487, 53);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(0, 18);
            this.lbl6.TabIndex = 11;
            // 
            // lbl5
            // 
            this.lbl5.AutoSize = true;
            this.lbl5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl5.Location = new System.Drawing.Point(487, 25);
            this.lbl5.Name = "lbl5";
            this.lbl5.Size = new System.Drawing.Size(0, 18);
            this.lbl5.TabIndex = 10;
            // 
            // lbl3
            // 
            this.lbl3.AutoSize = true;
            this.lbl3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl3.Location = new System.Drawing.Point(18, 111);
            this.lbl3.Name = "lbl3";
            this.lbl3.Size = new System.Drawing.Size(0, 18);
            this.lbl3.TabIndex = 9;
            // 
            // lbl2
            // 
            this.lbl2.AutoSize = true;
            this.lbl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2.Location = new System.Drawing.Point(18, 79);
            this.lbl2.Name = "lbl2";
            this.lbl2.Size = new System.Drawing.Size(0, 18);
            this.lbl2.TabIndex = 8;
            // 
            // lbl1
            // 
            this.lbl1.AutoSize = true;
            this.lbl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.Location = new System.Drawing.Point(18, 51);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(0, 18);
            this.lbl1.TabIndex = 7;
            // 
            // txtBox_Total
            // 
            this.txtBox_Total.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_Total.Location = new System.Drawing.Point(609, 82);
            this.txtBox_Total.Name = "txtBox_Total";
            this.txtBox_Total.ReadOnly = true;
            this.txtBox_Total.Size = new System.Drawing.Size(135, 24);
            this.txtBox_Total.TabIndex = 5;
            // 
            // txtBox_Rate
            // 
            this.txtBox_Rate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_Rate.Location = new System.Drawing.Point(610, 50);
            this.txtBox_Rate.Name = "txtBox_Rate";
            this.txtBox_Rate.ReadOnly = true;
            this.txtBox_Rate.Size = new System.Drawing.Size(134, 24);
            this.txtBox_Rate.TabIndex = 4;
            // 
            // txtBox_Pertemuan
            // 
            this.txtBox_Pertemuan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_Pertemuan.Location = new System.Drawing.Point(610, 22);
            this.txtBox_Pertemuan.Name = "txtBox_Pertemuan";
            this.txtBox_Pertemuan.ReadOnly = true;
            this.txtBox_Pertemuan.Size = new System.Drawing.Size(84, 24);
            this.txtBox_Pertemuan.TabIndex = 3;
            // 
            // txtBox_NoHP
            // 
            this.txtBox_NoHP.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_NoHP.Location = new System.Drawing.Point(145, 105);
            this.txtBox_NoHP.Name = "txtBox_NoHP";
            this.txtBox_NoHP.ReadOnly = true;
            this.txtBox_NoHP.Size = new System.Drawing.Size(152, 24);
            this.txtBox_NoHP.TabIndex = 2;
            // 
            // txtBox_Nama
            // 
            this.txtBox_Nama.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_Nama.Location = new System.Drawing.Point(145, 77);
            this.txtBox_Nama.Name = "txtBox_Nama";
            this.txtBox_Nama.ReadOnly = true;
            this.txtBox_Nama.Size = new System.Drawing.Size(152, 24);
            this.txtBox_Nama.TabIndex = 1;
            // 
            // txtBox_Kode
            // 
            this.txtBox_Kode.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_Kode.Location = new System.Drawing.Point(145, 49);
            this.txtBox_Kode.Name = "txtBox_Kode";
            this.txtBox_Kode.ReadOnly = true;
            this.txtBox_Kode.Size = new System.Drawing.Size(152, 24);
            this.txtBox_Kode.TabIndex = 0;
            // 
            // gbx_InfoDosen
            // 
            this.gbx_InfoDosen.Controls.Add(this.txtBox_KodeSlip);
            this.gbx_InfoDosen.Controls.Add(this.label5);
            this.gbx_InfoDosen.Controls.Add(this.label2);
            this.gbx_InfoDosen.Controls.Add(this.btn_SendSMS);
            this.gbx_InfoDosen.Controls.Add(this.lbl9);
            this.gbx_InfoDosen.Controls.Add(this.lbl8);
            this.gbx_InfoDosen.Controls.Add(this.lbl10);
            this.gbx_InfoDosen.Controls.Add(this.lbl4);
            this.gbx_InfoDosen.Controls.Add(this.lbl7);
            this.gbx_InfoDosen.Controls.Add(this.lbl6);
            this.gbx_InfoDosen.Controls.Add(this.lbl5);
            this.gbx_InfoDosen.Controls.Add(this.lbl3);
            this.gbx_InfoDosen.Controls.Add(this.lbl2);
            this.gbx_InfoDosen.Controls.Add(this.lbl1);
            this.gbx_InfoDosen.Controls.Add(this.txtBox_Status);
            this.gbx_InfoDosen.Controls.Add(this.txtBox_Total);
            this.gbx_InfoDosen.Controls.Add(this.txtBox_Rate);
            this.gbx_InfoDosen.Controls.Add(this.txtBox_Pertemuan);
            this.gbx_InfoDosen.Controls.Add(this.txtBox_NoHP);
            this.gbx_InfoDosen.Controls.Add(this.txtBox_Nama);
            this.gbx_InfoDosen.Controls.Add(this.txtBox_Kode);
            this.gbx_InfoDosen.Enabled = false;
            this.gbx_InfoDosen.Location = new System.Drawing.Point(1, 451);
            this.gbx_InfoDosen.Name = "gbx_InfoDosen";
            this.gbx_InfoDosen.Size = new System.Drawing.Size(793, 193);
            this.gbx_InfoDosen.TabIndex = 9;
            this.gbx_InfoDosen.TabStop = false;
            this.gbx_InfoDosen.Visible = false;
            // 
            // txtBox_KodeSlip
            // 
            this.txtBox_KodeSlip.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_KodeSlip.Location = new System.Drawing.Point(145, 19);
            this.txtBox_KodeSlip.Name = "txtBox_KodeSlip";
            this.txtBox_KodeSlip.Size = new System.Drawing.Size(152, 24);
            this.txtBox_KodeSlip.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(109, 18);
            this.label5.TabIndex = 18;
            this.label5.Text = "Kode Slip Gaji :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(450, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 18);
            this.label2.TabIndex = 17;
            // 
            // txtBox_Status
            // 
            this.txtBox_Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_Status.Location = new System.Drawing.Point(145, 138);
            this.txtBox_Status.Name = "txtBox_Status";
            this.txtBox_Status.ReadOnly = true;
            this.txtBox_Status.Size = new System.Drawing.Size(100, 24);
            this.txtBox_Status.TabIndex = 6;
            // 
            // cmbBox_KategoriGaji
            // 
            this.cmbBox_KategoriGaji.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBox_KategoriGaji.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBox_KategoriGaji.FormattingEnabled = true;
            this.cmbBox_KategoriGaji.Location = new System.Drawing.Point(179, 100);
            this.cmbBox_KategoriGaji.Name = "cmbBox_KategoriGaji";
            this.cmbBox_KategoriGaji.Size = new System.Drawing.Size(234, 26);
            this.cmbBox_KategoriGaji.TabIndex = 9;
            this.cmbBox_KategoriGaji.SelectedIndexChanged += new System.EventHandler(this.cmbBox_KategoriGaji_SelectedIndexChanged);
            // 
            // btn_Today
            // 
            this.btn_Today.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Today.Location = new System.Drawing.Point(419, 18);
            this.btn_Today.Name = "btn_Today";
            this.btn_Today.Size = new System.Drawing.Size(87, 25);
            this.btn_Today.TabIndex = 5;
            this.btn_Today.Text = "Today";
            this.btn_Today.UseVisualStyleBackColor = true;
            this.btn_Today.Click += new System.EventHandler(this.btn_Today_Click);
            // 
            // dTP_TanggalTransfer
            // 
            this.dTP_TanggalTransfer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dTP_TanggalTransfer.Location = new System.Drawing.Point(180, 19);
            this.dTP_TanggalTransfer.Name = "dTP_TanggalTransfer";
            this.dTP_TanggalTransfer.Size = new System.Drawing.Size(233, 24);
            this.dTP_TanggalTransfer.TabIndex = 4;
            this.dTP_TanggalTransfer.ValueChanged += new System.EventHandler(this.dTP_TanggalTransfer_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(18, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(147, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "1. Tanggal Transfer : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 18);
            this.label1.TabIndex = 6;
            this.label1.Text = "2. Periode Gaji : ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.dTP_PeriodeAkhir);
            this.groupBox1.Controls.Add(this.dTP_PeriodeAwal);
            this.groupBox1.Controls.Add(this.btn_ImportFile);
            this.groupBox1.Controls.Add(this.cmbBox_KategoriGaji);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btn_Today);
            this.groupBox1.Controls.Add(this.dTP_TanggalTransfer);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(1, 87);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(792, 155);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(399, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(14, 20);
            this.label6.TabIndex = 16;
            this.label6.Text = "-";
            // 
            // dTP_PeriodeAkhir
            // 
            this.dTP_PeriodeAkhir.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dTP_PeriodeAkhir.Location = new System.Drawing.Point(442, 59);
            this.dTP_PeriodeAkhir.Name = "dTP_PeriodeAkhir";
            this.dTP_PeriodeAkhir.Size = new System.Drawing.Size(176, 24);
            this.dTP_PeriodeAkhir.TabIndex = 15;
            this.dTP_PeriodeAkhir.ValueChanged += new System.EventHandler(this.dTP_PeriodeAkhir_ValueChanged);
            // 
            // dTP_PeriodeAwal
            // 
            this.dTP_PeriodeAwal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dTP_PeriodeAwal.Location = new System.Drawing.Point(180, 59);
            this.dTP_PeriodeAwal.Name = "dTP_PeriodeAwal";
            this.dTP_PeriodeAwal.Size = new System.Drawing.Size(180, 24);
            this.dTP_PeriodeAwal.TabIndex = 14;
            this.dTP_PeriodeAwal.ValueChanged += new System.EventHandler(this.dTP_PeriodeAwal_ValueChanged);
            // 
            // btn_ImportFile
            // 
            this.btn_ImportFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ImportFile.Location = new System.Drawing.Point(672, 103);
            this.btn_ImportFile.Name = "btn_ImportFile";
            this.btn_ImportFile.Size = new System.Drawing.Size(96, 33);
            this.btn_ImportFile.TabIndex = 11;
            this.btn_ImportFile.Text = "Import File";
            this.btn_ImportFile.UseVisualStyleBackColor = true;
            this.btn_ImportFile.Click += new System.EventHandler(this.btn_ImportFile_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SMSGatewayHonorDosen.Properties.Resources._6f01f191_595f_4d36_aa53_29c89ac8fb3d;
            this.pictureBox1.Location = new System.Drawing.Point(13, 23);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(78, 69);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 13;
            this.pictureBox1.TabStop = false;
            // 
            // smsgatewayhonordosen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(795, 656);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.dgv_tabelPenerimaan);
            this.Controls.Add(this.gbx_InfoDosen);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "smsgatewayhonordosen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SMS GATEWAY GAJI DOSEN";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.smsgatewayhonordosen_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_tabelPenerimaan)).EndInit();
            this.gbx_InfoDosen.ResumeLayout(false);
            this.gbx_InfoDosen.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView dgv_tabelPenerimaan;
        private System.Windows.Forms.Button btn_SendSMS;
        private System.Windows.Forms.Label lbl9;
        private System.Windows.Forms.Label lbl8;
        private System.Windows.Forms.Label lbl10;
        private System.Windows.Forms.Label lbl4;
        private System.Windows.Forms.Label lbl7;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.Label lbl5;
        private System.Windows.Forms.Label lbl3;
        private System.Windows.Forms.Label lbl2;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.TextBox txtBox_Total;
        private System.Windows.Forms.TextBox txtBox_Rate;
        private System.Windows.Forms.TextBox txtBox_Pertemuan;
        private System.Windows.Forms.TextBox txtBox_NoHP;
        private System.Windows.Forms.TextBox txtBox_Nama;
        private System.Windows.Forms.TextBox txtBox_Kode;
        private System.Windows.Forms.GroupBox gbx_InfoDosen;
        private System.Windows.Forms.ComboBox cmbBox_KategoriGaji;
        private System.Windows.Forms.Button btn_Today;
        private System.Windows.Forms.DateTimePicker dTP_TanggalTransfer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_ImportFile;
        private System.Windows.Forms.TextBox txtBox_Status;
        private System.Windows.Forms.TextBox txtBox_KodeSlip;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dTP_PeriodeAwal;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker dTP_PeriodeAkhir;
    }
}

