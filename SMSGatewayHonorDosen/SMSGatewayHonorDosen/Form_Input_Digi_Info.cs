﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using SMSGatewayHonorDosen;
using MainFormKeuangan;


namespace Aplikasi_Main_Akademik
{
    public partial class Form_Input_Digi_Info : Form
    {
        MySqlConnection conn = new MySqlConnection(Constanta.conString);
        MySqlCommand cmd;
        MySqlDataAdapter adp;
        MySqlDataReader dReader;

        Pegawai pegawai = new Pegawai();

        public Form_Input_Digi_Info()
        {
            InitializeComponent();

            pegawai.NIK = Form_Login.nik;
            Lbl_Bagian.Text = pegawai.GetBagian(conn, cmd, dReader);

            TampilanGrid();
            pegawai.GetPengumuman(conn, cmd, adp, DGV_DaftarPengumuman);
        }

        private void TampilanGrid()
        {
            DGV_DaftarPengumuman.ColumnCount = 4;

            DGV_DaftarPengumuman.Columns[0].Name = "No";
            DGV_DaftarPengumuman.Columns[1].Name = "Pengumuman";
            DGV_DaftarPengumuman.Columns[2].Name = "Tanggal Input";
            DGV_DaftarPengumuman.Columns[3].Name = "id_info";
            DGV_DaftarPengumuman.Columns[3].Visible = false;

            for (int i = 0; i < DGV_DaftarPengumuman.ColumnCount; i++)
            {
                DGV_DaftarPengumuman.Columns[i].ReadOnly = true;
            }

            DataGridViewCheckBoxColumn dgvChkBoxStatusTampil = new DataGridViewCheckBoxColumn();
            dgvChkBoxStatusTampil.HeaderText = "Tampilkan";
            dgvChkBoxStatusTampil.TrueValue = "TRUE";
            dgvChkBoxStatusTampil.FalseValue = "FALSE";

            DGV_DaftarPengumuman.Columns.Add(dgvChkBoxStatusTampil);

            DGV_DaftarPengumuman.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            DGV_DaftarPengumuman.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            DGV_DaftarPengumuman.AllowUserToAddRows = false;
        }

        private void Btn_Tambah_Click(object sender, EventArgs e)
        {
            if (pegawai.InsertPengumuman(conn, cmd, RtBox_Pengumuman.Text, "FALSE", DateTime.Now))
            {
                MessageBox.Show("Pengumuman telah berhasil diinput");
                RtBox_Pengumuman.Clear();
                pegawai.GetPengumuman(conn, cmd, adp, DGV_DaftarPengumuman);
            }
        }

        private void Form_Utama_FormClosed(object sender, FormClosedEventArgs e)
        {
            MainKeuanganForm.isFormAnak = !MainKeuanganForm.isFormAnak;
        }

        private void Btn_Publish_Click(object sender, EventArgs e)
        {
            string status;
            string idInfo;

            List<bool> publish = new List<bool>();

            for (int i = 0; i < DGV_DaftarPengumuman.RowCount; i++)
            {
                idInfo= DGV_DaftarPengumuman.Rows[i].Cells[3].Value.ToString();
                status = DGV_DaftarPengumuman.Rows[i].Cells[4].Value.ToString();

                if (pegawai.UpdateStatusTampil(conn, cmd, adp, status, idInfo))
                {
                    publish.Add(true);
                }
                else
	            {
                    publish.Add(false);
	            }
            }

            if (!publish.Contains(false))
            {
                MessageBox.Show("Pengumuman telah berhasil dipublish");
                pegawai.GetPengumuman(conn, cmd, adp, DGV_DaftarPengumuman);
            }
        }

        string idInfo;
        private void DGV_DaftarPengumuman_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            idInfo = DGV_DaftarPengumuman.Rows[e.RowIndex].Cells[3].Value.ToString();
        }

        private void Btn_Hapus_Click(object sender, EventArgs e)
        {
            if (idInfo != "")
            {
                if (pegawai.DeletePengumuman(conn, cmd, adp, idInfo))
                {
                    MessageBox.Show("Data pengumuman telah berhasil dihapus");
                    pegawai.GetPengumuman(conn, cmd, adp, DGV_DaftarPengumuman);
                }
            }
        }
    }
}
