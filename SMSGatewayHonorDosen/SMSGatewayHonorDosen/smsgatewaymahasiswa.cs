﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using MySql.Data.MySqlClient;

namespace SMSGatewayHonorDosen
{
    public partial class smsgatewaymahasiswa : Form
    {
        MySqlConnection connection;
        MySqlCommand cmd;
        MySqlDataAdapter adp;

        RegClass regClass = new RegClass();

        string query;

        public smsgatewaymahasiswa()
        {
            InitializeComponent();

            //Constanta.server = regClass.ReadFromRegistry("ip");

            rbtn_opsi1.Checked = false;
            rbtn_opsi2.Checked = false;

            isEnable();

            Constanta.Server();
            connection = new MySqlConnection(Constanta.conString);

            //if (isConnected())
            //{
            //    GetDataAngkatan();
            //    GetDataJurusan();
            //    //GetDataKelasWhereAngkatandanKelas();
            //}

            //FillComboBoxAngkatan();

        }

        private void smsgatewaymahasiswa_Load(object sender, EventArgs e)
        {
            if (isConnected())
            {
                //isiDataAngkatan
                GetDataAngkatan();

                //isiDataJurusan
                GetDataJurusan();
            }
            //isiComboboxAngkatan
            FillComboBoxAngkatan();
            //isiComboboxjurusan
            FillComboBoxjurusan();
        }

        List<string> dataangkatan = new List<string>();

        bool isConnected()
        {
            bool konek = true;

            try
            {
                connection.Open();

                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            return konek;
        }

        void GetDataAngkatan()
        {
            dataangkatan.Clear();

            query = "SELECT DISTINCT kode_angkatan FROM tb_mahasiswa";
            cmd = new MySqlCommand(query, connection);

            try
            {
                adp = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    dataangkatan.Add(r[0].ToString());
                    //MessageBox.Show(r[0].ToString());
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void FillComboBoxAngkatan()
        {
            cmbBox_Angkatan.Items.Clear();
            foreach (string s in dataangkatan)
            {
                cmbBox_Angkatan.Items.Add(s);
            }
        }

        List<string> kodejur = new List<string>();
        List<string> namajur = new List<string>();
        
        //List<string> angkatan = new List<string>();

        //List<string> kodejurSemuaAngkatan = new List<string>();
        //List<string> jurSemuaAngkatan = new List<string>();
        //List<string> kodeAngkatanYangAda = new List<string>();

        // Diget dulu semua setelah itu, disortir lagi dengan 

        //void GetDataJurusSemuaAngkatan()
        //{
        //    kodejurSemuaAngkatan.Clear();
        //    jurSemuaAngkatan.Clear();
        //    kodeAngkatanYangAda.Clear();

        //    query = "SELECT DISTINCT tb_jurusan.kode_jurusan, tb_jurusan.nama_jurusan, tb_mahasiswa.kode_angkatan FROM tb_mahasiswa INNER JOIN tb_jurusan ON tb_jurusan.kode_jurusan = tb_mahasiswa.kode_jurusan";
        //    cmd = new MySqlCommand(query, connection);

        //    try
        //    {
        //        adp = new MySqlDataAdapter(cmd);
        //        DataTable dt = new DataTable();
        //        adp.Fill(dt);

        //        foreach (DataRow r in dt.Rows)
        //        {
        //            kodejurSemuaAngkatan.Add(r[0].ToString());
        //            jurSemuaAngkatan.Add(r[1].ToString());
        //            kodeAngkatanYangAda.Add(r[2].ToString());
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //    }
        //}

        void GetDataJurusan()
        {
            kodejur.Clear();
            namajur.Clear();

            query = "SELECT DISTINCT tb_mahasiswa.kode_jurusan, tb_jurusan.nama_jurusan FROM tb_mahasiswa INNER JOIN tb_jurusan ON tb_jurusan.kode_jurusan = tb_mahasiswa.kode_jurusan";
            cmd = new MySqlCommand(query, connection);

            try
            {
                adp = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    kodejur.Add(r[0].ToString());
                    namajur.Add(r[1].ToString());
                    //angkatan.Add(r[3].ToString());
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void FillComboBoxjurusan()
        {
            cmbBox_Jurusan.Items.Clear();

            string pil = cmbBox_Angkatan.Text;

            //for (int i = 0; i < dataangkatan.Count; i++)
            //{
            //    if (pil == dataangkatan[i])
            //    {
                    for (int j = 0; j < namajur.Count ; j++)
                    {
                        cmbBox_Jurusan.Items.Add(namajur[j]);
                    }
            //    }
            //}
        }

        List<string> kodeKelas = new List<string>();
        List<string> namaKelas = new List<string>();

        void GetDataKelasWhereAngkatandanKelas(string ang, string jur)
        {
            kodeKelas.Clear();
            namaKelas.Clear();

            query = "SELECT DISTINCT tb_mahasiswa.kode_kelas, tb_kelas.nama_kelas FROM tb_mahasiswa INNER JOIN tb_kelas ON tb_kelas.kode_kelas = tb_mahasiswa.kode_kelas WHERE kode_angkatan='" + ang + "' AND kode_jurusan ='" + jur + "'";
            cmd = new MySqlCommand(query, connection);

            try
            {
                adp = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    kodeKelas.Add(r[0].ToString()); 
                    namaKelas.Add(r[1].ToString());
                    
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void FillComboBoxKelas()
        {
            cmbBox_Kelas.Items.Clear();

            try
            {
                GetDataKelasWhereAngkatandanKelas(cmbBox_Angkatan.Text, kodejur[int.Parse(cmbBox_Jurusan.SelectedIndex.ToString())]);

            }
            catch (Exception ex)
            {
                
            }
                
            for (int i = 0; i < namaKelas.Count; i++)
            {
                cmbBox_Kelas.Items.Add(namaKelas[i]);
            }
        }

        private void cmbBox_Angkatan_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbBox_Jurusan.Enabled = true;
            HapusIsiListbox();
            cmbBox_Jurusan.SelectedIndex = -1;
            cmbBox_Kelas.SelectedIndex = -1;
            cmbBox_Kelas.Enabled = false;
        }

        private void cmbBox_Jurusan_SelectedIndexChanged(object sender, EventArgs e)
        {
            //MessageBox.Show(cmbBox_Jurusan.SelectedIndex.ToString());
            
           cmbBox_Kelas.Enabled = true;
           FillComboBoxKelas();
           HapusIsiListbox();
        }

        private void cmbBox_Kelas_SelectedIndexChanged(object sender, EventArgs e)
        {
            lstBox_noHp.Items.Clear();
            lsbox_tidakadaHP.Items.Clear();

            if (cmbBox_Angkatan.SelectedIndex != -1 &&
                cmbBox_Jurusan.SelectedIndex != -1 &&
                cmbBox_Kelas.SelectedIndex != -1)
            {
                IsiDataNohp(cmbBox_Angkatan.Text, kodejur[cmbBox_Jurusan.SelectedIndex], kodeKelas[cmbBox_Kelas.SelectedIndex]);
            }
        }

        List<string> noHp = new List<string>();
        List<string> npmTidakAdanoHp = new List<string>();

        void IsiDataNohp(string ang, string jur, string kls)
        {
            noHp.Clear();
            npmTidakAdanoHp.Clear();
            
            query = "SELECT nomor_hp, npm_mhs, nama_mhs FROM tb_mahasiswa WHERE kode_angkatan = '" + ang + "' AND kode_jurusan ='" + jur + "' AND kode_kelas ='" + kls + "'";
            cmd = new MySqlCommand(query, connection);

            try
            {
                adp = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    if (r[0].ToString() != "")
                    {
                        lstBox_noHp.Items.Add(r[0].ToString() + " - " + r[2].ToString() );
                        noHp.Add(r[0].ToString());
                    }
                    else 
                    {
                        lsbox_tidakadaHP.Items.Add(r[1].ToString() + " - " + r[2].ToString());
                        npmTidakAdanoHp.Add(r[1].ToString());
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void HapusIsiListbox()
        {
            lstBox_noHp.Items.Clear();
            lsbox_tidakadaHP.Items.Clear();
        }
        
        void SetUlangComboBox()
        {
            cmbBox_Kelas.SelectedIndex = -1;
            cmbBox_Angkatan.SelectedIndex = -1;
            cmbBox_Jurusan.SelectedIndex = -1;
        }

        void hapusIsiListboxOpsi1()
        {
            listbox_NoHp.Items.Clear();
        }
       
        bool isEnable()
        {
            bool enable = false;

            groupBox1.Enabled = enable;
            groupBox2.Enabled = enable;

            return enable;
        }

        //Isi Opsi 1
        List<string> noHpOpsi1 = new List<string>();
        private void btn_InputNoHP_Click(object sender, EventArgs e)
        {
            YangDilakukanButtonInput();
            if (lbl_StatusStrip.Text == "Ready")
            {
                lbl_StatusStrip.Text = "Working";
            }
            else 
            {
                lbl_StatusStrip.Text = "Ready";
            }
        }

        void YangDilakukanButtonInput()
        {
            string text = ConvertNomor(txtBox_NoHp.Text);
            bool adalahSama = false;

            if (text != "")
            {
                if (listbox_NoHp.Items.Count > 0)
                {
                    for (int i = 0; i < listbox_NoHp.Items.Count; i++)
                    {
                        //MessageBox.Show(listbox_NoHp.Items[i].ToString());

                        if (listbox_NoHp.Items[i].ToString() == text)
                        {
                            adalahSama = true;
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("Silahkan Inputkan Nomor Handphone Terlebih Dahulu");
            }

            if (!adalahSama)
            {
                listbox_NoHp.Items.Add(text);
                noHpOpsi1.Add(text);
            }
            else
            {
                MessageBox.Show("Nomor " + text + " telah diinputkan");
            }

            txtBox_NoHp.Clear();
        }

        string ConvertNomor(string text)
        {
            string s = text;
            string mod = text.Substring(1, text.Length - 1);

            if (text.Substring(0,1) == "0")
                s = "+62" + mod;

            return s;
        }

        private void optionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Ini untuk Buka tombol opsi
            string x = Interaction.InputBox("Developer Team Account : ", "Entering To Configuration", "");
            if (x == "UPT Pengembangan Sisfo Praktisi")
            {
                Option option = new Option();
                option.ShowDialog();
            }
            else
            {
                MessageBox.Show("Configuration is Denied");
            }
        }

        private void settingConfigurationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string x = Interaction.InputBox("Developer Team Account : ", "Entering To Configuration", "");
            if (x == "UPT Pengembangan Sisfo Praktisi")
            {
                SettingConfiguration fset = new SettingConfiguration();
                fset.ShowDialog();
            }
            else
            {
                MessageBox.Show("Configuration is Denied");
            }
        }

        private void rbtn_opsi1_CheckedChanged(object sender, EventArgs e)
        {
            isEnable();
            groupBox1.Enabled = true;

            HapusIsiListbox();
            SetUlangComboBox();
        }

        private void rbtn_opsi2_CheckedChanged(object sender, EventArgs e)
        {
            isEnable();
            groupBox2.Enabled = true;

            hapusIsiListboxOpsi1();
            cmbBox_Jurusan.Enabled = false;
            cmbBox_Kelas.Enabled = false;
        }

        private void btn_Kirim_Click(object sender, EventArgs e)
        {
            try
            {
                //kirimSMS(nohp , rchBox_TextSms.Text);
                if (rbtn_opsi1.Checked == true)
                {
                    //MessageBox.Show(noHpOpsi1.Count.ToString());
                    for (int i = 0; i < noHpOpsi1.Count; i++)
                    {
                        //MessageBox.Show("a");
                        kirimSMS(noHpOpsi1[i], rchBox_TextSms.Text);
                        if (i == noHpOpsi1.Count - 1 )
                        {
                            MessageBox.Show("Message has been inputed");
                        }
                    }

                    hapusIsiListboxOpsi1();
                    rchBox_TextSms.Text = "";
                }
                else if(rbtn_opsi2.Checked == true)
                {
                    //MessageBox.Show(noHp.Count.ToString());
                    for (int j = 0; j < noHp.Count; j++)
                    {
                        kirimSMS(noHp[j], rchBox_TextSms.Text);
                        if (j == noHp.Count - 1)
                        {
                            MessageBox.Show("Message has been inputed");
                        }
                    }
                    //SetUlangComboBox();
                    cmbBox_Jurusan.SelectedIndex = -1;
                    cmbBox_Kelas.SelectedIndex = -1;
                    //cmbBox_Jurusan.Enabled = false;
                    cmbBox_Kelas.Enabled = false;
                    HapusIsiListbox();
                }

                if (lbl_StatusStrip.Text == "Ready")
                {
                    lbl_StatusStrip.Text = "Working";
                }
                else
                {
                    lbl_StatusStrip.Text = "Ready";
                }


            }
            catch (Exception ex)
            {
                
            }
        }

        private void kirimSMS(string nohp, string text)
        {
            query = "INSERT INTO outbox (DestinationNumber, TextDecoded, DeliveryReport, CreatorID) VALUES (@dn, @td, 'no', 'local')";
            cmd = new MySqlCommand(query, connection);
            cmd.Parameters.AddWithValue("@dn", nohp);
            cmd.Parameters.AddWithValue("@td", text);
            try
            {
                connection.Open();
                if (cmd.ExecuteNonQuery() > 0)
                {

                }
                connection.Close();
             }
                catch (Exception ex)
             {
                MessageBox.Show(ex.Message);
             }
        }

        private void btn_Hapus_Click(object sender, EventArgs e)
        {
            if (listbox_NoHp.SelectedIndex != -1)
            {
                listbox_NoHp.Items.RemoveAt(listbox_NoHp.SelectedIndex);
                if (lbl_StatusStrip.Text == "Ready")
                {
                    lbl_StatusStrip.Text = "Working";
                }
            }
            else
            {
                lbl_StatusStrip.Text = "Ready";
            }
        }

        private void smsgatewaymahasiswa_FormClosed(object sender, FormClosedEventArgs e)
        {
            MainKeuanganForm.isFormAnak = false;
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

     }
}
