﻿namespace SMSGatewayHonorDosen
{
    partial class SettingConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtbox_ipaddress = new System.Windows.Forms.TextBox();
            this.btn_cekKoneksi = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_setIP = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtbox_ipaddress
            // 
            this.txtbox_ipaddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtbox_ipaddress.Location = new System.Drawing.Point(48, 63);
            this.txtbox_ipaddress.Name = "txtbox_ipaddress";
            this.txtbox_ipaddress.Size = new System.Drawing.Size(220, 26);
            this.txtbox_ipaddress.TabIndex = 1;
            this.txtbox_ipaddress.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // btn_cekKoneksi
            // 
            this.btn_cekKoneksi.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cekKoneksi.Location = new System.Drawing.Point(8, 117);
            this.btn_cekKoneksi.Name = "btn_cekKoneksi";
            this.btn_cekKoneksi.Size = new System.Drawing.Size(302, 32);
            this.btn_cekKoneksi.TabIndex = 2;
            this.btn_cekKoneksi.Text = "Cek Koneksi";
            this.btn_cekKoneksi.UseVisualStyleBackColor = true;
            this.btn_cekKoneksi.Click += new System.EventHandler(this.btn_cekKoneksi_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(195, 19);
            this.label3.TabIndex = 14;
            this.label3.Text = "KONFIGURASI SERVER";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(150, 19);
            this.label4.TabIndex = 13;
            this.label4.Text = "TELEMARKETING";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(87, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 19);
            this.label1.TabIndex = 10;
            this.label1.Text = "Server IP Address";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_setIP);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtbox_ipaddress);
            this.groupBox1.Controls.Add(this.btn_cekKoneksi);
            this.groupBox1.Location = new System.Drawing.Point(12, 68);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(316, 211);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "IP Setting";
            // 
            // btn_setIP
            // 
            this.btn_setIP.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_setIP.Location = new System.Drawing.Point(8, 160);
            this.btn_setIP.Name = "btn_setIP";
            this.btn_setIP.Size = new System.Drawing.Size(302, 32);
            this.btn_setIP.TabIndex = 13;
            this.btn_setIP.Text = "Set IP To App";
            this.btn_setIP.UseVisualStyleBackColor = true;
            this.btn_setIP.Click += new System.EventHandler(this.btn_setIP_Click);
            // 
            // SettingConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(346, 291);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox1);
            this.Name = "SettingConfiguration";
            this.Text = "Setting Configuration";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtbox_ipaddress;
        private System.Windows.Forms.Button btn_cekKoneksi;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_setIP;
    }
}