﻿namespace SMSGatewayHonorDosen
{
    partial class ReminderPembayaranUASUTS_mhs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox_Nohp = new System.Windows.Forms.ListBox();
            this.txtBox_NoHP = new System.Windows.Forms.TextBox();
            this.btn_Input = new System.Windows.Forms.Button();
            this.btn_Hapus = new System.Windows.Forms.Button();
            this.grupBox_Opsicmb = new System.Windows.Forms.GroupBox();
            this.cmbBox_Kelas = new System.Windows.Forms.ComboBox();
            this.cmbBox_Jurusan = new System.Windows.Forms.ComboBox();
            this.cmbBox_Angkatan = new System.Windows.Forms.ComboBox();
            this.chekBox_Opsi = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.RadioBtn_opsi2 = new System.Windows.Forms.RadioButton();
            this.RadioBtn_opsi1 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_Kirim = new System.Windows.Forms.Button();
            this.rchBox_TextSms = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.optionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingConfigurationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grupBox_Opsicmb.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox_Nohp
            // 
            this.listBox_Nohp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBox_Nohp.FormattingEnabled = true;
            this.listBox_Nohp.ItemHeight = 20;
            this.listBox_Nohp.Location = new System.Drawing.Point(18, 162);
            this.listBox_Nohp.Name = "listBox_Nohp";
            this.listBox_Nohp.Size = new System.Drawing.Size(135, 244);
            this.listBox_Nohp.TabIndex = 0;
            // 
            // txtBox_NoHP
            // 
            this.txtBox_NoHP.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_NoHP.Location = new System.Drawing.Point(18, 49);
            this.txtBox_NoHP.Name = "txtBox_NoHP";
            this.txtBox_NoHP.Size = new System.Drawing.Size(135, 26);
            this.txtBox_NoHP.TabIndex = 1;
            // 
            // btn_Input
            // 
            this.btn_Input.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Input.Location = new System.Drawing.Point(18, 78);
            this.btn_Input.Name = "btn_Input";
            this.btn_Input.Size = new System.Drawing.Size(135, 30);
            this.btn_Input.TabIndex = 2;
            this.btn_Input.Text = "Input";
            this.btn_Input.UseVisualStyleBackColor = true;
            this.btn_Input.Click += new System.EventHandler(this.btn_Input_Click);
            // 
            // btn_Hapus
            // 
            this.btn_Hapus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Hapus.Location = new System.Drawing.Point(18, 412);
            this.btn_Hapus.Name = "btn_Hapus";
            this.btn_Hapus.Size = new System.Drawing.Size(135, 28);
            this.btn_Hapus.TabIndex = 3;
            this.btn_Hapus.Text = "Hapus";
            this.btn_Hapus.UseVisualStyleBackColor = true;
            this.btn_Hapus.Click += new System.EventHandler(this.btn_Hapus_Click);
            // 
            // grupBox_Opsicmb
            // 
            this.grupBox_Opsicmb.Controls.Add(this.cmbBox_Kelas);
            this.grupBox_Opsicmb.Controls.Add(this.cmbBox_Jurusan);
            this.grupBox_Opsicmb.Controls.Add(this.cmbBox_Angkatan);
            this.grupBox_Opsicmb.Controls.Add(this.chekBox_Opsi);
            this.grupBox_Opsicmb.Controls.Add(this.label5);
            this.grupBox_Opsicmb.Controls.Add(this.label4);
            this.grupBox_Opsicmb.Controls.Add(this.label3);
            this.grupBox_Opsicmb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grupBox_Opsicmb.Location = new System.Drawing.Point(230, 130);
            this.grupBox_Opsicmb.Name = "grupBox_Opsicmb";
            this.grupBox_Opsicmb.Size = new System.Drawing.Size(313, 185);
            this.grupBox_Opsicmb.TabIndex = 4;
            this.grupBox_Opsicmb.TabStop = false;
            // 
            // cmbBox_Kelas
            // 
            this.cmbBox_Kelas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBox_Kelas.FormattingEnabled = true;
            this.cmbBox_Kelas.Location = new System.Drawing.Point(99, 148);
            this.cmbBox_Kelas.Name = "cmbBox_Kelas";
            this.cmbBox_Kelas.Size = new System.Drawing.Size(196, 28);
            this.cmbBox_Kelas.TabIndex = 6;
            // 
            // cmbBox_Jurusan
            // 
            this.cmbBox_Jurusan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBox_Jurusan.FormattingEnabled = true;
            this.cmbBox_Jurusan.Location = new System.Drawing.Point(99, 101);
            this.cmbBox_Jurusan.Name = "cmbBox_Jurusan";
            this.cmbBox_Jurusan.Size = new System.Drawing.Size(196, 28);
            this.cmbBox_Jurusan.TabIndex = 5;
            this.cmbBox_Jurusan.SelectedIndexChanged += new System.EventHandler(this.cmbBox_Jurusan_SelectedIndexChanged);
            // 
            // cmbBox_Angkatan
            // 
            this.cmbBox_Angkatan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBox_Angkatan.FormattingEnabled = true;
            this.cmbBox_Angkatan.Location = new System.Drawing.Point(99, 54);
            this.cmbBox_Angkatan.Name = "cmbBox_Angkatan";
            this.cmbBox_Angkatan.Size = new System.Drawing.Size(196, 28);
            this.cmbBox_Angkatan.TabIndex = 4;
            this.cmbBox_Angkatan.SelectedValueChanged += new System.EventHandler(this.cmbBox_Angkatan_SelectedValueChanged);
            // 
            // chekBox_Opsi
            // 
            this.chekBox_Opsi.AutoSize = true;
            this.chekBox_Opsi.Location = new System.Drawing.Point(6, 20);
            this.chekBox_Opsi.Name = "chekBox_Opsi";
            this.chekBox_Opsi.Size = new System.Drawing.Size(263, 24);
            this.chekBox_Opsi.TabIndex = 3;
            this.chekBox_Opsi.Text = "Centang, Jika akan pilih satu opsi";
            this.chekBox_Opsi.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 20);
            this.label5.TabIndex = 2;
            this.label5.Text = "Kelas :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 20);
            this.label4.TabIndex = 1;
            this.label4.Text = "Jurusan :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Angkatan :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.listBox_Nohp);
            this.groupBox2.Controls.Add(this.btn_Hapus);
            this.groupBox2.Controls.Add(this.txtBox_NoHP);
            this.groupBox2.Controls.Add(this.btn_Input);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(19, 130);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(182, 493);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 139);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 20);
            this.label8.TabIndex = 5;
            this.label8.Text = "Daftar No HP :";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 20);
            this.label7.TabIndex = 4;
            this.label7.Text = "No Hp :";
            // 
            // RadioBtn_opsi2
            // 
            this.RadioBtn_opsi2.AutoSize = true;
            this.RadioBtn_opsi2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioBtn_opsi2.Location = new System.Drawing.Point(230, 107);
            this.RadioBtn_opsi2.Name = "RadioBtn_opsi2";
            this.RadioBtn_opsi2.Size = new System.Drawing.Size(72, 24);
            this.RadioBtn_opsi2.TabIndex = 6;
            this.RadioBtn_opsi2.Text = "Opsi 2";
            this.RadioBtn_opsi2.UseVisualStyleBackColor = true;
            this.RadioBtn_opsi2.Click += new System.EventHandler(this.RadioBtn_opsi2_Click);
            // 
            // RadioBtn_opsi1
            // 
            this.RadioBtn_opsi1.AutoSize = true;
            this.RadioBtn_opsi1.Checked = true;
            this.RadioBtn_opsi1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RadioBtn_opsi1.Location = new System.Drawing.Point(19, 107);
            this.RadioBtn_opsi1.Name = "RadioBtn_opsi1";
            this.RadioBtn_opsi1.Size = new System.Drawing.Size(72, 24);
            this.RadioBtn_opsi1.TabIndex = 7;
            this.RadioBtn_opsi1.TabStop = true;
            this.RadioBtn_opsi1.Text = "Opsi 1";
            this.RadioBtn_opsi1.UseVisualStyleBackColor = true;
            this.RadioBtn_opsi1.Click += new System.EventHandler(this.RadioBtn_opsi1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(104, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(400, 29);
            this.label1.TabIndex = 8;
            this.label1.Text = "SMS GATEWAY PEMBERITAHUAN";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(104, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(243, 25);
            this.label2.TabIndex = 9;
            this.label2.Text = "Pembayaran UAS - UTS";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SMSGatewayHonorDosen.Properties.Resources._6f01f191_595f_4d36_aa53_29c89ac8fb3d;
            this.pictureBox1.Location = new System.Drawing.Point(20, 34);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(78, 69);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn_Kirim);
            this.groupBox3.Controls.Add(this.rchBox_TextSms);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(230, 321);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(313, 302);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            // 
            // btn_Kirim
            // 
            this.btn_Kirim.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Kirim.Location = new System.Drawing.Point(135, 249);
            this.btn_Kirim.Name = "btn_Kirim";
            this.btn_Kirim.Size = new System.Drawing.Size(124, 29);
            this.btn_Kirim.TabIndex = 4;
            this.btn_Kirim.Text = "Kirim";
            this.btn_Kirim.UseVisualStyleBackColor = true;
            this.btn_Kirim.Click += new System.EventHandler(this.btn_Kirim_Click);
            // 
            // rchBox_TextSms
            // 
            this.rchBox_TextSms.Location = new System.Drawing.Point(54, 25);
            this.rchBox_TextSms.Name = "rchBox_TextSms";
            this.rchBox_TextSms.Size = new System.Drawing.Size(205, 218);
            this.rchBox_TextSms.TabIndex = 1;
            this.rchBox_TextSms.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "Text :";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 634);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(555, 22);
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "lll";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionToolStripMenuItem,
            this.settingConfigurationToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(555, 24);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // optionToolStripMenuItem
            // 
            this.optionToolStripMenuItem.Name = "optionToolStripMenuItem";
            this.optionToolStripMenuItem.Size = new System.Drawing.Size(56, 20);
            this.optionToolStripMenuItem.Text = "Option";
            this.optionToolStripMenuItem.Click += new System.EventHandler(this.optionToolStripMenuItem_Click);
            // 
            // settingConfigurationToolStripMenuItem
            // 
            this.settingConfigurationToolStripMenuItem.Name = "settingConfigurationToolStripMenuItem";
            this.settingConfigurationToolStripMenuItem.Size = new System.Drawing.Size(133, 20);
            this.settingConfigurationToolStripMenuItem.Text = "Setting Configuration";
            this.settingConfigurationToolStripMenuItem.Click += new System.EventHandler(this.settingConfigurationToolStripMenuItem_Click);
            // 
            // ReminderPembayaranUASUTS_mhs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(555, 656);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.RadioBtn_opsi1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RadioBtn_opsi2);
            this.Controls.Add(this.grupBox_Opsicmb);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ReminderPembayaranUASUTS_mhs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Reminder Pembayaran UAS - UTS";
            this.Load += new System.EventHandler(this.ReminderPembayaranUASUTS_mhs_Load);
            this.grupBox_Opsicmb.ResumeLayout(false);
            this.grupBox_Opsicmb.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_Nohp;
        private System.Windows.Forms.TextBox txtBox_NoHP;
        private System.Windows.Forms.Button btn_Input;
        private System.Windows.Forms.Button btn_Hapus;
        private System.Windows.Forms.GroupBox grupBox_Opsicmb;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton RadioBtn_opsi2;
        private System.Windows.Forms.RadioButton RadioBtn_opsi1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox cmbBox_Kelas;
        private System.Windows.Forms.ComboBox cmbBox_Jurusan;
        private System.Windows.Forms.ComboBox cmbBox_Angkatan;
        private System.Windows.Forms.CheckBox chekBox_Opsi;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RichTextBox rchBox_TextSms;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btn_Kirim;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem optionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingConfigurationToolStripMenuItem;
    }
}