﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Microsoft.VisualBasic;
using SMSGatewayHonorDosen;

namespace MainFormKeuangan
{
    public partial class Form_Login : Form
    {
        MySqlConnection conn;
        MySqlCommand cmd;
        MySqlDataAdapter adp;
        MySqlDataReader reader;

        RegClass regClass = new RegClass();

        public static string nik;
        string pass;

        public Form_Login()
        {
            InitializeComponent();
            Constanta.Server();
        }

        private void btn_Settings_Click(object sender, EventArgs e)
        {
            string x = Interaction.InputBox("Developer Team Account : ", "Entering To Configuration", "");
            if (x == "UPT Pengembangan Sisfo Praktisi")
            {
                SettingConfiguration fset = new SettingConfiguration();
                fset.ShowDialog();
            }
            else
            {
                MessageBox.Show("Configuration is Denied");
            }
        }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_Login_Click(object sender, EventArgs e)
        {
            nik = txtBox_Uname.Text;
            pass = txtbox_Password.Text;
            if (nik != "" && pass != "")
            {
                if (login(nik, pass))
                {
                    if (ValidasiBagian(nik))
                    {
                        MainKeuanganForm mfk = new MainKeuanganForm();
                        mfk.Show();
                        this.Hide();
                    }
                    else
                    {
                        MessageBox.Show("Program ini hanya dibuat untuk bagian keuangan");
                    }
                }
                else
                {
                    MessageBox.Show("Anda gagal Login");
                }
            }
            else
            {
                if (nik == "" && pass == "")
                {
                    MessageBox.Show("Silahkan isi NIK dan password");

                }
                else if (nik == "")
                {
                    MessageBox.Show("Anda belum mengisi npm");

                }
                else if (pass == "")
                {
                    MessageBox.Show("Anda belum mengisi password");
                }
            }
        
        }

        private bool login(string nik, string pass)
        {
            bool l = false;

            conn = new MySqlConnection(Constanta.conString);
            string query = "SELECT * FROM tb_pegawai WHERE nik_pegawai = '" + nik + "' AND password = '" + pass + "'";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                int count = 0;
                while (reader.Read())
                {
                    count = count + 1;
                }
                if (count == 1)
                {
                    l = true;
                }
                else
                {
                    l = false;
                }

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                l = false;
            }

            return l;

            
        }

        private bool ValidasiBagian(string nik)
        {
            bool v = false;
            conn = new MySqlConnection(Constanta.conString);

            string query = "SELECT tb_distpegawai.kode_bagian, tb_bagian.nama_bagian FROM tb_pegawai INNER JOIN tb_distpegawai ON tb_pegawai.nik_pegawai = tb_distpegawai.nik_pegawai INNER JOIN tb_bagian ON tb_bagian.kode_bagian = tb_distpegawai.kode_bagian WHERE tb_pegawai.nik_pegawai = '" + nik + "'";
            cmd = new MySqlCommand(query, conn);

            try
            {
                adp = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    if (r[0].ToString() == "KEU")
                    {
                       v = true;
                    }
                    else
                    {
                        v = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return v;
        }
    }
}
