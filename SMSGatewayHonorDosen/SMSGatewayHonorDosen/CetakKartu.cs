﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using CrystalDecisions.CrystalReports.Engine;
using SMSGatewayHonorDosen;
using Aplikasi_Main_Akademik;


namespace kartuUjian
{
    public partial class CetakKartu : Form
    {

        static string connect = "server = 192.168.0.123; database = db_sisfopraktisi; Uid = root; pwd = uptsisfo46; Allow User Variables=True";
        MySqlConnection con = new MySqlConnection(connect);
        MySqlCommand cmd;
        MySqlDataAdapter adp;

        public CetakKartu()
        {
            InitializeComponent();
            
            //AmbilSmsterUjian();
            //AmbilTahunAjaran();

            //cmbBox_Semester.Enabled = false;
            //cmbBox_Ta.Enabled = false;

            string npm = Form_Cek_Kuisioner.npm;
            AmbilDataHeader(npm);

            
            //txtBox_npm.Enabled = false;
            //btn_carinpm.Enabled = false;

        }
        //private void AmbilDataHeader(string npm,string tA, string indexSmstr, string indexPilSu, string pilStatusUjian, string smstr, string ta)
        private void AmbilDataHeader(string npm)
        {
            //PrintReport();
            //Ambil Informasi Mahasiswa
            string query = "SELECT tb_mahasiswa.npm_mhs, tb_mahasiswa.nama_mhs, tb_jurusan.nama_jurusan, tb_kelas.nama_kelas, tb_mahasiswa.foto_mhs FROM tb_mahasiswa INNER JOIN tb_jurusan ON tb_jurusan.kode_jurusan = tb_mahasiswa.kode_jurusan INNER JOIN tb_kelas ON tb_kelas.kode_kelas = tb_mahasiswa.kode_kelas WHERE tb_mahasiswa.npm_mhs = '" + npm + "'";
           
            //Ambil informasi jadwal
            //string query2 = "SELECT tb_mahasiswa.npm_mhs, tb_akd_jadwal_ujian.hari, tb_akd_jadwal_ujian.tanggal_ujian, tb_matkul.nama_matkul, tb_matkul.sks, tb_akd_sesi_ujian.jam_sesi, tb_akd_ruangan_ujian.kode_ruangan FROM tb_mahasiswa INNER JOIN tb_mahasiswa_mengontrak_matkul ON tb_mahasiswa_mengontrak_matkul.npm_mhs = tb_mahasiswa.npm_mhs INNER JOIN tb_perkuliahan ON tb_perkuliahan.kode_perkuliahan = tb_mahasiswa_mengontrak_matkul.kode_perkuliahan INNER JOIN tb_matkul ON tb_matkul.kode_matkul = tb_perkuliahan.kode_matkul LEFT JOIN tb_akd_jadwal_ujian ON tb_akd_jadwal_ujian.kode_perkuliahan = tb_mahasiswa_mengontrak_matkul.kode_perkuliahan LEFT JOIN tb_akd_sesi_ujian ON tb_akd_sesi_ujian.kode_sesi = tb_akd_jadwal_ujian.kode_sesi LEFT JOIN tb_akd_ruangan_ujian ON tb_akd_ruangan_ujian.kode_ruangan = tb_akd_jadwal_ujian.kode_ruangan WHERE tb_mahasiswa.npm_mhs = '" + npm + "' AND tb_akd_jadwal_ujian.tahun_akademik = '" + tA + "' AND tb_akd_jadwal_ujian.kode_status_semester = '" + indexSmstr + "' AND tb_akd_jadwal_ujian.kode_status_ujian = '"+ indexPilSu + "' ORDER BY tb_akd_jadwal_ujian.tanggal_ujian, tb_akd_sesi_ujian.jam_sesi ASC";
            string query2 = "SELECT tb_mahasiswa.npm_mhs, tb_akd_jadwal_ujian.hari, tb_akd_jadwal_ujian.tanggal_ujian, tb_matkul.nama_matkul, tb_matkul.sks, tb_akd_sesi_ujian.jam_sesi, tb_akd_ruangan_ujian.kode_ruangan FROM tb_mahasiswa INNER JOIN tb_mahasiswa_mengontrak_matkul ON tb_mahasiswa_mengontrak_matkul.npm_mhs = tb_mahasiswa.npm_mhs INNER JOIN tb_perkuliahan ON tb_perkuliahan.kode_perkuliahan = tb_mahasiswa_mengontrak_matkul.kode_perkuliahan INNER JOIN tb_matkul ON tb_matkul.kode_matkul = tb_perkuliahan.kode_matkul LEFT JOIN tb_akd_jadwal_ujian ON tb_akd_jadwal_ujian.kode_perkuliahan = tb_mahasiswa_mengontrak_matkul.kode_perkuliahan LEFT JOIN tb_akd_sesi_ujian ON tb_akd_sesi_ujian.kode_sesi = tb_akd_jadwal_ujian.kode_sesi LEFT JOIN tb_akd_ruangan_ujian ON tb_akd_ruangan_ujian.kode_ruangan = tb_akd_jadwal_ujian.kode_ruangan WHERE tb_mahasiswa.npm_mhs = '" + npm +"' AND tb_akd_jadwal_ujian.tahun_akademik = '2016' AND tb_akd_jadwal_ujian.kode_status_semester = '1' AND tb_akd_jadwal_ujian.kode_status_ujian = '2' ORDER BY tb_akd_jadwal_ujian.tanggal_ujian, tb_akd_sesi_ujian.jam_sesi ASC";

            
            MySqlCommand cmd1 = new MySqlCommand(query, con);
            MySqlCommand cmd2 = new MySqlCommand(query2, con);
            

            try
            {
                

                con.Open();
                MySqlDataAdapter adp1 = new MySqlDataAdapter(cmd1);
                MySqlDataAdapter adp2 = new MySqlDataAdapter(cmd2);
                
                ////////////////////// Lama  /////////////////////////
                //////informasiMhs im = new informasiMhs();
                //////Informasijadwal ij = new Informasijadwal();
                //////adp1.Fill(im, "tb_mahasiswa");
                ///////adp2.Fill(ij, "tb_mahasiswa");
                //////////////////////////////////////////////////////
                //isi ke dataset
                InformasiMahasiswa imhs = new InformasiMahasiswa();
                InformasiJdwl jd = new InformasiJdwl();
                adp1.Fill(imhs, "tb_mahasiswa");
                adp2.Fill(jd, "tb_mahasiswa");

                con.Close();
                
                //Baru
                KartuUjian ku = new KartuUjian();
                ku.SetDataSource(imhs.Tables[1]);
                ku.Subreports[0].SetDataSource(jd.Tables[1]);

                cR_CetakKartu.ReportSource = ku;

                //Baru
                //TextObject to = (TextObject)ku.ReportDefinition.ReportObjects["txt_subtitle"];
                //string text = pilStatusUjian+  " " + smstr + " TA " + ta;
                //to.Text = text;

                TextObject t = (TextObject)ku.ReportDefinition.ReportObjects["txt_noabsen"];
                string no = AmbilNoAbsen(npm);
                t.Text = "No Absen : " + no;
                
                //MessageBox.Show(no);
                cR_CetakKartu.Refresh();
               

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        List<string> kodeSmester = new List<string>();
        void AmbilSmsterUjian()
        {
            
            kodeSmester.Clear();
            cmbBox_Semester.Items.Clear();
            string query = "SELECT kode_status_semester, status_semester FROM tb_akd_status_semester";
            cmd = new MySqlCommand(query, con);

            try
            {
                con.Open();
                adp = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {
                    kodeSmester.Add(r[0].ToString());
                    cmbBox_Semester.Items.Add(r[1].ToString());
                }

                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        List<string> kodeTa = new List<string>();
        void AmbilTahunAjaran()
        {
           
            cmbBox_Ta.Items.Clear();
            kodeTa.Clear();
            string query = "SELECT kode_tahun_akademik, tahun_akademik FROM tb_tahun_akademik";
            cmd = new MySqlCommand(query, con);

            try
            {
                con.Open();
                adp = new MySqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                adp.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    kodeTa.Add(r[0].ToString());
                    cmbBox_Ta.Items.Add(r[1].ToString());
                }
                con.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cmbBox_StatusUjian_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbBox_Semester.Enabled = true;
        }

        private void cmbBox_Semester_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbBox_Ta.Enabled = true;
        }

        private void cmbBox_Ta_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtBox_npm.Enabled = true;
        }

        private void txtBox_npm_TextChanged(object sender, EventArgs e)
        {
            //btn_carinpm.Enabled = true;
        }

        private void btn_carinpm_Click(object sender, EventArgs e)
        {

            //int indSu = cmbBox_StatusUjian.SelectedIndex + 1;
            //int indSmster = cmbBox_Semester.SelectedIndex + 1;
            //int indTa = cmbBox_Ta.SelectedIndex;
            string npm = txtBox_npm.Text;
            //string judSu = cmbBox_StatusUjian.SelectedItem.ToString();
            //string judsmstr = cmbBox_Semester.SelectedItem.ToString();
            //string judTa = cmbBox_Ta.SelectedItem.ToString();

            AmbilDataHeader(npm);
            //MessageBox.Show(AmbilNoAbsen(npm));

            txtBox_npm.Clear();
        }

        private string AmbilNoAbsen(string npm)
        {
            string no = "";

            string query = "SELECT npm_mhs, tb_mahasiswa.position FROM (SELECT tb_mahasiswa.npm_mhs, tb_mahasiswa.nama_mhs, @rownum := @rownum + 1 AS position FROM tb_mahasiswa JOIN (SELECT @rownum := 0) r WHERE kode_kelas=(SELECT kode_kelas FROM tb_mahasiswa WHERE npm_mhs='" + npm + "')) tb_mahasiswa WHERE npm_mhs = '" + npm + "'";
            cmd = new MySqlCommand(query, con);

            try
            {
                con.Open();
                
                adp = new MySqlDataAdapter(cmd);

                DataTable dt = new DataTable();
                
                adp.Fill(dt);
                
                foreach (DataRow r in dt.Rows)
                {
                    
                    no = r[1].ToString();
                }
                con.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            return no;
        }

        private void CetakKartu_FormClosing(object sender, FormClosingEventArgs e)
        {
            MainKeuanganForm.isFormAnak = false;
            //Application.Exit();
            //this.Close();
        }

    }
}
