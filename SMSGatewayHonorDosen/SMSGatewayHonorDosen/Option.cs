﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using AlgoritmaPenambahanTextLib;
using MySql.Data.MySqlClient;

namespace SMSGatewayHonorDosen
{
    public partial class Option : Form
    {
        MySqlConnection connect;
        MySqlCommand cmd;
        MySqlDataAdapter adp;
        string query;

        //library
        HawkEye hw = new HawkEye();
        //Variabel yang dibutuhkan untuk menginisialisasi gammu
        string[] Port;
        string masterSmsdrc;
        string masterGammurc;
        bool isGammurc;
        string filename;
        string pathSmsdrc;
        string pathGammurc;
        string port;
        string connection;
        public static string path_aplikasi;
        public static string perintah;

        //List yang dibutuhkan untuk mencari dan mengisi di file gammurc
        List<string> yangDicaridiGammurc = new List<string> { "device =", "connection =" };
        List<string> yangDiisidiGammurc = new List<string>();
        //port di get dari variabel

        //List yang dibutuhkan untuk mencari dan mengisi di file smsdrc
        List<string> yangDicaridiSmsdrc = new List<string>{"device =", "connection =" 
            , "service =", "user =", "password =", "pc =", "database =", "driver ="};
        List<string> yangDiisidiSmsdrc = new List<string>();

        public Option()
        {
            InitializeComponent();
            Constanta.Server();

            connect = new MySqlConnection(Constanta.conString);

            GetPort();
            TampilanEnableDisable();
            FillComboboxMerk();
        }

        private void TampilanEnableDisable()
        {
            cmbBox_Modem.Enabled = false;
            cmbBox_Type.Enabled = false;
            cmbBox_Connection.Enabled = false;
            btn_Copy.Enabled = false;
            cmbBox_Service.Enabled = false;
            txtBox_Database.Enabled = false;
            txtbox_Driver.Enabled = false;
            txtbox_Password.Enabled = false;
            txtbox_PC.Enabled = false;
            txtbox_User.Enabled = false;
            btn_installservice.Enabled = false;
            btn_start.Enabled = false;
            btn_Copy.Enabled = false;
        }

        private void btn_browse_Click(object sender, EventArgs e)
        {
            OpenFileDialog od = new OpenFileDialog();

            if (od.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtbox_Path.Text = od.FileName;
                //rchbox_master.Text = hw.BacaIsiFile(od.FileName);
                cmbBox_Modem.Enabled = true;
                if (od.SafeFileName == "gammurc")
                {
                    masterGammurc = hw.BacaIsiFile(od.FileName);
                    pathGammurc = od.FileName;
                    //MessageBox.Show(pathGammurc);
                    isGammurc = true;
                }
                else if (od.SafeFileName == "smsdrc")
                {
                    masterSmsdrc = hw.BacaIsiFile(od.FileName);
                    pathSmsdrc = od.FileName;
                    isGammurc = false;
                }
                else
                {

                }
            }
        }

        private void GetPort()
        {
            // Port yang digunakan
            Port = SerialPort.GetPortNames();
            try
            {
                for (int i = 0; i < 1; i++)
                {
                    port = Port[i];
                    MessageBox.Show(port);
                    yangDiisidiGammurc.Add(port + ":");
                    yangDiisidiSmsdrc.Add(port + ":");
                }
                btn_browse.Enabled = true;
                txtbox_Path.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Port tidak ditemukan");
                btn_browse.Enabled = false;
                txtbox_Path.Enabled = false;
                btn_installservice.Enabled = false;
                btn_start.Enabled = false;
                btn_Copy.Enabled = false;
                btn_saveFile.Enabled = false;
                btn_identify.Enabled = false;
            }

        }

        private void FillComboboxMerk()
        {
            query = "SELECT DISTINCT Manufacturer FROM tb_connection";
            cmd = new MySqlCommand(query, connect);
            cmbBox_Modem.Items.Clear();
            try
            {
                connect.Open();
                DataTable dt = new DataTable();
                adp = new MySqlDataAdapter(cmd);
                adp.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cmbBox_Modem.Items.Add(r[0].ToString());
                }
                connect.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FillComboboxType(string manufacturer)
        {
            query = "SELECT DISTINCT Name FROM tb_connection WHERE Manufacturer ='" + manufacturer + "'";
            cmd = new MySqlCommand(query, connect);
            cmbBox_Type.Items.Clear();
            try
            {
                connect.Open();
                DataTable dt = new DataTable();
                adp = new MySqlDataAdapter(cmd);
                adp.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cmbBox_Type.Items.Add(r[0].ToString());
                }
                connect.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FillComboboxConnection(string manufacturer, string type)
        {
            query = "SELECT DISTINCT Connection FROM tb_connection WHERE Manufacturer ='" + manufacturer + "' AND Name ='" + type + "'";
            cmd = new MySqlCommand(query, connect);
            cmbBox_Connection.Items.Clear();
            try
            {
                connect.Open();
                DataTable dt = new DataTable();
                adp = new MySqlDataAdapter(cmd);
                adp.Fill(dt);
                foreach (DataRow r in dt.Rows)
                {
                    cmbBox_Connection.Items.Add(r[0].ToString());
                }
                connect.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void cmbBox_Connection_SelectedIndexChanged(object sender, EventArgs e)
        {
            connection = cmbBox_Connection.Text;
            yangDiisidiGammurc.Add(connection);
            yangDiisidiSmsdrc.Add(connection);
            cmbBox_Service.Enabled = true;
            txtBox_Database.Enabled = true;
            txtbox_Driver.Enabled = true;
            txtbox_Password.Enabled = true;
            txtbox_PC.Enabled = true;
            txtbox_User.Enabled = true;
            btn_Copy.Enabled = true;
        }

        private void btn_saveFile_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFile = new SaveFileDialog();
            if (saveFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                using (Stream s = File.Open(saveFile.FileName, FileMode.CreateNew))
                using (StreamWriter sw = new StreamWriter(s))
                {
                    if (isGammurc == true)
                    {
                        CaridanIsidiMasterGammurc();
                        sw.Write(masterGammurc);
                    }
                    else if (isGammurc == false)
                    {
                        CaridanIsidiMasterSmsdrc();
                        sw.Write(masterSmsdrc);
                        filename = saveFile.FileName;
                    }
                }
                MessageBox.Show("File " + saveFile.FileName + " berhasil disimpan");
            }
            else
            {

            }
        }

        private void CaridanIsidiMasterGammurc()
        {
            //CariKalimatGammurc();
            for (int i = 0; i < yangDicaridiGammurc.Count; i++)
            {
                masterGammurc = hw.TambahkanText(yangDicaridiGammurc[i], masterGammurc, yangDiisidiGammurc[i], hw.GetFirstIndex(masterGammurc, yangDicaridiGammurc[i]));
                //rchbox_master.Text = hw.TambahkanText(yangDicaridiGammurc[i], rchbox_master.Text, yangDiisidiGammurc[i], hw.GetFirstIndex(rchbox_master.Text, yangDicaridiGammurc[i]));
            }
        }

        private void CaridanIsidiMasterSmsdrc()
        {
            //listke-2
            yangDiisidiSmsdrc.Add(cmbBox_Service.Text);
            //listke-3
            yangDiisidiSmsdrc.Add(txtbox_User.Text);
            //listke-4
            yangDiisidiSmsdrc.Add(txtbox_Password.Text);
            //listke-5
            yangDiisidiSmsdrc.Add(txtbox_PC.Text);
            //listke-6
            yangDiisidiSmsdrc.Add(txtBox_Database.Text);
            //listke-7
            yangDiisidiSmsdrc.Add(txtbox_Driver.Text);
            //CariKalimatSMSDRC();
            for (int j = 0; j < yangDicaridiSmsdrc.Count; j++)
            {
                masterSmsdrc = hw.TambahkanText(yangDicaridiSmsdrc[j], masterSmsdrc, yangDiisidiSmsdrc[j], hw.GetFirstIndex(masterSmsdrc, yangDicaridiSmsdrc[j]));
                //rchbox_master.Text = hw.TambahkanText(yangDicaridiSmsdrc[j], rchbox_master.Text, yangDiisidiSmsdrc[j], hw.GetFirstIndex(rchbox_master.Text, yangDicaridiSmsdrc[j]));
            }
        }

        private void cmbBox_Modem_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillComboboxType(cmbBox_Modem.Text);
            cmbBox_Type.Enabled = true;
        }

        private void cmbBox_Type_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillComboboxConnection(cmbBox_Modem.Text, cmbBox_Type.Text);
            cmbBox_Connection.Enabled = true;
        }

        public static void ProcessCMD(string command, TextBox t)
        {
            path_aplikasi = t.Text;
            perintah = command;
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.UseShellExecute = true;
            startInfo.WorkingDirectory = Path.GetDirectoryName(path_aplikasi);
            startInfo.Arguments = perintah;
            startInfo.FileName = "cmd.exe";
            process.StartInfo = startInfo;
            process.Start();

        }

        private void btn_identify_Click(object sender, EventArgs e)
        {
            ProcessCMD("/C gammu.exe identify", txtbox_Path);
        }

        private void btn_copy()
        {
            //sourcePath
            string sourcePath = @"" + pathSmsdrc;
            //Nentuin path buat target copy
            string targetPath;
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                targetPath = @"" + ofd.FileName;
                string sourceFile = System.IO.Path.Combine(sourcePath, filename);
                string destFile = System.IO.Path.Combine(targetPath, filename);

                if (System.IO.Directory.Exists(targetPath))
                {
                    System.IO.Directory.CreateDirectory(targetPath);
                }

                System.IO.File.Copy(sourceFile, targetPath, true);

                MessageBox.Show("File " + filename + "berhasil dicopy");
            }
            else
            {

            }
        }

        private void btn_Copy_Click(object sender, EventArgs e)
        {
            btn_copy();
        }

        private void btn_installservice_Click(object sender, EventArgs e)
        {
            ProcessCMD("/C gammu-smsd.exe -c D:\\Gammu\\smsdrc -i", txtbox_Path);
        }

        private void btn_start_Click(object sender, EventArgs e)
        {
            ProcessCMD("/C gammu-smsd.exe -c D:\\Gammu\\smsdrc -s", txtbox_Path);
        }
    }
}
