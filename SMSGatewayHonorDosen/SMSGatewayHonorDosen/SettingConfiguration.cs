﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace SMSGatewayHonorDosen
{
    public partial class SettingConfiguration : Form
    {
        //MySqlConnection connection;
        MySqlConnection connectionTesting;


        RegClass regClass = new RegClass();
        
        public SettingConfiguration()
        {
            InitializeComponent();
        }

        private void btn_cekKoneksi_Click(object sender, EventArgs e)
        {
            if (!ValidasiIP())
                MessageBox.Show("Isi terlebih dahulu Server IP Address");
            else
            {
                Cek_Koneksi(txtbox_ipaddress);
            }
        }

        private void Cek_Koneksi(TextBox t)
        {
            connectionTesting = new MySqlConnection("Server=" + t.Text + "; Database = db_sisfopraktisi ; Uid=root; Pwd = uptsisfo46");
            
            try
            {
                connectionTesting.Open();

                MessageBox.Show("Connection is Connect");

                connectionTesting.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR CONNECT : " + ex.Message);
            }
        }

        private bool ValidasiIP()
        {
            bool b = false;

            string ip = txtbox_ipaddress.Text;
            string digit6Awal = "";

            if (ip.Length > 7)
            {
                digit6Awal = ip.Substring(0, 8);

                if (digit6Awal == "192.168.")
                {
                    b = true;
                }
                else
                {
                    b = false;
                }
            }
            return b;
        }

        private void btn_setIP_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Yakin Untuk Set Alamat " + txtbox_ipaddress.Text + " sebagai Server Baru?", "Set IP",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (regClass.WriteToRegistry("ip", txtbox_ipaddress.Text))
                {
                    Constanta.server = txtbox_ipaddress.Text;
                    Constanta.Server();
                    MessageBox.Show("Server  " + Constanta.server + " Berhasil di Save");
                    //Constanta.ServerKeuangan();
                    //MessageBox.Show("Server Keuangan " + Constanta.server + " Berhasil di Save");
                    txtbox_ipaddress.Clear();
                }
            }
        }
    }
}
