﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Mockup_Pembayaran_Mhs_New;

namespace SMSGatewayHonorDosen
{
    public partial class Form_BayarSPPDPP : Form
    {
        DataTable mainDGVKeuangan;
        List<Mahasiswa> mahasiswa = new List<Mahasiswa>();
        List<DataKeuangan> dk = new List<DataKeuangan>();
        string kategori;

        OlahDataKeuangan spp = new OlahDataKeuangan();
        JoyashariSON j = new JoyashariSON();

        public Form_BayarSPPDPP()
        {
            InitializeComponent();

            SetGrid();
        }

        private void OlahDataSPP_Load(object sender, EventArgs e)
        {
            
        }
        private bool CariNPM(string npm, string php)
        {
            bool b = false;

            if (spp.NPMChecking(npm, php))
            {
                // store nilai dari parsing json ke class mahasiswa
                mahasiswa = j.ParsingJSONString(spp.Message);

                // isikan nilai ke textbox
                FillInformationTextBox(mahasiswa[0].NPM, mahasiswa[0].NamaMhs, mahasiswa[0].Kelas, mahasiswa[0].Jurusan,
                    mahasiswa[0].Angkatan, mahasiswa[0].FotoMhs);

                // isi data keuangan
                EnterDataKeuangan(j, spp);

                b = true;
            }

            return b;
        }
        private void btn_cariNPM_Click(object sender, EventArgs e)
        {
            rbtn_spp.Enabled = false;
            rbtn_dpp.Enabled = false;

            if(CariNPM(txtbox_cekNPM.Text, "http://" + Constanta.server + "/SisfoPraktisi/php/keuangan/ceklogin.php"))
            {
                if (mahasiswa[0].PiutangTiapSemester == null)
                {
                    MessageBox.Show("Belum Terdaftar di Kategori Pembayaran");
                }
                else
                {
                    // open radio button
                    rbtn_spp.Enabled = true;
                    rbtn_dpp.Enabled = true;
                }
                // open radio button
                //rbtn_spp.Enabled = true;
                //rbtn_dpp.Enabled = true;
            }
            else
            {
                // not found data and error
                MessageBox.Show(spp.Message);
            }

            // bersihkan cek NPM textbox
            txtbox_cekNPM.Clear();
            txtbox_cekNPM.Focus();

            BersihkanInfoPembayaran();
        }

        private void BersihkanInfoPembayaran()
        {
            ClearFiltering();

            cbox_semester.SelectedIndex = -1;
            txtbox_cicilanke.Clear();
            txtbox_status.Clear();
            txtbox_sisaBayar.Clear();
            txtbox_besarbayaran.Clear();
            txtbox_besarbayaran.Enabled = false;
            btn_inputbayar.Enabled = false;
        }

        private void OlahDataSPP_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Apakah yakin akan keluar?", "exit - session",
                MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                MainKeuanganForm.isFormAnak = false;
            }
            else
            {
                e.Cancel = true;
            }
        }
        private void rbtn_dpp_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtn_dpp.Checked)
            {
                cbox_semester.Enabled = true;

                kategori = "dpp";
                // refresh
                DataView v = DatView(kategori, cbox_semester.SelectedIndex, mainDGVKeuangan);

                dgv_historyPembayaran.Columns.Clear();
                dgv_historyPembayaran.DataSource = v;

                SetGrid2();
            }
        }
        private void rbtn_spp_CheckedChanged(object sender, EventArgs e)
        {
            if (rbtn_spp.Checked)
            {
                cbox_semester.Enabled = true;
                kategori = "spp";

                // refresh
                DataView v = DatView(kategori, cbox_semester.SelectedIndex, mainDGVKeuangan);

                dgv_historyPembayaran.Columns.Clear();
                dgv_historyPembayaran.DataSource = v;

                SetGrid2();
            }
        }
        private void btn_inputbayar_Click(object sender, EventArgs e)
        {
            if (txtbox_besarbayaran.Text != "")
            {
                if (int.Parse(txtbox_sisaBayar.Text) >= int.Parse(txtbox_besarbayaran.Text))
                {
                    OperasiString os = new OperasiString();

                    if (MessageBox.Show("Apakah anda yakin untuk melakukan pembayaran kategori " + kategori +
                        " pada semester " + cbox_semester.SelectedItem +
                        " untuk cicilan ke-" + txtbox_cicilanke.Text +
                        " yaitu sebesar Rp. " + os.InputTextUang(txtbox_besarbayaran.Text) +
                        " untuk mahasiswa dengan NPM " + txtbox_npmmhs.Text,
                        "Validasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        string x = Microsoft.VisualBasic.Interaction.InputBox("Validasi", "Validasi2");

                        if (x == "aka")
                        {
                            string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                            string dateBank = dateTimePicker1.Value.ToString("yyyy-MM-dd");

                            OlahDataKeuangan s = new OlahDataKeuangan();

                            if (s.UploadPembayaran(txtbox_npmmhs.Text, date, dateBank,
                                int.Parse(cbox_semester.SelectedItem.ToString()),
                                int.Parse(txtbox_cicilanke.Text), int.Parse(txtbox_besarbayaran.Text), kategori,
                                "http://" + Constanta.server + "/SisfoPraktisi/php/keuangan/insertpembayarankedb.php"))
                            {
                                MessageBox.Show("respon server: " + s.MessageKeu);

                                cbox_semester.SelectedIndex = -1;
                                txtbox_cicilanke.Clear();
                                txtbox_status.Clear();
                                txtbox_sisaBayar.Clear();
                                txtbox_besarbayaran.Clear();
                                rbtn_spp.Checked = false;
                                rbtn_dpp.Checked = false;

                                EnterDataKeuangan(j, spp);
                            }
                            else
                            {
                                MessageBox.Show(s.MessageKeu);
                            }
                        }
                        else
                        {
                            MessageBox.Show("maaf validasi keuangan salah");
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Pembayaran yang anda inputkan melebihi Sisa Pembayaran");
                    txtbox_besarbayaran.Clear();
                }
            }
            else
            {
                MessageBox.Show("Isi dengan lengkap terlebih dahulu");
            } 
        }
        private void cbox_semester_SelectedIndexChanged(object sender, EventArgs e)
        {
            btn_inputbayar.Enabled = false;
            dateTimePicker1.Enabled = false;
            txtbox_besarbayaran.Enabled = false;

            // pemilihan index untuk semester > 0, pengaruh ke cicilan ke,, source : kategori
            if (cbox_semester.SelectedIndex > 0)
            {
                int indexSemester = cbox_semester.SelectedIndex;

                // fungsi untuk menampilkan data histori pembayaran di datagridview
                // bergantung (parameter) : semester dan kategori
                if (kategori == "dpp")
                {
                    DataView v = DatView("dpp", cbox_semester.SelectedIndex, mainDGVKeuangan);

                    dgv_historyPembayaran.Columns.Clear();
                    dgv_historyPembayaran.DataSource = v;

                    SetGrid2();
                }
                else if (kategori == "spp")
                {
                    DataView v = DatView("spp", cbox_semester.SelectedIndex, mainDGVKeuangan);

                    dgv_historyPembayaran.Columns.Clear();
                    dgv_historyPembayaran.DataSource = v;

                    SetGrid2();
                }

                int banyakBaris = dgv_historyPembayaran.Rows.Count;
                int cicilanCurr = 1;
                
                if (banyakBaris > 0)
                    cicilanCurr = int.Parse(dgv_historyPembayaran.Rows[banyakBaris - 1].Cells[2].Value.ToString());
                
                // isi txtbox cicilan, pengecekkan dulu terhadap data history pembayaran
                // setiap mahasiswa
                if(mahasiswa[0].DataKeu.Count > 0)
                    txtbox_cicilanke.Text = " " + dk[0].GetDataCurrentCicilan(cicilanCurr, banyakBaris).ToString();
                else
                    txtbox_cicilanke.Text = "1";
                
                // ambil data dari list pembayaran
                int sisa = SisaPembayaranSemesterTersebut(GetPiutangSemester(dgv_historyPembayaran), int.Parse(mahasiswa[0].PiutangTiapSemester));
                
                if (sisa == 0)
                {
                    txtbox_status.Text = "LUNAS";
                    txtbox_cicilanke.Text = "-";
                }
                else if (sisa > 0)
                {
                    txtbox_status.Text = "HUTANG";

                    btn_inputbayar.Enabled = true;
                    dateTimePicker1.Enabled = true;
                    txtbox_besarbayaran.Enabled = true;
                }
                else if (sisa < 0)
                {
                    txtbox_status.Text = "KELEBIHAN";
                }
                
                txtbox_sisaBayar.Text = sisa.ToString();                
            }
            else
            {
                txtbox_sisaBayar.Clear();
                txtbox_status.Clear();
                txtbox_besarbayaran.Clear();
                txtbox_cicilanke.Clear();
                txtbox_besarbayaran.Enabled = false;

                DataView v = DatView(kategori, cbox_semester.SelectedIndex, mainDGVKeuangan);

                dgv_historyPembayaran.Columns.Clear();
                dgv_historyPembayaran.DataSource = v;

                SetGrid2();
            }
        }

        private void SetGrid()
        {
            dgv_historyPembayaran.ColumnCount = 7;

            dgv_historyPembayaran.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgv_historyPembayaran.AllowUserToAddRows = false;
            dgv_historyPembayaran.AllowUserToDeleteRows = false;
            dgv_historyPembayaran.ReadOnly = true;
            dgv_historyPembayaran.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            dgv_historyPembayaran.ColumnHeadersDefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 8);
            dgv_historyPembayaran.RowHeadersVisible = false;

            foreach (DataGridViewColumn c in dgv_historyPembayaran.Columns)
            {
                c.DefaultCellStyle.ForeColor = Color.Black;
                c.DefaultCellStyle.Font = new Font("Tahoma", 12f, GraphicsUnit.Pixel);
            }

            dgv_historyPembayaran.Columns[0].Width = 39;
            dgv_historyPembayaran.Columns[1].Width = 35;
            dgv_historyPembayaran.Columns[2].Width = 40;
            dgv_historyPembayaran.Columns[3].Width = 60;
            dgv_historyPembayaran.Columns[4].Width = 150;
            dgv_historyPembayaran.Columns[5].Width = 150;

            dgv_historyPembayaran.Columns[0].Name = "Kat";
            dgv_historyPembayaran.Columns[1].Name = "SMST";
            dgv_historyPembayaran.Columns[2].Name = "Cicilan";
            dgv_historyPembayaran.Columns[3].Name = "Resi";
            dgv_historyPembayaran.Columns[4].Name = "Tgl input";
            dgv_historyPembayaran.Columns[5].Name = "Tgl Bank";
            dgv_historyPembayaran.Columns[6].Name = "Pembayaran";
        }

        private DateTime TodayDate()
        {
            return DateTime.Now;
        }

        private void ClearFiltering()
        {
            rbtn_dpp.Checked = false;
            rbtn_spp.Checked = false;
        }

        private string SetDataCicilanCurr(string cic)
        {
            int x = cic != null ? int.Parse(cic) : 0;
            return (x + 1).ToString();
        }

        private void EnterDataKeuangan(JoyashariSON js, OlahDataKeuangan spps)
        {
            // tarik data-data histori pembayaran dari database dengan syarat 'npm'
            // simpan ke list 
            
            if( txtbox_cekNPM.Text != "")
                dk = js.ParsingJSONStringInfoDataKeuUmumBasedOnClass(spps.ListPembayaran(txtbox_cekNPM.Text, 
                    "http://" + Constanta.server + "/SisfoPraktisi/php/keuangan/historydatapembayaran.php"));
            else if(txtbox_npmmhs.Text != "")
                dk = js.ParsingJSONStringInfoDataKeuUmumBasedOnClass(spps.ListPembayaran(txtbox_npmmhs.Text,
                    "http://" + Constanta.server + "/SisfoPraktisi/php/keuangan/historydatapembayaran.php"));

            //if (dk == null)
            //    MessageBox.Show("not");
            //else
            //    MessageBox.Show("dk: " + dk.Count);

            // store data keuangan ke class Mahasiswa
            if (dk != null)
                mahasiswa[0].DataKeu = dk;

            // set data riwayat keuangan ke grid
            ShowDataRiwayatPembayaran(mahasiswa[0].DataKeu);
        }

        private void FillInformationTextBox(string _npm, string _nama, string _kelas,
            string _jurusan, string _angkatan, string foto)
        {
            txtbox_npmmhs.Text = _npm;
            txtbox_nama.Text = _nama;
            txtbox_kelas.Text = _kelas;
            txtbox_jurusan.Text = _jurusan;
            txtbox_angkatan.Text = _angkatan;

            try
            {
                pcbox_foto.ImageLocation = "http://" + Constanta.server + foto.Substring(9);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SetGrid2()
        {
            dgv_historyPembayaran.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            dgv_historyPembayaran.AllowUserToAddRows = false;
            dgv_historyPembayaran.AllowUserToDeleteRows = false;
            dgv_historyPembayaran.ReadOnly = true;
            dgv_historyPembayaran.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            dgv_historyPembayaran.ColumnHeadersDefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 8);
            dgv_historyPembayaran.RowHeadersVisible = false;

            foreach (DataGridViewColumn c in dgv_historyPembayaran.Columns)
            {
                c.DefaultCellStyle.ForeColor = Color.Black;
                c.DefaultCellStyle.Font = new Font("Tahoma", 12f, GraphicsUnit.Pixel);
            }

            dgv_historyPembayaran.Columns[0].Width = 39;
            dgv_historyPembayaran.Columns[1].Width = 35;
            dgv_historyPembayaran.Columns[2].Width = 40;
            dgv_historyPembayaran.Columns[3].Width = 60;
            dgv_historyPembayaran.Columns[4].Width = 150;
            dgv_historyPembayaran.Columns[5].Width = 150;
        }

        private DataView DatView(string valueOfKategori, int valueOfSemester, DataTable dt)
        {
            DataView dv = new DataView(dt);

            // memberi nama kolom pada dataview
            NamingColumnsTable(dv);

            string colNameKategori = dv.Table.Columns[0].ColumnName;
            string colSmstKategori = dv.Table.Columns[1].ColumnName;

            if (valueOfSemester > 0)
                dv.RowFilter = colNameKategori + " = '" + valueOfKategori + "'"
                    + " AND " + colSmstKategori + " = '" + valueOfSemester + "'";
            else
                dv.RowFilter = colNameKategori + " = '" + valueOfKategori + "'"
                    + " AND " + colSmstKategori + " <> '" + valueOfSemester + "'";

            return dv;
        }

        private void NamingColumnsTable(DataView dv)
        {
            dv.Table.Columns[0].ColumnName = "Kat";
            dv.Table.Columns[1].ColumnName = "SMST";
            dv.Table.Columns[2].ColumnName = "Cicilan";
            dv.Table.Columns[3].ColumnName = "Resi";
            dv.Table.Columns[4].ColumnName = "Tgl Input";
            dv.Table.Columns[5].ColumnName = "Tgl Bank";
            dv.Table.Columns[6].ColumnName = "Pembayaran";
        }

        private DataTable GetDataGridToDataTable(DataGridView dg)
        {
            DataTable dt = new DataTable();

            // insert column
            foreach (DataGridViewColumn col in dg.Columns)
            {
                dt.Columns.Add(col.HeaderText);
            }

            // adding row
            foreach (DataGridViewRow row in dg.Rows)
            {
                DataRow dRows = dt.NewRow();

                foreach (DataGridViewCell cell in row.Cells)
                {
                    dRows[cell.ColumnIndex] = cell.Value;
                }

                dt.Rows.Add(dRows);
            }

            return dt;
        }

        private void ShowDataRiwayatPembayaran(List<DataKeuangan> dk)
        {
            mainDGVKeuangan = new DataTable();
            dgv_historyPembayaran.DataSource = null;
            dgv_historyPembayaran.Rows.Clear();

            SetGrid();

            foreach (DataKeuangan dr in dk)
            {
                dgv_historyPembayaran.Rows.Add(dr.Kategori, dr.SemesterPembayaran,
                    dr.CicilanKe, dr.Resi, dr.TanggalBayar.ToString(),
                    dr.TanggalBank.ToString("yyyy-MM-dd"), dr.Pembayaran);
            }

            // pindahkan main ke dgv (temp)
            mainDGVKeuangan = GetDataGridToDataTable(dgv_historyPembayaran);
        }        

        private List<int> GetPiutangSemester(DataGridView dg)
        {
            List<int> l = new List<int>();

            for (int i = 0; i < dg.Rows.Count;i++)
            {
                l.Add(int.Parse(dg.Rows[i].Cells[6].Value.ToString()));
            }

            return l;
        }

        private int kelebihanPembayaran;

        private int SisaPembayaranSemesterTersebut(List<int> l, int piutangTiapSemester)
        {
            int totalTerbayar = 0;
            
            foreach(int item in l)
            {
                totalTerbayar += item;
            }

            int sisaBayarSemesterTersebut = piutangTiapSemester - totalTerbayar;
            
            if( sisaBayarSemesterTersebut < 0)
            {
                kelebihanPembayaran = -(sisaBayarSemesterTersebut);
            }

            return sisaBayarSemesterTersebut;
        }
    }
}
