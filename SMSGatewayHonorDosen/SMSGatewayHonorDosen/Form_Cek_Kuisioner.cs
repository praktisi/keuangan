﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using CrystalDecisions.CrystalReports.Engine;
using SMSGatewayHonorDosen;
using kartuUjian;

namespace Aplikasi_Main_Akademik
{
    public partial class Form_Cek_Kuisioner : Form
    {
        MySqlConnection conn = new MySqlConnection(Constanta.conString);
        MySqlCommand cmd;
        MySqlDataReader dReader;
        MySqlDataAdapter adp;

        public static string npm;
        public Form_Cek_Kuisioner()
        {
            InitializeComponent();
        }

        private void Form_Cek_Kuisioner_FormClosed(object sender, FormClosedEventArgs e)
        {
            MainKeuanganForm.isFormAnak = false;
        }

        private void Btn_Cek_Click(object sender, EventArgs e)
        {
            npm = TxtBox_NPM.Text;

            if (TxtBox_NPM.Text != "")
            {
                if (CariNPM(conn, cmd, npm))
                {
                    if (CekIsiQuisioner(conn, cmd, dReader, npm))
                    {
                        Lbl_StatusPengisian.Text = "Sudah Lengkap";
                        btn_printLangsung.Enabled = true;

                        //TxtBox_NPM.Clear();
                    }
                    else
                    {
                        Lbl_StatusPengisian.Text = "Belum Lengkap";
                        btn_printLangsung.Enabled = false;

                        //TxtBox_NPM.Clear();
                        TxtBox_NPM.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Maaf NPM yang anda cari tidak ada");
                    Lbl_StatusPengisian.Text = "-";
                    btn_printLangsung.Enabled = false;
                    
                    TxtBox_NPM.Clear();
                    TxtBox_NPM.Focus();
                }
            }
            else
            {
                MessageBox.Show("Harap isi dulu npm yang akan dicari");
            }

            btn_Preview.Enabled = true;

        }

        public bool CariNPM(MySqlConnection cnn, MySqlCommand com, string npm)
        {
            bool isNPMAda = false;

            string query = "SELECT npm_mhs FROM tb_mahasiswa WHERE npm_mhs = '" + npm + "'";
            com = new MySqlCommand(query, cnn);

            try
            {
                cnn.Open();
                MySqlDataReader dReader;
                dReader = com.ExecuteReader();

                if (dReader.Read())
                {
                    isNPMAda = true;
                }
                cnn.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return isNPMAda;
        }

        public bool CekIsiQuisioner(MySqlConnection cnn, MySqlCommand com, MySqlDataReader read, string npm)
        {
            bool isLengkap = false;

            string query = "SELECT IF((SELECT COUNT(kode_perkuliahan) FROM tb_mahasiswa_mengontrak_matkul WHERE npm_mhs = '" + npm + "')=(SELECT COUNT(kode_perkuliahan) FROM tb_quipenildos_hasilangket WHERE npm_mhs = '" + npm + "'), true, false)";
            com = new MySqlCommand(query, cnn);

            try
            {
                cnn.Open();

                read = com.ExecuteReader();
                if (read.Read())
                {
                    if (read[0].ToString() == "1")
                    {
                        isLengkap = true;
                    }
                }

                cnn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return isLengkap;
        }

        private void Form_Cek_Kuisioner_Load(object sender, EventArgs e)
        {
            TxtBox_NPM.Clear();
        }

        void ResetTampilan()
        {
            TxtBox_NPM.Focus();
            Lbl_StatusPengisian.Text = "";
            btn_printLangsung.Enabled = false;
        }

        private void btn_printLangsung_Click(object sender, EventArgs e)
        {
            AmbilDataHeader(npm);
            ResetTampilan();
        }

        private void AmbilDataHeader(string npm)
        {
            //Ambil Informasi Mahasiswa
            
            string query = "SELECT tb_mahasiswa.npm_mhs, tb_mahasiswa.nama_mhs, tb_jurusan.nama_jurusan, tb_kelas.nama_kelas, tb_mahasiswa.foto_mhs FROM tb_mahasiswa INNER JOIN tb_jurusan ON tb_jurusan.kode_jurusan = tb_mahasiswa.kode_jurusan INNER JOIN tb_kelas ON tb_kelas.kode_kelas = tb_mahasiswa.kode_kelas WHERE tb_mahasiswa.npm_mhs = '" + npm + "'";

            //Ambil informasi jadwal
            //string query2 = "SELECT tb_mahasiswa.npm_mhs, tb_akd_jadwal_ujian.hari, tb_akd_jadwal_ujian.tanggal_ujian, tb_matkul.nama_matkul, tb_matkul.sks, tb_akd_sesi_ujian.jam_sesi, tb_akd_ruangan_ujian.kode_ruangan FROM tb_mahasiswa INNER JOIN tb_mahasiswa_mengontrak_matkul ON tb_mahasiswa_mengontrak_matkul.npm_mhs = tb_mahasiswa.npm_mhs INNER JOIN tb_perkuliahan ON tb_perkuliahan.kode_perkuliahan = tb_mahasiswa_mengontrak_matkul.kode_perkuliahan INNER JOIN tb_matkul ON tb_matkul.kode_matkul = tb_perkuliahan.kode_matkul LEFT JOIN tb_akd_jadwal_ujian ON tb_akd_jadwal_ujian.kode_perkuliahan = tb_mahasiswa_mengontrak_matkul.kode_perkuliahan LEFT JOIN tb_akd_sesi_ujian ON tb_akd_sesi_ujian.kode_sesi = tb_akd_jadwal_ujian.kode_sesi LEFT JOIN tb_akd_ruangan_ujian ON tb_akd_ruangan_ujian.kode_ruangan = tb_akd_jadwal_ujian.kode_ruangan WHERE tb_mahasiswa.npm_mhs = '" + npm + "' AND tb_akd_jadwal_ujian.tahun_akademik = '" + tA + "' AND tb_akd_jadwal_ujian.kode_status_semester = '" + indexSmstr + "' AND tb_akd_jadwal_ujian.kode_status_ujian = '"+ indexPilSu + "' ORDER BY tb_akd_jadwal_ujian.tanggal_ujian, tb_akd_sesi_ujian.jam_sesi ASC";
            string query2 = "SELECT tb_mahasiswa.npm_mhs, tb_akd_jadwal_ujian.hari, tb_akd_jadwal_ujian.tanggal_ujian, tb_matkul.nama_matkul, tb_matkul.sks, tb_akd_sesi_ujian.jam_sesi, tb_akd_ruangan_ujian.kode_ruangan FROM tb_mahasiswa INNER JOIN tb_mahasiswa_mengontrak_matkul ON tb_mahasiswa_mengontrak_matkul.npm_mhs = tb_mahasiswa.npm_mhs INNER JOIN tb_perkuliahan ON tb_perkuliahan.kode_perkuliahan = tb_mahasiswa_mengontrak_matkul.kode_perkuliahan INNER JOIN tb_matkul ON tb_matkul.kode_matkul = tb_perkuliahan.kode_matkul LEFT JOIN tb_akd_jadwal_ujian ON tb_akd_jadwal_ujian.kode_perkuliahan = tb_mahasiswa_mengontrak_matkul.kode_perkuliahan LEFT JOIN tb_akd_sesi_ujian ON tb_akd_sesi_ujian.kode_sesi = tb_akd_jadwal_ujian.kode_sesi LEFT JOIN tb_akd_ruangan_ujian ON tb_akd_ruangan_ujian.kode_ruangan = tb_akd_jadwal_ujian.kode_ruangan WHERE tb_mahasiswa.npm_mhs = '" + npm + "' AND tb_akd_jadwal_ujian.tahun_akademik = '2016' AND tb_akd_jadwal_ujian.kode_status_semester = '1' AND tb_akd_jadwal_ujian.kode_status_ujian = '2' ORDER BY tb_akd_jadwal_ujian.tanggal_ujian, tb_akd_sesi_ujian.jam_sesi ASC";


            MySqlCommand cmd1 = new MySqlCommand(query, conn);
            MySqlCommand cmd2 = new MySqlCommand(query2, conn);


            try
            {


                conn.Open();
                MySqlDataAdapter adp1 = new MySqlDataAdapter(cmd1);
                MySqlDataAdapter adp2 = new MySqlDataAdapter(cmd2);
                
                InformasiMahasiswa imhs = new InformasiMahasiswa();
                InformasiJdwl jd = new InformasiJdwl();
                adp1.Fill(imhs, "tb_mahasiswa");
                adp2.Fill(jd, "tb_mahasiswa");

                conn.Close();

                //Baru
                KartuUjian ku = new KartuUjian();
                ku.SetDataSource(imhs.Tables[1]);
                ku.Subreports[0].SetDataSource(jd.Tables[1]);
                
                TextObject t = (TextObject)ku.ReportDefinition.ReportObjects["txt_noabsen"];
                string no = AmbilNoAbsen(npm);
                t.Text = "No Absen : " + no;


                ku.PrintToPrinter(1, true, 1, 1);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private string AmbilNoAbsen(string npm)
        {
            string no = "";
            string connect = "server = 192.168.0.123; database = db_sisfopraktisi; Uid = root; pwd = uptsisfo46; Allow User Variables=True";
            conn = new MySqlConnection(connect);


            string query = "SELECT npm_mhs, tb_mahasiswa.position FROM (SELECT tb_mahasiswa.npm_mhs, tb_mahasiswa.nama_mhs, @rownum := @rownum + 1 AS position FROM tb_mahasiswa JOIN (SELECT @rownum := 0) r WHERE kode_kelas=(SELECT kode_kelas FROM tb_mahasiswa WHERE npm_mhs='" + npm + "')) tb_mahasiswa WHERE npm_mhs = '" + npm + "'";
            cmd = new MySqlCommand(query, conn);

            try
            {
                conn.Open();

                adp = new MySqlDataAdapter(cmd);

                DataTable dt = new DataTable();

                adp.Fill(dt);

                foreach (DataRow r in dt.Rows)
                {

                    no = r[1].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            return no;
        }

        private void btn_Preview_Click(object sender, EventArgs e)
        {
            if (npm != "")
            {
                CetakKartu ck = new CetakKartu();
                ck.ShowDialog();
                //npm = "";
                
            }
        }

        private void TxtBox_NPM_TextChanged(object sender, EventArgs e)
        {
            if (TxtBox_NPM.Text == "")
            {
                //Btn_Cek.Enabled = false;
                btn_printLangsung.Enabled = false;
                btn_Preview.Enabled = false;
                Lbl_StatusPengisian.Text = "";
            }
        }


    }
}
